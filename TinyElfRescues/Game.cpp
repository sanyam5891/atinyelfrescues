#include "game.h"

float GetRandomNumber(float min, float max)
{
	float result = ((rand() / (float)RAND_MAX) * (max - min)) + min;
	return result;
}

float GetMod(float x, float y)
{
	// -1, 20
	float quotient = x / y; // -0.05
	float integral = floorf(quotient); // -1.0f
	float result = x - (y * integral); // -1 - (20 * -1);
	return result;
}

#pragma region NodeSetup

void SetupNodeData()
{
	Game *game = GetGameMemory();
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;

	Node *nodes = currentLevelInfo->nodes;

	NodeType currentNode = NodeType::BEGIN_NODE;
	NodeType optionPosNode = NodeType::LEVEL1_NODE0;
	NodeType optionNegNode = NodeType::LEVEL1_NODE1;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	// ---------------------------------------------------------

	currentNode = NodeType::LEVEL1_NODE0;
	optionPosNode = NodeType::LEVEL2_NODE0;
	optionNegNode = NodeType::LEVEL2_NODE1;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL1_NODE1;
	optionPosNode = NodeType::LEVEL2_NODE2;
	optionNegNode = NodeType::LEVEL2_NODE3;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	// ---------------------------------------------------------

	currentNode = NodeType::LEVEL2_NODE0;
	optionPosNode = NodeType::LEVEL3_NODE0;
	optionNegNode = NodeType::LEVEL3_NODE1;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL2_NODE1;
	optionPosNode = NodeType::LEVEL3_NODE2;
	optionNegNode = NodeType::LEVEL3_NODE3;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL2_NODE2;
	optionPosNode = NodeType::LEVEL3_NODE4;
	optionNegNode = NodeType::LEVEL3_NODE5;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL2_NODE3;
	optionPosNode = NodeType::LEVEL3_NODE6;
	optionNegNode = NodeType::LEVEL3_NODE7;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	// ---------------------------------------------------------

	currentNode = NodeType::LEVEL3_NODE0;
	optionPosNode = NodeType::LEVEL4_NODE0;
	optionNegNode = NodeType::LEVEL4_NODE1;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE1;
	optionPosNode = NodeType::LEVEL4_NODE2;
	optionNegNode = NodeType::LEVEL4_NODE3;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE2;
	optionPosNode = NodeType::LEVEL4_NODE4;
	optionNegNode = NodeType::LEVEL4_NODE5;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE3;
	optionPosNode = NodeType::LEVEL4_NODE6;
	optionNegNode = NodeType::LEVEL4_NODE7;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE4;
	optionPosNode = NodeType::LEVEL4_NODE8;
	optionNegNode = NodeType::LEVEL4_NODE9;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE5;
	optionPosNode = NodeType::LEVEL4_NODE10;
	optionNegNode = NodeType::LEVEL4_NODE9;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE6;
	optionPosNode = NodeType::LEVEL4_NODE8;
	optionNegNode = NodeType::LEVEL4_NODE9;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL3_NODE7;
	optionPosNode = NodeType::LEVEL4_NODE14;
	optionNegNode = NodeType::LEVEL4_NODE15;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	// ---------------------------------------------------------

	currentNode = NodeType::LEVEL4_NODE0;
	optionPosNode = NodeType::LEVEL5_NODE0;
	optionNegNode = NodeType::LEVEL5_NODE1;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE1;
	optionPosNode = NodeType::LEVEL5_NODE2;
	optionNegNode = NodeType::LEVEL5_NODE3;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE2;
	optionPosNode = NodeType::LEVEL5_NODE4;
	optionNegNode = NodeType::LEVEL5_NODE5;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE3;
	optionPosNode = NodeType::LEVEL5_NODE6;
	optionNegNode = NodeType::LEVEL5_NODE7;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE4;
	optionPosNode = NodeType::LEVEL5_NODE8;
	optionNegNode = NodeType::LEVEL5_NODE9;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE5;
	optionPosNode = NodeType::LEVEL5_NODE10;
	optionNegNode = NodeType::LEVEL5_NODE11;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE6;
	optionPosNode = NodeType::LEVEL5_NODE12;
	optionNegNode = NodeType::LEVEL5_NODE13;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE7;
	optionPosNode = NodeType::LEVEL5_NODE14;
	optionNegNode = NodeType::LEVEL5_NODE15;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE8;
	optionPosNode = NodeType::LEVEL5_NODE16;
	optionNegNode = NodeType::LEVEL5_NODE17;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE9;
	optionPosNode = NodeType::LEVEL5_NODE18;
	optionNegNode = NodeType::LEVEL5_NODE19;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE10;
	optionPosNode = NodeType::LEVEL5_NODE20;
	optionNegNode = NodeType::LEVEL5_NODE21;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE14;
	optionPosNode = NodeType::LEVEL5_NODE28;
	optionNegNode = NodeType::LEVEL5_NODE29;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	currentNode = NodeType::LEVEL4_NODE15;
	optionPosNode = NodeType::LEVEL5_NODE30;
	optionNegNode = NodeType::LEVEL5_NODE31;
	nodes[currentNode].type = currentNode;
	nodes[currentNode].optionPos = &nodes[optionPosNode];
	nodes[currentNode].optionNeg = &nodes[optionNegNode];

	// ---------------------------------------------------------

	for (int i = NodeType::LEVEL5_NODE0; i <= NodeType::LEVEL5_NODE31; i++)
	{
		currentNode = (NodeType) i;
		nodes[currentNode].type = currentNode;
	}
	
	// ---------------------------------------------------------

	currentLevelInfo->currentNode = &nodes[NodeType::BEGIN_NODE];
	currentLevelInfo->completedNodes[0] = currentLevelInfo->currentNode;
}

#pragma endregion


#pragma region Loading Data

void LoadMap(MapType mapType, int *destination)
{
	switch (mapType)
	{
		case MapType::SUPER_EASY:
		{
			int mapEasy[TILEMAP_COUNT_Y * TILEMAP_COUNT_X] =
			{
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 1, 0, 0,
				0, 0, 1, 6, 0, 6, 1, 1, 1, 1, 1, 1, 1, 1, 5, 0, 5, 1, 0, 0,
				0, 0, 1, 6, 0, 6, 1, 1, 1, 1, 1, 1, 1, 1, 5, 0, 5, 1, 0, 0,
				0, 0, 1, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			};
			
			memcpy(destination, mapEasy, sizeof(mapEasy));
		}
		break;

		case MapType::OKAY_EASY:
		{
			int mapMedium[TILEMAP_COUNT_Y * TILEMAP_COUNT_X] =
			{
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
				0, 0, 1, 1, 6, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 2, 1, 0, 0,
				0, 0, 1, 6, 0, 6, 2, 1, 1, 1, 1, 1, 1, 1, 5, 0, 5, 2, 0, 0,
				0, 0, 1, 6, 0, 6, 2, 1, 1, 1, 1, 1, 1, 1, 5, 0, 5, 2, 0, 0,
				0, 0, 1, 1, 6, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 2, 1, 0, 0,
				0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			};
			
			memcpy(destination, mapMedium, sizeof(mapMedium));
		}
		break;

		case MapType::MEDIUM:
		{
			int mapHard[TILEMAP_COUNT_Y * TILEMAP_COUNT_X] =
			{
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 2, 2, 2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 5, 2, 2, 2, 0, 0,
				0, 0, 2, 6, 2, 6, 1, 1, 1, 1, 1, 1, 5, 5, 5, 2, 5, 2, 0, 0,
				0, 0, 2, 2, 2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 5, 2, 2, 2, 0, 0,
				0, 0, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			};

			memcpy(destination, mapHard, sizeof(mapHard));
		}
		break;

		case MapType::OKAY_HARD:
		{
			int mapHard[TILEMAP_COUNT_Y * TILEMAP_COUNT_X] =
			{
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 4, 6, 6, 6, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 4, 0, 0,
				0, 0, 4, 6, 1, 6, 1, 2, 1, 1, 1, 1, 1, 1, 5, 5, 5, 4, 0, 0,
				0, 0, 4, 1, 6, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 5, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 5, 1, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 5, 1, 1, 4, 0, 0,
				0, 0, 4, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 4, 0, 0,
			};

			memcpy(destination, mapHard, sizeof(mapHard));
		}
		break;

		case MapType::SUPER_HARD:
		{
			int mapHard[TILEMAP_COUNT_Y * TILEMAP_COUNT_X] =
			{
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 0, 0,
				0, 0, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 0, 0,
				0, 0, 1, 1, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
				0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			};

			memcpy(destination, mapHard, sizeof(mapHard));
		}
		break;
	}
}

int GetTileHitCapacity(TileType tileType)
{
	unsigned int hits = 0;
	switch (tileType)
	{
		case TileType::SINGLE_HIT:
			hits = 1;
			break;
		case TileType::DOUBLE_HIT:
			hits = 2;
			break;
		case TileType::TRIPLE_HIT:
			hits = 3;
			break;
		case TileType::RIGID:
			hits = 1;
			break;
		case TileType::GOOD_MEMORY:
			hits = 1;
			break;
		case TileType::BAD_MEMORY:
			hits = 1;
			break;
	}

	return hits;
}

void LoadCurrentTilemapInfo(MapType mapType)
{
	Game *game = GetGameMemory();
	void *tempMemory = GetTempMemory();
	int canvasWidth = game->canvasWidth;
	int canvasHeight = game->canvasHeight;

	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;

	currentTilemapInfo->sideGap = 0;
	currentTilemapInfo->topGap = 0;
	
	currentTilemapInfo->countX = TILEMAP_COUNT_X;
	currentTilemapInfo->countY = TILEMAP_COUNT_Y;
	currentTilemapInfo->tileAspectRatio = 2.0f;
	currentTilemapInfo->leftWallWidth = 96.0f;
	currentTilemapInfo->rightWallWidth = 96.0f;
	currentTilemapInfo->topWallHeight = 96.0f;
	currentTilemapInfo->bottomWallHeight = 132.0f;

	currentTilemapInfo->topLeft = glm::vec2(currentTilemapInfo->leftWallWidth + currentTilemapInfo->sideGap, canvasHeight - currentTilemapInfo->topWallHeight - currentTilemapInfo->topGap);
	currentTilemapInfo->topRight = glm::vec2(canvasWidth - currentTilemapInfo->rightWallWidth - currentTilemapInfo->sideGap, canvasHeight - currentTilemapInfo->topWallHeight - currentTilemapInfo->topGap);
	/*currentTilemapInfo->tileSlotScale.x = (float)(currentTilemapInfo->topRight.x - currentTilemapInfo->topLeft.x - ((currentTilemapInfo->countX - 1) * currentTilemapInfo->tileSpacingX)) / (float)currentTilemapInfo->countX;
	currentTilemapInfo->tileSlotScale.y = currentTilemapInfo->tileSlotScale.x / currentTilemapInfo->tileAspectRatio;*/
	
	currentTilemapInfo->tileSlotScale.x = 48.0f;
	currentTilemapInfo->tileSlotScale.y = 24.0f;
	
	currentTilemapInfo->tileSpacingX = ((currentTilemapInfo->topRight.x - currentTilemapInfo->topLeft.x) - (currentTilemapInfo->tileSlotScale.x * currentTilemapInfo->countX)) / 
		((float)currentTilemapInfo->countX - 1);
	currentTilemapInfo->tileSpacingY = currentTilemapInfo->tileSpacingX;
	


	currentTilemapInfo->mapType = mapType;
	LoadMap(mapType, currentTilemapInfo->map);
	int totalTiles = currentTilemapInfo->countX * currentTilemapInfo->countY;

	int totalActiveTiles = 0;
	int totalActiveNonMemoryTiles = 0;
	int totalActiveGoodMemoryTiles = 0;
	int totalActiveBadMemoryTiles = 0;

	int *map = currentTilemapInfo->map;
	for (int i = 0; i < totalTiles; i++)
	{
		TileType type = (TileType)map[i];
		if (type != TileType::NONE)
		{
			if (type == TileType::GOOD_MEMORY)
				totalActiveGoodMemoryTiles++;
			else if (type == TileType::BAD_MEMORY)
				totalActiveBadMemoryTiles++;
			else
				totalActiveNonMemoryTiles++;
		}
	}

	totalActiveTiles = totalActiveNonMemoryTiles + totalActiveGoodMemoryTiles + totalActiveBadMemoryTiles;

	int nonMemoryTilesStartIndex = 0;
	int goodMemoryTilesStartIndex = nonMemoryTilesStartIndex + totalActiveNonMemoryTiles;
	int badMemoryTilesStartIndex = goodMemoryTilesStartIndex + totalActiveGoodMemoryTiles;

	currentTilemapInfo->totalActiveTiles = totalActiveTiles;
	currentTilemapInfo->totalPossibleTiles = totalTiles;
	currentTilemapInfo->totalActiveNonMemoryTiles = totalActiveNonMemoryTiles;
	currentTilemapInfo->totalActiveGoodMemoryTiles = totalActiveGoodMemoryTiles;
	currentTilemapInfo->totalActiveBadMemoryTiles = totalActiveBadMemoryTiles;
	currentTilemapInfo->nonMemoryTilesStartIndex = nonMemoryTilesStartIndex;
	currentTilemapInfo->goodMemoryTilesStartIndex = goodMemoryTilesStartIndex;
	currentTilemapInfo->badMemoryTilesStartIndex = badMemoryTilesStartIndex;
	Tile *tiles = currentTilemapInfo->tiles;
	TileType *types = currentTilemapInfo->types;
	glm::vec2 *positions = currentTilemapInfo->positions;

	int totalRows = currentTilemapInfo->countY;
	int totalCols = currentTilemapInfo->countX;
	int currentNonMemoryIndex = nonMemoryTilesStartIndex;
	int currentGoodMemoryIndex = goodMemoryTilesStartIndex;
	int currentBadMemoryIndex = badMemoryTilesStartIndex;
	glm::vec2 topLeft = currentTilemapInfo->topLeft;
	float tileSlotScaleX = currentTilemapInfo->tileSlotScale.x;
	float tileSlotScaleY = currentTilemapInfo->tileSlotScale.y;
	float spacingX = currentTilemapInfo->tileSpacingX;
	float spacingY = currentTilemapInfo->tileSpacingY;

	int *goodMemoryPieceIndex = (int *)tempMemory;
	int *badMemoryPieceIndex = goodMemoryPieceIndex + totalActiveGoodMemoryTiles;
	int goodPiecesLeftToAllocate = totalActiveGoodMemoryTiles;
	int badPiecesLeftToAllocate = totalActiveBadMemoryTiles;

	for (int i = 0; i < totalActiveGoodMemoryTiles; i++)
	{
		goodMemoryPieceIndex[i] = i;
	}

	for (int i = 0; i < totalActiveBadMemoryTiles; i++)
	{
		badMemoryPieceIndex[i] = i;
	}

	for (int i = 0; i < totalTiles; i++)
	{
		int row = i / totalCols;
		int col = i % totalCols;
		TileType type = (TileType)map[i];

		int currentActiveIndex = 0;
		int hitsLeft = 0;
		if (type != TileType::NONE)
		{
			switch (type)
			{
				case TileType::GOOD_MEMORY:
				{
					currentActiveIndex = currentGoodMemoryIndex;
					currentGoodMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);

					int pieceIndex = (int)((rand() / (float)RAND_MAX) * goodPiecesLeftToAllocate);
					int val = goodMemoryPieceIndex[pieceIndex];
					for (int i = pieceIndex; i < goodPiecesLeftToAllocate-1; i++)
					{
						goodMemoryPieceIndex[i] = goodMemoryPieceIndex[i + 1];
					}
					goodPiecesLeftToAllocate--;

					tiles[currentActiveIndex].memory.pieceIndex = val;
				}
					break;
				case TileType::BAD_MEMORY:
				{
					currentActiveIndex = currentBadMemoryIndex;
					currentBadMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);

					int pieceIndex = (int)((rand() / (float)RAND_MAX) * badPiecesLeftToAllocate);
					int val = badMemoryPieceIndex[pieceIndex];
					for (int i = pieceIndex; i < badPiecesLeftToAllocate - 1; i++)
					{
						badMemoryPieceIndex[i] = badMemoryPieceIndex[i + 1];
					}
					badPiecesLeftToAllocate--;

					tiles[currentActiveIndex].memory.pieceIndex = val;
				}
					break;
				case TileType::SINGLE_HIT:
					currentActiveIndex = currentNonMemoryIndex;
					currentNonMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);
					break;
				case TileType::DOUBLE_HIT:
					currentActiveIndex = currentNonMemoryIndex;
					currentNonMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);
					break;
				case TileType::TRIPLE_HIT:
					currentActiveIndex = currentNonMemoryIndex;
					currentNonMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);
					break;
				case TileType::RIGID:
					currentActiveIndex = currentNonMemoryIndex;
					currentNonMemoryIndex++;
					hitsLeft = GetTileHitCapacity(type);
					break;
			}

			tiles[currentActiveIndex].mapIndex = i;
			tiles[currentActiveIndex].hitsLeft = GetTileHitCapacity(type);
			types[currentActiveIndex] = type;
			positions[currentActiveIndex] = topLeft + glm::vec2(tileSlotScaleX * 0.5f, -tileSlotScaleY * 0.5f) +
				glm::vec2(col * tileSlotScaleX, -row * tileSlotScaleY) + glm::vec2(col * spacingX, -row * spacingY);
		}
	}

	Wall leftWall;
	leftWall.type = WallType::LEFT;
	leftWall.width = currentTilemapInfo->leftWallWidth;
	leftWall.height = canvasHeight;
	leftWall.position = glm::vec2(leftWall.width * 0.5f, canvasHeight * 0.5f);

	Wall rightWall;
	rightWall.type = WallType::RIGHT;
	rightWall.width = currentTilemapInfo->rightWallWidth;
	rightWall.height = canvasHeight;
	rightWall.position = glm::vec2(canvasWidth - (rightWall.width * 0.5f), leftWall.position.y);

	Wall topWall;
	topWall.type = WallType::TOP;
	topWall.width = canvasWidth - leftWall.width - rightWall.width;
	topWall.height = currentTilemapInfo->topWallHeight;
	topWall.position = glm::vec2((canvasWidth - rightWall.width + leftWall.width) * 0.5f, canvasHeight - (topWall.height * 0.5));

	Wall bottomWall;
	bottomWall.type = WallType::BOTTOM;
	bottomWall.width = topWall.width;
	bottomWall.height = currentTilemapInfo->bottomWallHeight;
	bottomWall.position = glm::vec2(topWall.position.x, bottomWall.height * 0.5f);

	currentTilemapInfo->walls[WallType::LEFT] = leftWall;
	currentTilemapInfo->walls[WallType::RIGHT] = rightWall;
	currentTilemapInfo->walls[WallType::TOP] = topWall;
	currentTilemapInfo->walls[WallType::BOTTOM] = bottomWall;

	Collider *colliders = currentTilemapInfo->tileColliders;
	for (int i = nonMemoryTilesStartIndex; i < totalActiveNonMemoryTiles; i++)
	{
		SetupBoxCollider(&colliders[i], EntityType::TILE, positions[i], tileSlotScaleX, tileSlotScaleY);
	}

	for (int i = goodMemoryTilesStartIndex; i < (goodMemoryTilesStartIndex + totalActiveGoodMemoryTiles); i++)
	{
		SetupCircleCollider(&colliders[i], EntityType::TILE, positions[i], tileSlotScaleY * 0.5f);
	}

	for (int i = badMemoryTilesStartIndex; i < (badMemoryTilesStartIndex + totalActiveBadMemoryTiles); i++)
	{
		SetupCircleCollider(&colliders[i], EntityType::TILE, positions[i], tileSlotScaleY * 0.5f);
	}

	Collider *wallColliders = currentTilemapInfo->wallColliders;
	Wall *walls = currentTilemapInfo->walls;
	for (int i = 0; i < WallType::WALL_COUNT; i++)
	{
		SetupBoxCollider(&wallColliders[i], EntityType::WALL, walls[i].position, walls[i].width, walls[i].height);
	}
}

void LoadCurrentLevelInfo()
{
	Game *game = GetGameMemory();
	int canvasWidth = game->canvasWidth;
	int canvasHeight = game->canvasHeight;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	LevelType levelToLoad = currentLevelInfo->nextLevelType;

	switch (levelToLoad)
	{
		case LevelType::FIRST:
		{
			LoadCurrentTilemapInfo(MapType::SUPER_EASY);

			currentLevelInfo->ballsAvailable[BallType::BASIC_BALL] = 6;
			currentLevelInfo->ballsAvailable[BallType::BUBBLE_BALL] = 0;
			currentLevelInfo->ballsAvailable[BallType::EXPLODE_BALL] = 0;

		
			currentLevelInfo->goodMemoryBoard.panel.rows = 2;
			currentLevelInfo->goodMemoryBoard.panel.cols = 3;
			currentLevelInfo->badMemoryBoard.panel.rows  = 2;
			currentLevelInfo->badMemoryBoard.panel.cols  = 3;
		}
		break;

		case LevelType::SECOND:
		{
			LoadCurrentTilemapInfo(MapType::OKAY_EASY);

			currentLevelInfo->ballsAvailable[BallType::BASIC_BALL] = 6;
			currentLevelInfo->ballsAvailable[BallType::BUBBLE_BALL] = 0;
			currentLevelInfo->ballsAvailable[BallType::EXPLODE_BALL] = 0;

			currentLevelInfo->goodMemoryBoard.panel.rows = 2;
			currentLevelInfo->goodMemoryBoard.panel.cols = 3;
			currentLevelInfo->badMemoryBoard.panel.rows = 2;
			currentLevelInfo->badMemoryBoard.panel.cols = 3;
		}
		break;

		case LevelType::THIRD:
		{
			LoadCurrentTilemapInfo(MapType::MEDIUM);

			currentLevelInfo->ballsAvailable[BallType::BASIC_BALL] = 4;
			currentLevelInfo->ballsAvailable[BallType::BUBBLE_BALL] = 1;
			currentLevelInfo->ballsAvailable[BallType::EXPLODE_BALL] = 0;

			currentLevelInfo->goodMemoryBoard.panel.rows = 2;
			currentLevelInfo->goodMemoryBoard.panel.cols = 3;
			currentLevelInfo->badMemoryBoard.panel.rows = 2;
			currentLevelInfo->badMemoryBoard.panel.cols = 3;
		}
		break;

		case LevelType::FOURTH:
		{
			LoadCurrentTilemapInfo(MapType::OKAY_HARD);

			currentLevelInfo->ballsAvailable[BallType::BASIC_BALL] = 2;
			currentLevelInfo->ballsAvailable[BallType::BUBBLE_BALL] = 1;
			currentLevelInfo->ballsAvailable[BallType::EXPLODE_BALL] = 0;

			currentLevelInfo->goodMemoryBoard.panel.rows = 2;
			currentLevelInfo->goodMemoryBoard.panel.cols = 3;
			currentLevelInfo->badMemoryBoard.panel.rows = 2;
			currentLevelInfo->badMemoryBoard.panel.cols = 3;
		}
		break;

		case LevelType::FIFTH:
		{
			LoadCurrentTilemapInfo(MapType::SUPER_HARD);

			currentLevelInfo->ballsAvailable[BallType::BASIC_BALL] = 4;
			currentLevelInfo->ballsAvailable[BallType::BUBBLE_BALL] = 0;
			currentLevelInfo->ballsAvailable[BallType::EXPLODE_BALL] = 1;

			currentLevelInfo->goodMemoryBoard.panel.rows = 2;
			currentLevelInfo->goodMemoryBoard.panel.cols = 3;
			currentLevelInfo->badMemoryBoard.panel.rows = 2;
			currentLevelInfo->badMemoryBoard.panel.cols = 3;
		}
		break;
	}



	for (int i = 0; i < MAX_BALLS; i++)
	{
		currentLevelInfo->ballSlots[i] = BallSlot::NONE_SLOT;
	}
	int currentSlotIndex = 0;
	for (int i = 0; i < BallType::BALL_TYPE_COUNT; i++)
	{
		int ballsAvailable = currentLevelInfo->ballsAvailable[i];
		for (int j = 0; j < ballsAvailable; j++)
		{
			currentLevelInfo->ballSlots[currentSlotIndex++] = (BallSlot)i;
		}
	}

	Wall *walls = game->currentTilemapInfo.walls;

	Shooter shooter;
	shooter.width = (float)32.0f;
	shooter.height = 64.0f;
	shooter.position = glm::vec2(walls[WallType::BOTTOM].position.x, walls[WallType::BOTTOM].height);
	shooter.movementSpeed = canvasWidth * 0.75f;
	shooter.tiltAngle = 0.0f;
	shooter.maxTiltAngle = 22.5f;
	shooter.tiltSpeed = 90.0f;
	shooter.minX = walls[WallType::LEFT].width + (shooter.width * 0.5f);
	shooter.maxX = canvasWidth - walls[WallType::RIGHT].width - (shooter.width * 0.5f);
	shooter.isActive = true;
	shooter.isMoving = false;
	shooter.wasMoving = false;
	shooter.changedDirection = false;
	currentLevelInfo->shooter = shooter;

	Gun gun;
	gun.position = shooter.position + glm::vec2(0.0f, shooter.height * 0.5f);
	gun.maxAngle = 75.0f;
	gun.isActive = false;
	currentLevelInfo->gun = gun;

	currentLevelInfo->ball.isActive = false;

	currentLevelInfo->currentLevelUpdate = UpdateLevelBeginMode;
	currentLevelInfo->currentLevelType = levelToLoad;

	Node **completedNodes = currentLevelInfo->completedNodes;
	for (int i = 0; i < LevelType::LEVEL_COUNT; i++)
	{
		if (i == levelToLoad)
		{
			currentLevelInfo->currentNode = completedNodes[i];
			for (int j = i + 1; j <= LevelType::LEVEL_COUNT; j++)
			{
				completedNodes[j] = 0;
			}
		}
	}

	SetupJigsawPanels();
	LoadLevelUI();
}

void LoadCurrentSceneInfo()
{
	Game *game = GetGameMemory();
	int canvasWidth = game->canvasWidth;
	int canvasHeight = game->canvasHeight;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	SceneType sceneToLoad = currentSceneInfo->nextSceneType;

	switch (sceneToLoad)
	{
		case SceneType::MAIN_MENU:
		{
			currentSceneInfo->totalUIElements = MainMenuUIElements::MAIN_MENU_UI_COUNT;
			LoadSceneMainMenuUI();
			currentSceneInfo->loadScene = LoadSceneMainMenu;
			currentSceneInfo->unloadScene = UnloadSceneMainMenu;
		}
		break;
		case SceneType::GAME:
		{
			currentSceneInfo->totalUIElements = GameUIElements::GAME_UI_COUNT;
			LoadSceneGameUI();
			LoadCurrentLevelInfo();

			currentSceneInfo->loadScene = LoadSceneGame;
			currentSceneInfo->unloadScene = UnloadSceneGame;
		}
		break;
		case SceneType::LEVEL_SELECTOR:
		{
			currentSceneInfo->totalUIElements = LevelSelectorUIElements::LEVEL_SELECTOR_UI_COUNT;
			LoadSceneLevelSelectorUI();

			currentSceneInfo->loadScene = LoadSceneLevelSelector;
			currentSceneInfo->unloadScene = UnloadSceneLevelSelector;
		}
		break;
	}

	currentSceneInfo->fadeInTime = 0.5f;
	currentSceneInfo->fadeOutTime = 0.5f;
	currentSceneInfo->currentSceneType = sceneToLoad;

	UIElement *element = &currentSceneInfo->overlayElement;
	element->parentElement = 0;
	element->type = UIElementType::PANEL;
	element->isActive = true;
	element->isInteractable = false;
	element->isMouseOver = false;
	element->isMouseDown = false;
	element->wasMouseDown = false;
	element->localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	element->width = canvasWidth * 2.0f;
	element->height = canvasHeight * 2.0f;
	element->normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	element->isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	element->isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	currentSceneInfo->loadScene();
}

void LoadBall(BallType ballToLoad)
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	glm::vec2 gunPos = currentLevelInfo->gun.position;
	Ball *ball = &currentLevelInfo->ball;

	glm::vec2 tileSize = game->currentTilemapInfo.tileSlotScale;

	switch (ballToLoad)
	{
		case BallType::BASIC_BALL:
		{
			ball->acceleration = 100.0f;
			ball->maxSpeed = 500.0f;
			ball->radius = 10.0f;
			ball->ballTileInteraction = BasicBallTileInteraction;
			ball->ballWallInteraction = BasicBallWallInteraction;
		}
		break;

		case BallType::BUBBLE_BALL:
		{
			ball->acceleration = 100.0f;
			ball->maxSpeed = 500.0f;
			ball->radius = 10.0f;
			ball->ballTileInteraction = BubbleBallTileInteraction;
			ball->ballWallInteraction = BubbleBallWallInteraction;
		}
		break;

		case BallType::EXPLODE_BALL:
		{
			ball->acceleration = 100.0f;
			ball->maxSpeed = 500.0f;
			ball->radius = 10.0f;
			ball->explode.maxImpactRadius = tileSize.x * 3.0f;
			ball->ballTileInteraction = ExplodeBallTileInteraction;
			ball->ballWallInteraction = ExplodeBallWallInteraction;
		}
		break;
	}

	ball->isMoving = false;
	ball->position = gunPos;
	ball->speed = 0;
	ball->angle = 0.0f;
	ball->type = ballToLoad;
	ball->isActive = true;
	SetupCircleCollider(&ball->collider, EntityType::BALL, ball->position, ball->radius);
}

#pragma endregion


#pragma region Loading UI

void LoadSceneMainMenuUI()
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	MainMenuUIElements element = MainMenuUIElements::MENU_CONTAINER_PANEL;
	uiElements[element].parentElement = 0;
	uiElements[element].type = UIElementType::PANEL;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	uiElements[element].width = canvasWidth * 0.60f;
	uiElements[element].height = canvasHeight * 0.60f;
	uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	

	element = MainMenuUIElements::PLAY_GAME_BUTTON;
	uiElements[element].parentElement = &uiElements[MainMenuUIElements::MENU_CONTAINER_PANEL];
	uiElements[element].type = UIElementType::BUTTON;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].scaleUpOnMouseOver = true;
	uiElements[element].localPosition = glm::vec2(0.0f, 100.0f);
	uiElements[element].width = 256.0f;
	uiElements[element].height = 128.0f;
	uiElements[element].normalColor = glm::vec4(1.0f, 0.2f, 0.5f, 0.0f);
	uiElements[element].nonInteractableColor = glm::vec4(1.0f, 0.2f, 0.5f, 0.5f);
	uiElements[element].isMouseOverColor = glm::vec4(1.0f, 0.3f, 0.6f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(1.0f, 0.3f, 0.6f, 0.0f);
	uiElements[element].text = game->renderer.gameButtonTexts[Texts::MAIN_MENU_PLAY_BUTTON];

	element = MainMenuUIElements::EXIT_GAME_BUTTON;
	uiElements[element].parentElement = &uiElements[MainMenuUIElements::MENU_CONTAINER_PANEL];
	uiElements[element].type = UIElementType::BUTTON;
	uiElements[element].scaleUpOnMouseOver = true;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(0.0f, -100.0f);
	uiElements[element].width = 256.0f;
	uiElements[element].height = 128.0f;
	uiElements[element].normalColor = glm::vec4(1.0f, 0.2f, 0.5f, 0.0f);
	uiElements[element].nonInteractableColor = glm::vec4(1.0f, 0.2f, 0.5f, 0.5f);
	uiElements[element].isMouseOverColor = glm::vec4(1.0f, 0.3f, 0.6f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(1.0f, 0.3f, 0.6f, 0.0f);
	uiElements[element].text = game->renderer.gameButtonTexts[Texts::GAME_MENU_EXIT_GAME_BUTTON];

	element = MainMenuUIElements::OVERLAY_PANEL_MAIN_MENU;
	uiElements[element].parentElement = 0;
	uiElements[element].type = UIElementType::PANEL;
	uiElements[element].isActive = false;
	uiElements[element].isInteractable = false;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	uiElements[element].width = canvasWidth;
	uiElements[element].height = canvasHeight;
	uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
}

void LoadSceneGameUI()
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	BallSlot *ballSlots = game->currentLevelInfo.ballSlots;
	UIElement *uiElements = currentSceneInfo->uiElements;

	GameUIElements element = GameUIElements::AIM_PANEL;
	uiElements[element].parentElement = 0;
	uiElements[element].type = UIElementType::PANEL;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	uiElements[element].width = canvasWidth;
	uiElements[element].height = canvasHeight;
	uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	element = GameUIElements::MENU_CONTAINER_PANEL_GAME;
	uiElements[element].parentElement = 0;
	uiElements[element].type = UIElementType::PANEL;
	uiElements[element].isActive = false;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	uiElements[element].width = canvasWidth;
	uiElements[element].height = canvasHeight;
	uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	glm::vec4 normalColorButton = glm::vec4(0.3f, 0.6f, 0.3f, 0.0f);
	glm::vec4 mouseOverColorButton = glm::vec4(0.3f, 0.7f, 0.3f, 0.0f);
	glm::vec4 mouseDownColorButton = glm::vec4(0.3f, 0.6f, 0.3f, 0.0f);

	element = GameUIElements::RESTART_LEVEL_BUTTON_GAME;
	uiElements[element].parentElement = &uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME];
	uiElements[element].type = UIElementType::BUTTON;
	uiElements[element].scaleUpOnMouseOver = true;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(-canvasWidth, canvasHeight * (2.0f / 9.0f));
	uiElements[element].width = 256.0f;
	uiElements[element].height = 128.0f;
	uiElements[element].normalColor = normalColorButton;
	uiElements[element].isMouseOverColor = mouseOverColorButton;
	uiElements[element].isMouseDownColor = mouseDownColorButton;
	uiElements[element].texture = 0;
	uiElements[element].text = game->renderer.gameButtonTexts[Texts::GAME_MENU_RELOAD_LEVEL_BUTTON];

	element = GameUIElements::LEVEL_SELECTOR_BUTTON_GAME;
	uiElements[element].parentElement = &uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME];
	uiElements[element].type = UIElementType::BUTTON;
	uiElements[element].scaleUpOnMouseOver = true;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth, canvasHeight * (0.0f / 9.0f));
	uiElements[element].width = 256.0f;
	uiElements[element].height = 128.0f;
	uiElements[element].normalColor = normalColorButton;
	uiElements[element].isMouseOverColor = mouseOverColorButton;
	uiElements[element].isMouseDownColor = mouseDownColorButton;
	uiElements[element].texture = 0;
	uiElements[element].text = game->renderer.gameButtonTexts[Texts::GAME_MENU_SELECT_LEVEL_BUTTON];

	element = GameUIElements::HOME_SCREEN_BUTTON_GAME;
	uiElements[element].parentElement = &uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME];
	uiElements[element].type = UIElementType::BUTTON;
	uiElements[element].scaleUpOnMouseOver = true;
	uiElements[element].isActive = true;
	uiElements[element].isInteractable = true;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(-canvasWidth, canvasHeight * (-2.0f / 9.0f));
	uiElements[element].width = 256.0f;
	uiElements[element].height = 128.0f;
	uiElements[element].normalColor = normalColorButton;
	uiElements[element].isMouseOverColor = mouseOverColorButton;
	uiElements[element].isMouseDownColor = mouseDownColorButton;
	uiElements[element].texture = 0;
	uiElements[element].text = game->renderer.gameButtonTexts[Texts::GAME_MENU_EXIT_GAME_BUTTON];

	element = GameUIElements::OVERLAY_PANEL_GAME;
	uiElements[element].parentElement = 0;
	uiElements[element].type = UIElementType::PANEL;
	uiElements[element].isActive = false;
	uiElements[element].isInteractable = false;
	uiElements[element].isMouseOver = false;
	uiElements[element].isMouseDown = false;
	uiElements[element].wasMouseDown = false;
	uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
	uiElements[element].width = canvasWidth;
	uiElements[element].height = canvasHeight;
	uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
}

void LoadSceneLevelSelectorUI()
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Node **completedNodes = currentLevelInfo->completedNodes;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	LevelSelectorUIElements element;

	int totalLevels = LevelType::LEVEL_COUNT;
	int rows = 2;
	int cols = 3;
	glm::vec2 showableSize = glm::vec2(1088, 492.0f);
	glm::vec2 sizePerCell = showableSize / glm::vec2(cols, rows); // 272 x 246
	//float widthPerElement = sizePerCell.x * 0.5f;
	glm::vec2 sizePerElement = glm::vec2(288.0f, 192.0f);
	glm::vec2 firstPos = glm::vec2(96.0f + sizePerCell.x / 2.0f, canvasHeight - 96.0f - (sizePerCell.y / 2.0f));

	for (int i = 0; i <= totalLevels; i++)
	{
		int row = i / cols;
		int col = i % cols;
		element = (LevelSelectorUIElements)(i + LevelSelectorUIElements::BEGIN_NODE_UI);
		uiElements[element].parentElement = 0;
		uiElements[element].type = UIElementType::BUTTON;
		uiElements[element].scaleUpOnMouseOver = true;
		uiElements[element].isActive = true;
		uiElements[element].isInteractable = false;
		uiElements[element].isMouseOver = false;
		uiElements[element].isMouseDown = false;
		uiElements[element].wasMouseDown = false;
		uiElements[element].localPosition = firstPos + glm::vec2(col * sizePerCell.x, -row * sizePerCell.y);
		uiElements[element].width = sizePerElement.x;
		uiElements[element].height = sizePerElement.y;
		uiElements[element].normalColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		uiElements[element].nonInteractableColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		uiElements[element].isMouseOverColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		uiElements[element].isMouseDownColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		uiElements[element].text = 0;

		element = LevelSelectorUIElements::OVERLAY_PANEL_LEVEL_SELECTOR;
		uiElements[element].parentElement = 0;
		uiElements[element].type = UIElementType::PANEL;
		uiElements[element].isActive = false;
		uiElements[element].isInteractable = false;
		uiElements[element].isMouseOver = false;
		uiElements[element].isMouseDown = false;
		uiElements[element].wasMouseDown = false;
		uiElements[element].localPosition = glm::vec2(canvasWidth * 0.5f, canvasHeight * 0.5f);
		uiElements[element].width = canvasWidth;
		uiElements[element].height = canvasHeight;
		uiElements[element].normalColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
		uiElements[element].isMouseOverColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
		uiElements[element].isMouseDownColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

void LoadLevelUI()
{
	Game *game = GetGameMemory();
	int canvasWidth = game->canvasWidth;
	int canvasHeight = game->canvasHeight;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;

	UIElement *uiElements = currentSceneInfo->uiElements;
	Wall rightWall = currentTilemapInfo->walls[WallType::RIGHT];


	BallSlot *ballSlots = game->currentLevelInfo.ballSlots;
	int cols = 4;
	float ballDia = 20.0f;
	float gapV = -2.0f;
	float gapH = 2.0f;
	int totalElements = GameUIElements::BALL_SELECT_BUTTON_LAST - GameUIElements::BALL_SELECT_BUTTON_FIRST + 1;
	float root = sqrtf(1.0f + (4 * 1 * 2.0f * totalElements));
	float nf = (-1 + root) * 0.5f;
	int n = ceilf(nf);

	glm::vec2 startPos = glm::vec2(canvasWidth - currentTilemapInfo->rightWallWidth - (ballDia * 0.5f) - 20.0f, currentTilemapInfo->bottomWallHeight + (ballDia * 0.5f) + 10.0f);
	int row = 0;
	int col = 0;
	for (int i = GameUIElements::BALL_SELECT_BUTTON_FIRST; i <= GameUIElements::BALL_SELECT_BUTTON_LAST; i++)
	{
		float startX = startPos.x - (((gapH + ballDia) * (row)) * 0.5f);
		uiElements[i].parentElement = 0;
		uiElements[i].type = UIElementType::BUTTON;

		if (ballSlots[i - GameUIElements::BALL_SELECT_BUTTON_FIRST] != BallSlot::NONE_SLOT)
		{
			uiElements[i].isActive = true;
		}
		else
		{
			uiElements[i].isActive = false;
		}

		uiElements[i].isInteractable = true;
		uiElements[i].isMouseOver = false;
		uiElements[i].isMouseDown = false;
		uiElements[i].wasMouseDown = false;
		uiElements[i].localPosition = glm::vec2(startX - (col * (gapH + ballDia)), startPos.y + (row * (gapV + ballDia)));
		uiElements[i].width = ballDia;
		uiElements[i].height = ballDia;
		uiElements[i].normalColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		uiElements[i].nonInteractableColor = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f);
		uiElements[i].isMouseOverColor = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		uiElements[i].isMouseDownColor = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

		col++;
		if (col >= n)
		{
			col = 0;
			row++;
			n--;
		}
	}
}

#pragma endregion


#pragma region Jigsaw

void SetupJigsawPanel(MemoryPanel *panel)
{
	int rows = panel->rows;
	int cols = panel->cols;
	int cellWidth = panel->cellSize;
	int cellHeight = panel->cellSize;
	glm::vec2 firstPos = glm::vec2(panel->position.x - (panel->panelWidth * 0.5f) + (cellWidth * 0.5f),
		panel->position.y + (panel->panelHeight * 0.5f) - (cellHeight * 0.5f));

	for (int row = 0; row < rows; row++)
	{
		for (int col = 0; col < cols; col++)
		{
			JigsawPiece piece;
			float rowFrac = ((float)row / (float)(rows - 1));
			int rowType = (rowFrac > 0.0f) ? (int)(rowFrac + 1) : 0;
			piece.rowType = (JigsawPieceRowType)rowType;

			float colFrac = ((float)col / (float)(cols - 1));
			int colType = (colFrac > 0.0f) ? (int)(colFrac + 1) : 0;
			piece.colType = (JigsawPieceColType)colType;

			piece.isActive = false;
			piece.position = firstPos + glm::vec2(col * cellWidth, -row * cellHeight);
			panel->pieces[(row * cols) + col] = piece;
		}
	}
}

void SetupJigsawPanels()
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Wall rightWall = currentTilemapInfo->walls[WallType::RIGHT];

	float cellSize = 48.0f;
	float pieceSize = 72.0f;

	Blackboard *goodMemoryBoard = &currentLevelInfo->goodMemoryBoard;
	Blackboard *badMemoryBoard = &currentLevelInfo->badMemoryBoard;

	badMemoryBoard->boardWidth = 160.0f;
	badMemoryBoard->boardHeight = 160.0f;
	badMemoryBoard->panelWidth = 144.0f;
	badMemoryBoard->panelHeight = 96;
	badMemoryBoard->position = glm::vec2(canvasWidth * 0.5f, currentTilemapInfo->bottomWallHeight) +
		glm::vec2(-badMemoryBoard->boardWidth * 0.5f, badMemoryBoard->boardHeight * 0.5f) +
		glm::vec2(-50.0f, 0.0f);
	
	goodMemoryBoard->boardWidth = 160.0f;
	goodMemoryBoard->boardHeight = 160.0f;
	goodMemoryBoard->panelWidth = 144.0f;
	goodMemoryBoard->panelHeight = 96.0f;
	goodMemoryBoard->position = glm::vec2(canvasWidth * 0.5f, currentTilemapInfo->bottomWallHeight) +
		glm::vec2(goodMemoryBoard->boardWidth * 0.5f, goodMemoryBoard->boardHeight * 0.5f) +
		glm::vec2(50.0f, 0.0f);
	
	MemoryPanel *goodMemoryPanel = &goodMemoryBoard->panel;
	MemoryPanel *badMemoryPanel = &badMemoryBoard->panel;

	badMemoryPanel->cellSize = cellSize;
	badMemoryPanel->pieceSize = pieceSize;
	badMemoryPanel->panelWidth = badMemoryBoard->panelWidth;
	badMemoryPanel->panelHeight = badMemoryBoard->panelHeight;
	badMemoryPanel->position = glm::vec2(badMemoryBoard->position.x, badMemoryBoard->position.y + 16.0f);

	goodMemoryPanel->cellSize = cellSize;
	goodMemoryPanel->pieceSize = pieceSize;
	goodMemoryPanel->panelWidth = goodMemoryBoard->panelWidth;
	goodMemoryPanel->panelHeight = goodMemoryBoard->panelHeight;
	goodMemoryPanel->position = glm::vec2(goodMemoryBoard->position.x, goodMemoryBoard->position.y + 16.0f);

	SetupJigsawPanel(goodMemoryPanel);
	SetupJigsawPanel(badMemoryPanel);
}

void UpdateJigsawPieces()
{
	Game *game = GetGameMemory();
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Tile *tiles = currentTilemapInfo->tiles;
	int totalActiveTiles = currentTilemapInfo->totalActiveTiles;

	int totalActiveGoodMemoryTiles = currentTilemapInfo->totalActiveGoodMemoryTiles;
	int totalActiveBadMemoryTiles = currentTilemapInfo->totalActiveBadMemoryTiles;

	int goodMemoryTilesStartIndex = currentTilemapInfo->goodMemoryTilesStartIndex;
	int badMemoryTilesStartIndex = currentTilemapInfo->badMemoryTilesStartIndex;

	int goodMemoryTilesEndIndex = goodMemoryTilesStartIndex + totalActiveGoodMemoryTiles - 1;
	int badMemoryTilesEndIndex = badMemoryTilesStartIndex + totalActiveBadMemoryTiles - 1;

	int totalGoodMemoryTiles = currentLevelInfo->goodMemoryBoard.panel.rows * currentLevelInfo->goodMemoryBoard.panel.cols;
	int totalBadMemoryTiles = currentLevelInfo->badMemoryBoard.panel.rows * currentLevelInfo->badMemoryBoard.panel.cols;

	JigsawPiece *goodMemoryPieces = currentLevelInfo->goodMemoryBoard.panel.pieces;
	JigsawPiece *badMemoryPieces = currentLevelInfo->badMemoryBoard.panel.pieces;

	for (int i = 0; i < totalGoodMemoryTiles; i++)
	{
		goodMemoryPieces[i].isActive = true;
	}

	for (int i = 0; i < totalBadMemoryTiles; i++)
	{
		badMemoryPieces[i].isActive = true;
	}

	for (int i = goodMemoryTilesStartIndex; i <= goodMemoryTilesEndIndex; i++)
	{
		goodMemoryPieces[tiles[i].memory.pieceIndex].isActive = false;
	}

	for (int i = badMemoryTilesStartIndex; i <= badMemoryTilesEndIndex; i++)
	{
		badMemoryPieces[tiles[i].memory.pieceIndex].isActive = false;
	}
}

#pragma endregion

void ChangeScenes()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	currentSceneInfo->onSceneUnloadComplete = OnSceneUnloaded;

	currentSceneInfo->unloadScene();
}

void RemoveTiles(int count, int *indices)
{
	Game *game = GetGameMemory();
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;

	Tile *tiles = currentTilemapInfo->tiles;
	glm::vec2 *positions = currentTilemapInfo->positions;
	TileType *types = currentTilemapInfo->types;
	Collider *colliders = currentTilemapInfo->tileColliders;

	int totalActiveTiles = currentTilemapInfo->totalActiveTiles;
	int currentEmptyIndex;
	int fromIndex;
	int totalTilesToMove;
	float size;

	/* Tile struct. */
	size = sizeof(Tile);

	for (int i = 0; i < count; i++)
	{
		currentEmptyIndex = indices[i] - i;
		fromIndex = indices[i] + 1;
		if (i == count - 1)
		{
			totalTilesToMove = totalActiveTiles - indices[i] - 1;
		}
		else
		{
			totalTilesToMove = indices[i + 1] - indices[i] - 1;
		}
		memmove(&tiles[currentEmptyIndex], &tiles[fromIndex], totalTilesToMove * size);
	}

	/* Positions struct. */
	size = sizeof(glm::vec2);

	for (int i = 0; i < count; i++)
	{
		currentEmptyIndex = indices[i] - i;
		fromIndex = indices[i] + 1;
		if (i == count - 1)
		{
			totalTilesToMove = totalActiveTiles - indices[i] - 1;
		}
		else
		{
			totalTilesToMove = indices[i + 1] - indices[i] - 1;
		}
		memmove(&positions[currentEmptyIndex], &positions[fromIndex], totalTilesToMove * size);
	}

	/* Types struct. */
	size = sizeof(TileType);

	for (int i = 0; i < count; i++)
	{
		currentEmptyIndex = indices[i] - i;
		fromIndex = indices[i] + 1;
		if (i == count - 1)
		{
			totalTilesToMove = totalActiveTiles - indices[i] - 1;
		}
		else
		{
			totalTilesToMove = indices[i + 1] - indices[i] - 1;
		}
		memmove(&types[currentEmptyIndex], &types[fromIndex], totalTilesToMove * size);
	}

	/* Collider struct. */
	size = sizeof(Collider);

	for (int i = 0; i < count; i++)
	{
		currentEmptyIndex = indices[i] - i;
		fromIndex = indices[i] + 1;
		if (i == count - 1)
		{
			totalTilesToMove = totalActiveTiles - indices[i] - 1;
		}
		else
		{
			totalTilesToMove = indices[i + 1] - indices[i] - 1;
		}
		memmove(&colliders[currentEmptyIndex], &colliders[fromIndex], totalTilesToMove * size);
	}

	int totalActiveNonMemoryTiles = currentTilemapInfo->totalActiveNonMemoryTiles;
	int totalActiveGoodMemoryTiles = currentTilemapInfo->totalActiveGoodMemoryTiles;
	int totalActiveBadMemoryTiles = currentTilemapInfo->totalActiveBadMemoryTiles;
	int goodMemoryTilesStartIndex = currentTilemapInfo->goodMemoryTilesStartIndex;
	int badMemoryTilesStartIndex = currentTilemapInfo->badMemoryTilesStartIndex;
	for (int i = 0; i < count; i++)
	{
		int index = indices[i];
		if (index < currentTilemapInfo->goodMemoryTilesStartIndex)
		{
			totalActiveNonMemoryTiles--;

			goodMemoryTilesStartIndex--;
			badMemoryTilesStartIndex--;
		}
		else if (index >= currentTilemapInfo->goodMemoryTilesStartIndex && index < currentTilemapInfo->badMemoryTilesStartIndex)
		{
			totalActiveGoodMemoryTiles--;

			badMemoryTilesStartIndex--;
		}
		else if (index >= currentTilemapInfo->badMemoryTilesStartIndex)
		{
			totalActiveBadMemoryTiles--;
		}
	}

	totalActiveTiles = totalActiveNonMemoryTiles + totalActiveGoodMemoryTiles + totalActiveBadMemoryTiles;

	currentTilemapInfo->totalActiveTiles = totalActiveTiles;
	currentTilemapInfo->totalActiveNonMemoryTiles = totalActiveNonMemoryTiles;
	currentTilemapInfo->totalActiveGoodMemoryTiles = totalActiveGoodMemoryTiles;
	currentTilemapInfo->totalActiveBadMemoryTiles = totalActiveBadMemoryTiles;
	currentTilemapInfo->goodMemoryTilesStartIndex = goodMemoryTilesStartIndex;
	currentTilemapInfo->badMemoryTilesStartIndex = badMemoryTilesStartIndex;
}

void BounceBall(CollisionInfo info)
{
	Ball *ball = &GetGameMemory()->currentLevelInfo.ball;
	ball->position -= info.penetration;
	ball->direction = ball->direction - (2 * (glm::dot(info.impactSurfaceNormal, ball->direction)) * info.impactSurfaceNormal);
}

void DamageTile(int index, int *tilesToRemove, int *tempMemory)
{
	Game *game = GetGameMemory();
	SoundPlayer *soundPlayer = &game->soundPlayer;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Tile *tiles = currentTilemapInfo->tiles;
	TileType *types = currentTilemapInfo->types;

	TileType type = types[index];
	
	if (type != TileType::RIGID)
	{
		tiles[index].hitsLeft--;

		switch (type)
		{
		case TileType::SINGLE_HIT:
			PlaySound(&soundPlayer->snowBreak);
			break;

		case TileType::DOUBLE_HIT:
			if (tiles[index].hitsLeft == 0)
			{
				PlaySound(&soundPlayer->iceBreak2);
			}
			else
			{
				PlaySound(&soundPlayer->iceBreak1);
			}
			break;

		case TileType::GOOD_MEMORY:
		case TileType::BAD_MEMORY:
			PlaySound(&soundPlayer->ballToGoodMemoryTile);
			break;

		default:
			break;
		}

		if (tiles[index].hitsLeft == 0)
		{
			*(tempMemory + (*tilesToRemove)++) = index;

			glm::vec2 position = currentTilemapInfo->positions[index];
			glm::vec2 scale = currentTilemapInfo->tileSlotScale;
			AddTileToDestroy(type, position, scale);
		}
	}
}

#pragma region Ball Tile Wall Interactions

void BasicBallTileInteraction(CollisionInfo info)
{
	Game *game = GetGameMemory();
	BounceBall(info);
	int *tempMemory = (int *)GetTempMemory();
	int tilesToRemove = 0;

	DamageTile(info.index, &tilesToRemove, tempMemory);
	RemoveTiles(tilesToRemove, tempMemory);

	//CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	//if (!currentTilemapInfo->totalActiveGoodMemoryTiles || !currentTilemapInfo->totalActiveBadMemoryTiles)
	//{
	//	OnLevelComplete();
	//}
}

void BubbleBallTileInteraction(CollisionInfo info)
{
	int *tempMemory = (int *)GetTempMemory();
	*tempMemory = info.index;

	Game *game = GetGameMemory();
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	TileType type = currentTilemapInfo->types[info.index];

	glm::vec2 position = currentTilemapInfo->positions[info.index];
	glm::vec2 scale = currentTilemapInfo->tileSlotScale;
	AddTileToDestroy(type, position, scale);

	RemoveTiles(1, tempMemory);
}

void ExplodeBallTileInteraction(CollisionInfo info)
{
	Game *game = GetGameMemory();
	SoundPlayer *soundPlayer = &GetGameMemory()->soundPlayer;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	currentLevelInfo->ball.isActive = false;
	currentLevelInfo->ball.explode.currentImpactRadius = 0.0f;
	currentLevelInfo->currentLevelUpdate = UpdateLevelBallExplodeMode;
	InitiateParticleEmitter(ParticleSystems::BALL_EXPLOSION);

	game->renderer.shakeTime = game->renderer.maxShakeTime;
	PlaySound(&soundPlayer->explosion);
}

void BasicBallWallInteraction(CollisionInfo info)
{
	if (info.index == WallType::BOTTOM)
	{
		OnBallFinished();
	}
	else
	{
		BounceBall(info);
	}
}

void BubbleBallWallInteraction(CollisionInfo info)
{
	OnBallFinished();
}

void ExplodeBallWallInteraction(CollisionInfo info)
{
	Game *game = GetGameMemory();
	SoundPlayer *soundPlayer = &GetGameMemory()->soundPlayer;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	currentLevelInfo->ball.isActive = false;
	currentLevelInfo->ball.explode.currentImpactRadius = 0.0f;
	currentLevelInfo->currentLevelUpdate = UpdateLevelBallExplodeMode;
	InitiateParticleEmitter(ParticleSystems::BALL_EXPLOSION);

	game->renderer.shakeTime = game->renderer.maxShakeTime;
	PlaySound(&soundPlayer->explosion);
}

#pragma endregion


#pragma region Events

void OnSceneUnloaded()
{
	GetGameMemory()->currentSceneInfo.onSceneUnloadComplete = 0;
	EndParticleEmitters();
	LoadCurrentSceneInfo();
}

void OnLevelUnloaded()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	LevelType currentLevelType = currentLevelInfo->currentLevelType;

	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;

	if (currentTilemapInfo->totalActiveBadMemoryTiles == 0)
	{
		currentLevelInfo->completedNodes[currentLevelType+1] = currentLevelInfo->currentNode->optionNeg;
		currentLevelInfo->currentNode = currentLevelInfo->currentNode->optionNeg;
	}
	else if(currentTilemapInfo->totalActiveGoodMemoryTiles == 0)
	{
		currentLevelInfo->completedNodes[currentLevelType + 1] = currentLevelInfo->currentNode->optionPos;
		currentLevelInfo->currentNode = currentLevelInfo->currentNode->optionPos;
	}
}

void OnLevelComplete()
{
	Game *game = GetGameMemory();
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;

	Ball *ball = &currentLevelInfo->ball;
	ball->isActive = false;
	ball->ballTileInteraction = 0;
	ball->ballWallInteraction = 0;

	game->currentSceneInfo.nextSceneType = SceneType::LEVEL_SELECTOR;
	ChangeScenes();
}

void OnBallFinished()
{
	CurrentLevelInfo *currentLevelInfo = &GetGameMemory()->currentLevelInfo;
	Ball *ball = &currentLevelInfo->ball;
	BallSlot *ballSlots = currentLevelInfo->ballSlots;
	ball->isActive = false;
	for (int i = 0; i < MAX_BALLS; i++)
	{
		BallSlot slot = ballSlots[i];
		if (slot != BallSlot::NONE_SLOT)
		{
			UpdateBallSlotsAndUI(i);
			LoadBall((BallType)slot);
			currentLevelInfo->currentLevelUpdate = UpdateLevelAimMode;
			return;
		}
	}

	OnBallsFinished();
}

void OnBallsFinished()
{
	Game *game = GetGameMemory();
	game->currentSceneInfo.nextSceneType = SceneType::GAME;
	game->currentLevelInfo.nextLevelType = game->currentLevelInfo.currentLevelType;
	ChangeScenes();
}

void OnShoot()
{
	Game *game = GetGameMemory();
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	Shooter *shooter = &currentLevelInfo->shooter;
	Gun *gun = &currentLevelInfo->gun;
	Ball *ball = &currentLevelInfo->ball;

	ball->direction = gun->aimDirection;
	gun->isActive = false;

	BallSlot *ballSlots = currentLevelInfo->ballSlots;
	for (int i = 0; i < MAX_BALLS; i++)
	{
		if (ballSlots[i] == BallSlot::CURRENT_LOADED)
		{
			ballSlots[i] = BallSlot::NONE_SLOT;
			break;
		}
	}

	currentLevelInfo->ballsAvailable[ball->type]--;

	currentLevelInfo->currentLevelUpdate = UpdateLevelBallMoveMode;
}

#pragma endregion

void InitAudio()
{
	SoundPlayer *soundPlayer = &GetGameMemory()->soundPlayer;
	int tempSize;
	void *tempMemory = GetTempMemory(&tempSize);
	std::cout << tempSize << std::endl;
	CreateSound(&soundPlayer->background, "Resources/Audio/background.wav", 1, 0.25f, glm::vec3(0), glm::vec3(0), 1, tempMemory, tempSize);
	CreateSound(&soundPlayer->ballToGoodMemoryTile, "Resources/Audio/ballToGoodMemoryTile.wav", 1, 0.05f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	CreateSound(&soundPlayer->ballToBadMemoryTile, "Resources/Audio/ballToGoodMemoryTile.wav", 1, 0.05f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	CreateSound(&soundPlayer->explosion, "Resources/Audio/explosion.wav", 1, 0.25f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	CreateSound(&soundPlayer->iceBreak1, "Resources/Audio/iceBreak1.wav", 1, 0.25f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	CreateSound(&soundPlayer->iceBreak2, "Resources/Audio/iceBreak2.wav", 1, 0.25f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	CreateSound(&soundPlayer->snowBreak, "Resources/Audio/snowBreak.wav", 1, 0.25f, glm::vec3(0), glm::vec3(0), 0, tempMemory, tempSize);
	//PlaySound(&soundPlayer->explosion);
	PlaySound(&soundPlayer->background);
}

void InitGame()
{
	Game *game = GetGameMemory();
	game->pickedBall = false;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	currentSceneInfo->nextSceneType = SceneType::MAIN_MENU;
	currentLevelInfo->nextLevelType = LevelType::THIRD;
	LoadCurrentSceneInfo();

	InitRenderer();
	InitAudio();
}

void UpdateGame()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	float time = game->currentTime;
	FontData *fontData = &game->renderer.fontData;
	HwInput hwInput = game->hwInput;
	Renderer *renderer = &game->renderer;

	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;

	ProcessMouseInput(currentSceneInfo->uiElements, currentSceneInfo->totalUIElements, hwInput.mouse, hwInput.mouseX, hwInput.mouseY);
	UpdateWorldPositionsAll(currentSceneInfo->uiElements, currentSceneInfo->totalUIElements);
	currentSceneInfo->updateScene();

	RenderText(game->renderer.shaderIds[Shaders::FONT_SHADER], fontData, "fps: " + std::to_string((int)(game->frameRateAvg)),
		game->canvasWidth - 100, game->canvasHeight - 50, 50.0f / 128, glm::vec3(0.1f, 0.8f, 0.3f), 15);

	RenderOverlayPanel(renderer, &currentSceneInfo->overlayElement);
}

void UpdateWorldPositionsAll(UIElement *uiElements, int totalElements)
{
	for (int i = 0; i < totalElements; i++)
	{
		if (!uiElements[i].parentElement)
		{
			uiElements[i].worldPosition = uiElements[i].localPosition;
		}
		else
		{
			uiElements[i].worldPosition = uiElements[i].localPosition + uiElements[i].parentElement->worldPosition;
		}
	}
}

void ProcessMouseInput(UIElement *uiElements, int totalElements, HwButton mouseButtonInput, float mouseX, float mouseY)
{
	bool isMouseOver = false;

	for (int i = totalElements - 1; i >= 0; i--)
	{
		if (uiElements[i].isMouseDown)
		{
			uiElements[i].wasMouseDown = true;
		}
		else
		{
			uiElements[i].wasMouseDown = false;
		}

		if (!isMouseOver)
		{
			if (uiElements[i].isActive && uiElements[i].isInteractable)
			{
				if ((mouseX < (uiElements[i].worldPosition.x + (uiElements[i].width * 0.5f))) &&
					(mouseX > (uiElements[i].worldPosition.x - (uiElements[i].width * 0.5f))) &&
					(mouseY > (uiElements[i].worldPosition.y - (uiElements[i].height * 0.5f))) &&
					(mouseY < (uiElements[i].worldPosition.y + (uiElements[i].height * 0.5f))))
				{
					uiElements[i].isMouseOver = true;
					isMouseOver = true;
					uiElements[i].isMouseDown = mouseButtonInput.isPressed;
				}
				else
				{
					uiElements[i].isMouseOver = false;
					uiElements[i].isMouseDown = false;
				}
			}
		}
		else
		{
			uiElements[i].isMouseOver = false;
			uiElements[i].isMouseDown = false;
		}
	}
}

int FadeOut(float time, float deltaTime, UIElement *uiElement)
{
	float alpha = uiElement->normalColor.a + (deltaTime / time);
	if (alpha >= 1.0f)
	{
		uiElement->normalColor.a = 1.0f;
		return 1;
	}

	uiElement->normalColor.a = alpha;
	return 0;
}

int FadeIn(float time, float deltaTime, UIElement *uiElement)
{
	float alpha = uiElement->normalColor.a - (deltaTime / time);
	if (alpha <= 0.0f)
	{
		uiElement->normalColor.a = 0.0f;
		return 1;
	}

	uiElement->normalColor.a = alpha;
	return 0;
}

BallType UpdateBallSlotsAndUI(int ballSlotIndex)
{
	Game *game = GetGameMemory();
	UIElement *uiElements = game->currentSceneInfo.uiElements;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	BallSlot *ballSlots = currentLevelInfo->ballSlots;

	for (int i = 0; i < MAX_BALLS; i++)
	{
		if (ballSlots[i] == BallSlot::CURRENT_LOADED)
		{
			ballSlots[i] = (BallSlot)currentLevelInfo->ball.type;
			uiElements[i + GameUIElements::BALL_SELECT_BUTTON_FIRST].isActive = true;
			break;
		}
	}

	BallType ballToLoad = (BallType)ballSlots[ballSlotIndex];
	ballSlots[ballSlotIndex] = BallSlot::CURRENT_LOADED;
	uiElements[ballSlotIndex + GameUIElements::BALL_SELECT_BUTTON_FIRST].isActive = false;

	return ballToLoad;
}

void MoveShooter(Shooter *shooter, glm::vec2 direction, float deltaTime)
{
	if ((direction.x < 0.0f && shooter->direction.x < 0.0f) || (direction.x > 0.0f && shooter->direction.x > 0.0f))
	{
		shooter->changedDirection = false;
	}
	else
	{
		shooter->changedDirection = true;
	}

	shooter->direction = direction;
	glm::vec2 newPos = shooter->position + (shooter->movementSpeed * direction * deltaTime);

	if (newPos.x < shooter->minX)
	{
		if (!shooter->isMoving)
			shooter->wasMoving = false;
		shooter->isMoving = false;

		newPos.x = shooter->minX;
	}
	else if (newPos.x > shooter->maxX)
	{
		if (!shooter->isMoving)
			shooter->wasMoving = false;
		shooter->isMoving = false;

		newPos.x = shooter->maxX;
	}
	else
	{
		if (shooter->isMoving)
			shooter->wasMoving = true;
		shooter->isMoving = true;

		if (direction.x > 0.0f)
		{
			shooter->tiltAngle += (shooter->tiltSpeed * deltaTime);
		}
		else
		{
			shooter->tiltAngle -= (shooter->tiltSpeed * deltaTime);
		}
	}

	shooter->tiltAngle = clamp(shooter->tiltAngle, -shooter->maxTiltAngle, shooter->maxTiltAngle);
	shooter->position = newPos;
}

void TakeAim(Shooter *shooter, Gun *gun, float mouseX, float mouseY)
{
	glm::vec2 aimDirection = glm::vec2(mouseX, mouseY) - gun->position;
	aimDirection = glm::normalize(aimDirection);

	glm::vec2 base = glm::vec2(0.0f, 1.0f);
	float angle = glm::degrees(glm::acos(glm::dot(aimDirection, base)));

	if (aimDirection.x > 0.0f)
	{
		angle *= -1.0f;
	}

	if (angle >= -gun->maxAngle && angle <= gun->maxAngle)
	{
		gun->aimDirection = aimDirection;
	}

	gun->angle = clamp(angle, -gun->maxAngle, gun->maxAngle);
}

#pragma region UpdateLevelMethods

void UpdateLevelBallExplodeMode()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	int *tempMemory = (int *)GetTempMemory();
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	int totalActiveTiles = currentTilemapInfo->totalActiveTiles;
	glm::vec2 *positions = currentTilemapInfo->positions;
	Ball *ball = &game->currentLevelInfo.ball;
	Shooter *shooter = &game->currentLevelInfo.shooter;
	glm::vec2 ballPosition = ball->position;
	float maxImpactRadius = ball->explode.maxImpactRadius;
	ball->explode.currentImpactRadius = ball->explode.currentImpactRadius + (deltaTime * maxImpactRadius * 1.0f);
	ball->explode.currentImpactRadius = clamp(ball->explode.currentImpactRadius, 0.0f, maxImpactRadius);

	if (!shooter->isMoving)
		shooter->wasMoving = false;
	shooter->isMoving = false;

	if (shooter->tiltAngle < 0.0f)
	{
		shooter->tiltAngle += (shooter->tiltSpeed * deltaTime);
		shooter->tiltAngle = clamp(shooter->tiltAngle, -shooter->maxTiltAngle, 0.0f);
	}
	else if (shooter->tiltAngle > 0.0f)
	{
		shooter->tiltAngle -= (shooter->tiltSpeed * deltaTime);
		shooter->tiltAngle = clamp(shooter->tiltAngle, 0.0f, shooter->maxTiltAngle);
	}

	int tilesToRemove = 0;

	for (int i = 0; i < totalActiveTiles; i++)
	{
		float distance = glm::distance(ballPosition, positions[i]);
		if (distance <= ball->explode.currentImpactRadius)
		{
			*(tempMemory + tilesToRemove) = i;
			tilesToRemove++;

			TileType type = currentTilemapInfo->types[i];
			glm::vec2 position = currentTilemapInfo->positions[i];
			glm::vec2 scale = currentTilemapInfo->tileSlotScale;
			AddTileToDestroy(type, position, scale);
		}
	}

	RemoveTiles(tilesToRemove, tempMemory);
	UpdateJigsawPieces();

	if (ball->explode.currentImpactRadius >= maxImpactRadius)
	{
		int totalActiveGoodMemoryTiles = currentTilemapInfo->totalActiveGoodMemoryTiles;
		int totalActiveBadMemoryTiles = currentTilemapInfo->totalActiveBadMemoryTiles;
		if ((totalActiveGoodMemoryTiles * totalActiveBadMemoryTiles == 0) && (totalActiveGoodMemoryTiles != totalActiveBadMemoryTiles))
		{
			currentLevelInfo->levelChangeBeginTime = 0.0f;
			currentLevelInfo->currentLevelUpdate = UpdateLevelChangeLevelMode;
		}
		else
		{
			OnBallFinished();
		}
	}
}

void UpdateLevelChangeLevelMode()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	Ball *ball = &currentLevelInfo->ball;

	float speed = ball->speed + (ball->acceleration * deltaTime);
	ball->speed = clamp(speed, 0.0f, ball->maxSpeed);
	ball->position += (ball->direction * ball->speed * deltaTime);
	ball->angle += (ball->speed * 2.0f * deltaTime);
	ball->collider.circle.centre = ball->position;
	
	currentLevelInfo->levelChangeBeginTime += deltaTime;

	if (currentLevelInfo->levelChangeBeginTime >= 2.0f)
	{
		currentLevelInfo->levelChangeBeginTime = 0.0f;
		OnLevelComplete();
	}
}

void UpdateLevelBallMoveMode()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Ball *ball = &currentLevelInfo->ball;
	Shooter *shooter = &currentLevelInfo->shooter;

	float speed = ball->speed + (ball->acceleration * deltaTime);
	ball->speed = clamp(speed, 0.0f, ball->maxSpeed);
	ball->position += (ball->direction * ball->speed * deltaTime);
	ball->angle += (ball->speed * 2.0f * deltaTime);
	ball->collider.circle.centre = ball->position;

	if (!shooter->isMoving)
		shooter->wasMoving = false;
	shooter->isMoving = false;

	if (shooter->tiltAngle < 0.0f)
	{
		shooter->tiltAngle += (shooter->tiltSpeed * deltaTime);
		shooter->tiltAngle = clamp(shooter->tiltAngle, -shooter->maxTiltAngle, 0.0f);
	}
	else if (shooter->tiltAngle > 0.0f)
	{
		shooter->tiltAngle -= (shooter->tiltSpeed * deltaTime);
		shooter->tiltAngle = clamp(shooter->tiltAngle, 0.0f, shooter->maxTiltAngle);
	}

	CollisionInfo collisionInfo;
	if (DoCollisions(ball->collider,
		currentTilemapInfo->tileColliders, currentTilemapInfo->totalActiveTiles,
		currentTilemapInfo->wallColliders, WallType::WALL_COUNT, &collisionInfo))
	{
		if (collisionInfo.entityType == EntityType::TILE)
		{
			if (ball->ballTileInteraction)
			{
				ball->ballTileInteraction(collisionInfo);
			}

			UpdateJigsawPieces();
		}
		else if (collisionInfo.entityType == EntityType::WALL)
		{
			if (ball->ballWallInteraction)
			{
				ball->ballWallInteraction(collisionInfo);
			}
		}
	}

	int totalActiveGoodMemoryTiles = currentTilemapInfo->totalActiveGoodMemoryTiles;
	int totalActiveBadMemoryTiles = currentTilemapInfo->totalActiveBadMemoryTiles;
	if ((totalActiveGoodMemoryTiles * totalActiveBadMemoryTiles == 0) && (totalActiveGoodMemoryTiles != totalActiveBadMemoryTiles))
	{
		currentLevelInfo->levelChangeBeginTime = 0.0f;
		currentLevelInfo->currentLevelUpdate = UpdateLevelChangeLevelMode;
		
	}
}

void UpdateLevelAimMode()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	float time = game->currentTime;
	HwInput input = game->hwInput;
	UIElement *uiElements = game->currentSceneInfo.uiElements;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;

	for (int i = GameUIElements::BALL_SELECT_BUTTON_FIRST; i <= GameUIElements::BALL_SELECT_BUTTON_LAST; i++)
	{
		if (uiElements[i].wasMouseDown && !uiElements[i].isMouseDown)
		{
			BallType ballToLoad = UpdateBallSlotsAndUI(i - GameUIElements::BALL_SELECT_BUTTON_FIRST);
			LoadBall(ballToLoad);
			break;
		}
	}

	Shooter *shooter = &currentLevelInfo->shooter;
	Ball *ball = &currentLevelInfo->ball;
	Gun *gun = &currentLevelInfo->gun;
	gun->isActive = true;
	if (input.left.isPressed)
	{
		MoveShooter(shooter, glm::vec2(-1.0f, 0.0f), deltaTime);
	}
	else if (input.right.isPressed)
	{
		MoveShooter(shooter, glm::vec2(1.0f, 0.0f), deltaTime);
	}
	else
	{
		if (!shooter->isMoving)
			shooter->wasMoving = false;
		shooter->isMoving = false;

		if (shooter->tiltAngle < 0.0f)
		{
			shooter->tiltAngle += (shooter->tiltSpeed * deltaTime);
			shooter->tiltAngle = clamp(shooter->tiltAngle, -shooter->maxTiltAngle, 0.0f);
		}
		else if (shooter->tiltAngle > 0.0f)
		{
			shooter->tiltAngle -= (shooter->tiltSpeed * deltaTime);
			shooter->tiltAngle = clamp(shooter->tiltAngle, 0.0f, shooter->maxTiltAngle);
		}
	}

	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(shooter->position, 0.0f));
	model = glm::rotate(model, glm::radians(shooter->tiltAngle), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::translate(model, glm::vec3(0.0f, shooter->height * 0.5f, 0.0f));
	
	glm::vec4 pos = model * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	//std::cout << pos.x << ", " << pos.y << std::endl;
	gun->position = glm::vec2(pos.x, pos.y);
	ball->position = gun->position;
	ball->collider.circle.centre = ball->position;
	TakeAim(shooter, gun, input.mouseX, input.mouseY);

	if (uiElements[GameUIElements::AIM_PANEL].isMouseDown)
	{
		OnShoot();
	}
}

void UpdateLevelBeginMode()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	float time = game->currentTime;
	HwInput input = game->hwInput;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	Shooter *shooter = &currentLevelInfo->shooter;

	if (input.left.isPressed)
	{
		MoveShooter(shooter, glm::vec2(-1.0f, 0.0f), deltaTime);
	}
	else if (input.right.isPressed)
	{
		MoveShooter(shooter, glm::vec2(1.0f, 0.0f), deltaTime);
	}
	else
	{
		if (!shooter->isMoving)
			shooter->wasMoving = false;
		shooter->isMoving = false;

		if (shooter->tiltAngle < 0.0f)
		{
			shooter->tiltAngle += (shooter->tiltSpeed * deltaTime);
			shooter->tiltAngle = clamp(shooter->tiltAngle, -shooter->maxTiltAngle, 0.0f);
		}
		else if(shooter->tiltAngle > 0.0f)
		{
			shooter->tiltAngle -= (shooter->tiltSpeed * deltaTime);
			shooter->tiltAngle = clamp(shooter->tiltAngle, 0.0f, shooter->maxTiltAngle);
		}
	}

	for (int i = GameUIElements::BALL_SELECT_BUTTON_FIRST; i <= GameUIElements::BALL_SELECT_BUTTON_LAST; i++)
	{
		if (uiElements[i].wasMouseDown && !uiElements[i].isMouseDown)
		{
			game->pickedBall = true;
			BallType ballToLoad = UpdateBallSlotsAndUI(i - GameUIElements::BALL_SELECT_BUTTON_FIRST);
			LoadBall(ballToLoad);
			game->currentLevelInfo.gun.isActive = true;
			game->currentLevelInfo.gun.position = shooter->position + glm::vec2(0.0f, shooter->height * 0.5f);
			game->currentLevelInfo.currentLevelUpdate = UpdateLevelAimMode;
			break;
		}
	}
}

#pragma endregion

#pragma region SceneLoadsUnloadsAndUpdates

#pragma region MainMenuScene

void LoadSceneMainMenu()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 1.0f;
	currentSceneInfo->updateScene = UpdateSceneMainMenuLoading;

	SetupNodeData();
	LoadRendererMainMenu();
}

void UpdateSceneMainMenuLoading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	float fadeInTime = currentSceneInfo->fadeInTime;

	if (FadeIn(fadeInTime, deltaTime, overlayElement))
	{
		overlayElement->isActive = false;
		currentSceneInfo->updateScene = UpdateSceneMainMenu;
	}

	LoadingRendererMainMenu();
}

void UpdateSceneMainMenu()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	UIElement *uiElements = currentSceneInfo->uiElements;
	UIElement *startGameButton = &uiElements[MainMenuUIElements::PLAY_GAME_BUTTON];
	UIElement *quitGameButton = &uiElements[MainMenuUIElements::EXIT_GAME_BUTTON];

	if (!startGameButton->isMouseDown && startGameButton->wasMouseDown)
	{
		currentSceneInfo->nextSceneType = SceneType::LEVEL_SELECTOR;
		ChangeScenes();
		//LoadCurrentSceneInfo();
	}
	else if (!quitGameButton->isMouseDown && quitGameButton->wasMouseDown)
	{
		Quit();
	}

	UpdateRendererMainMenu();
}

void UnloadSceneMainMenu()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 0.0f;
	currentSceneInfo->updateScene = UpdateSceneMainMenuUnloading;

	UnloadRendererMainMenu();
}

void UpdateSceneMainMenuUnloading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	float fadeOutTime = currentSceneInfo->fadeOutTime;

	if (FadeOut(fadeOutTime, deltaTime, overlayElement))
	{
		OnSceneTransitionComplete *onSceneUnloadComplete = game->currentSceneInfo.onSceneUnloadComplete;
		if (onSceneUnloadComplete)
		{
			onSceneUnloadComplete();
		}
	}

	UnloadingRendererMainMenu();
}

#pragma endregion


#pragma region GameScene

void LoadSceneGame()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 1.0f;
	currentSceneInfo->updateScene = UpdateSceneGameLoading;

	UpdateParticleEmitters();

	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	for (int i = 0; i <= LevelType::LEVEL_COUNT; i++)
	{
		if (currentLevelInfo->completedNodes[i] != 0)
		{
			std::cout << "Node: " << currentLevelInfo->completedNodes[i]->type << std::endl;
			std::cout << "Pos: " << currentLevelInfo->completedNodes[i]->optionPos->type << std::endl;
			std::cout << "Neg: " << currentLevelInfo->completedNodes[i]->optionNeg->type << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	}

	LoadRendererGame();
}

void UpdateSceneGameLoading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	float fadeInTime = currentSceneInfo->fadeInTime;

	if (FadeIn(fadeInTime, deltaTime, overlayElement))
	{
		overlayElement->isActive = false;
		currentSceneInfo->updateScene = UpdateSceneGame;
	}

	UpdateParticleEmitters();

	LoadingRendererGame();
}

struct LerpInfo
{
	glm::vec2 finalPos;
	int hasReached;
};

LerpInfo Lerp(glm::vec2 fromPos, glm::vec2 toPos, float speed, float deltaTime)
{
	LerpInfo info;
	glm::vec2 normalizedDir = glm::normalize(toPos - fromPos);
	float distanceToCover = speed * deltaTime;

	float currentDistance = glm::distance(fromPos, toPos);

	if (currentDistance > distanceToCover)
	{
		info.hasReached = false;
		info.finalPos = fromPos + (normalizedDir * distanceToCover);
	}
	else
	{
		info.hasReached = true;
		info.finalPos = toPos;
	}

	return info;
}

void UpdateSceneGamePausing()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	HwButton escape = game->hwInput.escape;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	int elementsDone = 0;
	for (int i = GameUIElements::RESTART_LEVEL_BUTTON_GAME; i <= GameUIElements::HOME_SCREEN_BUTTON_GAME; i++)
	{
		glm::vec2 toPos = uiElements[i].localPosition;
		toPos.x = 0.0f;
		LerpInfo lerpInfo = Lerp(uiElements[i].localPosition, toPos, 6000.0f, deltaTime);
		uiElements[i].localPosition = lerpInfo.finalPos;
		if (lerpInfo.hasReached)
		{
			elementsDone++;
		}
	}

	if (elementsDone == (GameUIElements::OVERLAY_PANEL_GAME - GameUIElements::RESTART_LEVEL_BUTTON_GAME))
	{
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = false;
		game->currentSceneInfo.updateScene = UpdateSceneGamePaused;
	}

	if (escape.isPressed && !escape.wasPressed)
	{
		game->currentSceneInfo.updateScene = UpdateSceneGameResuming;
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = true;
	}

	UpdateRendererGamePaused();
}

void UpdateSceneGamePaused()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	HwButton escape = game->hwInput.escape;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	GameUIElements element = GameUIElements::RESTART_LEVEL_BUTTON_GAME;
	if (!uiElements[element].isMouseDown && uiElements[element].wasMouseDown)
	{
		game->currentSceneInfo.nextSceneType = game->currentSceneInfo.currentSceneType;
		game->currentLevelInfo.nextLevelType = game->currentLevelInfo.currentLevelType;
		ChangeScenes();
	}

	element = GameUIElements::LEVEL_SELECTOR_BUTTON_GAME;
	if (!uiElements[element].isMouseDown && uiElements[element].wasMouseDown)
	{
		game->currentSceneInfo.nextSceneType = SceneType::LEVEL_SELECTOR;
		ChangeScenes();
	}

	element = GameUIElements::HOME_SCREEN_BUTTON_GAME;
	if (!uiElements[element].isMouseDown && uiElements[element].wasMouseDown)
	{
		game->currentSceneInfo.nextSceneType = SceneType::MAIN_MENU;
		ChangeScenes();
	}

	if (escape.isPressed && !escape.wasPressed)
	{
		game->currentSceneInfo.updateScene = UpdateSceneGameResuming;
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = true;
	}

	UpdateRendererGamePaused();
}

void UpdateSceneGameResuming()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	HwButton escape = game->hwInput.escape;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	int elementsDone = 0;
	float toPosX[3] = { -canvasWidth, canvasWidth, -canvasWidth };

	for (int i = GameUIElements::RESTART_LEVEL_BUTTON_GAME; i <= GameUIElements::HOME_SCREEN_BUTTON_GAME; i++)
	{
		glm::vec2 toPos = uiElements[i].localPosition;
		toPos.x = toPosX[i - GameUIElements::RESTART_LEVEL_BUTTON_GAME];
		LerpInfo lerpInfo = Lerp(uiElements[i].localPosition, toPos, 6000.0f, deltaTime);
		uiElements[i].localPosition = lerpInfo.finalPos;
		if (lerpInfo.hasReached)
		{
			elementsDone++;
		}
	}

	if (elementsDone == (GameUIElements::OVERLAY_PANEL_GAME - GameUIElements::RESTART_LEVEL_BUTTON_GAME))
	{
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = false;
		uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME].isActive = false;
		game->currentSceneInfo.updateScene = UpdateSceneGame;
	}

	if (escape.isPressed && !escape.wasPressed)
	{
		game->currentSceneInfo.updateScene = UpdateSceneGamePausing;
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = true;
		uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME].isActive = true;
	}

	UpdateRendererGamePaused();
}

void UpdateSceneGame()
{
	Game *game = GetGameMemory();
	HwInput *input = &game->hwInput;
	game->currentLevelInfo.currentLevelUpdate();

	if (input->escape.isPressed && !input->escape.wasPressed)
	{
		UIElement *uiElements = game->currentSceneInfo.uiElements;
		uiElements[GameUIElements::OVERLAY_PANEL_GAME].isActive = true;
		uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME].isActive = true;
		game->currentSceneInfo.updateScene = UpdateSceneGamePausing;
	}

	UpdateParticleEmitters();

	UpdateRendererGame();
}

void UnloadSceneGame()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 0.0f;
	currentSceneInfo->updateScene = UpdateSceneGameUnloading;

	UpdateParticleEmitters();

	UnloadRendererGame();
}

void UpdateSceneGameUnloading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;

	UnloadingRendererGame();

	float fadeOutTime = currentSceneInfo->fadeOutTime;

	UpdateParticleEmitters();

	if (FadeOut(fadeOutTime, deltaTime, overlayElement))
	{
		OnLevelUnloaded();
		OnSceneTransitionComplete *onSceneUnloadComplete = game->currentSceneInfo.onSceneUnloadComplete;
		if (onSceneUnloadComplete)
		{
			onSceneUnloadComplete();
		}
		return;
	}
}

#pragma endregion


#pragma region LevelSelectorScene

void LoadSceneLevelSelector()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *uiElements = currentSceneInfo->uiElements;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 1.0f;
	currentSceneInfo->updateScene = UpdateSceneLevelSelectorLoading;


	Node **completedNodes = currentLevelInfo->completedNodes;
	int isCompleted = true;

	Texture2D nodeTexture = renderer->nodeTextures[completedNodes[0]->type];
	float aspectRatio = nodeTexture.width / nodeTexture.height;
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].width = 288.0f;
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].height = 192.0f;
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].normalColor = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].isMouseOverColor = glm::vec4(1.0f);
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].isMouseDownColor = glm::vec4(1.0f);
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].isInteractable = false;
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].isActive = true;
	uiElements[LevelSelectorUIElements::BEGIN_NODE_UI].texture = &renderer->nodeTextures[completedNodes[0]->type];

	for (int i = LevelSelectorUIElements::MEMORY_FIRST; i <= LevelSelectorUIElements::MEMORY_LAST; i++)
	{
		if (completedNodes[i] != 0)
		{
			Texture2D nodeTexture = renderer->nodeTextures[completedNodes[i]->type];
			float aspectRatio = nodeTexture.width / nodeTexture.height;
			uiElements[i].width = 288.0f;
			uiElements[i].height = 192.0f;
			uiElements[i].normalColor = glm::vec4(1.0f);
			uiElements[i].isMouseOverColor = glm::vec4(1.0f);
			uiElements[i].isMouseDownColor = glm::vec4(1.0f);
			uiElements[i].isInteractable = true;
			uiElements[i].isActive = true;
			uiElements[i].texture = &renderer->nodeTextures[completedNodes[i]->type];
			isCompleted = true;
		}
		else
		{
			if (isCompleted)
			{
				uiElements[i].normalColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				uiElements[i].isMouseOverColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				uiElements[i].isMouseDownColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				uiElements[i].isInteractable = true;
				uiElements[i].isActive = true;
				uiElements[i].texture = &renderer->textures[Textures::UNLOCK];
				isCompleted = false;
			}
			else
			{
				uiElements[i].isInteractable = false;
				uiElements[i].isActive = false;
				uiElements[i].texture = 0;
				isCompleted = false;
			}
		}
	}


	LoadRendererLevelSelector();
}

void UpdateSceneLevelSelectorLoading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	float fadeInTime = currentSceneInfo->fadeInTime;

	if (FadeIn(fadeInTime, deltaTime, overlayElement))
	{
		overlayElement->isActive = false;
		currentSceneInfo->updateScene = UpdateSceneLevelSelector;
	}

	LoadingRendererLevelSelector();
}

void UpdateSceneLevelSelector()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	UIElement *uiElements = currentSceneInfo->uiElements;

	for (int i = LevelSelectorUIElements::MEMORY_FIRST; i <= LevelSelectorUIElements::MEMORY_LAST; i++)
	{
		if (!uiElements[i].isMouseDown && uiElements[i].wasMouseDown)
		{
			currentSceneInfo->nextSceneType = SceneType::GAME;
			currentLevelInfo->nextLevelType = (LevelType)(i - LevelSelectorUIElements::MEMORY_FIRST);
			//currentLevelInfo->nextLevelType = LevelType::THIRD;
			ChangeScenes();
			break;
		}
	}

	UpdateRendererLevelSelector();
}

void UnloadSceneLevelSelector()
{
	Game *game = GetGameMemory();
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;
	overlayElement->isActive = true;
	overlayElement->normalColor.a = 0.0f;
	currentSceneInfo->updateScene = UpdateSceneLevelSelectorUnloading;

	UnloadRendererLevelSelector();
}

void UpdateSceneLevelSelectorUnloading()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	CurrentSceneInfo *currentSceneInfo = &game->currentSceneInfo;
	UIElement *overlayElement = &currentSceneInfo->overlayElement;

	UnloadingRendererLevelSelector();

	float fadeOutTime = currentSceneInfo->fadeOutTime;

	if (FadeOut(fadeOutTime, deltaTime, overlayElement))
	{
		OnSceneTransitionComplete *onSceneUnloadComplete = game->currentSceneInfo.onSceneUnloadComplete;
		if (onSceneUnloadComplete)
		{
			onSceneUnloadComplete();
		}
	}
}

#pragma endregion

#pragma endregion