#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

struct FrameBuffer
{
	unsigned int fbo;
	unsigned int rbo;
	unsigned int colorTextureBuffer;
};

FrameBuffer CreateFramebuffer(int width, int height);

#endif