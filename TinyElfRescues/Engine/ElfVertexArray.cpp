#include "glad/glad.h"
#include "ElfVertexArray.h"

unsigned int CreateVAO(int size, void *data, int attributeInfo[], int attributeCount, int startIndex)
{
	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);

	/* Bind created objects it to the appropriate buffer in OpenGL. */
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	/* Send your data to the buffers in GPU. */
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

	int totalCount = 0;

	for (int i = 0; i < attributeCount; i++)
	{
		totalCount = totalCount + attributeInfo[i];
	}

	int cumulativeCount = 0;

	/* Inform OpenGL the layout of data that has been sent to the GPU. */
	for (int i = 0; i < attributeCount; i++)
	{
		glVertexAttribPointer(startIndex + i, attributeInfo[i], GL_FLOAT, GL_FALSE, totalCount * sizeof(float), (void *)(cumulativeCount * sizeof(float)));
		glEnableVertexAttribArray(startIndex + i);

		cumulativeCount += attributeInfo[i];
	}

	glBindVertexArray(0);

	return vao;
}

void DrawQuad(unsigned int vao)
{
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}