#ifndef ELF_AUDIO_H
#define ELF_AUDIO_H

#include <iostream>

#include "glm/glm.hpp"
#include "AL/al.h"
#include "AL/alc.h"

struct AudioData
{
	unsigned int bufferId;
	short audioFormat;
	short channels;
	int sampleRate;
	int byteRate;
	short bitsPerSample;
	int dataSizeInBytes;
	float duration;
};

struct SourceData
{
	unsigned int sourceId;
	float pitch;
	float gain;
	glm::vec3 position;
	glm::vec3 velocity;
	int loop;
};

struct Sound
{
	int valid;
	SourceData sourceData;
	AudioData audioData;
};

int CreateAudio(const char *filepath, AudioData *audioData, void *preAllocatedMemory, int preAllocatedSize);
void CreateSource(SourceData *sourceData, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop);
int CreateSound(Sound *sound, const char *filepath, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop, void *allocatedMem, int sizeOfAllocatedMem);
void PlaySound(Sound *sound);

#endif
