#include "Line.h"

void SetupLine(Line *line)
{
	float points[4] =
	{
		0.0f,  0.0f,
		1.0f,  0.0f
	};

	glGenVertexArrays(1, &line->vao);
	glBindVertexArray(line->vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
}

void SetupDottedLine(DottedLine *dottedLine, void *tempMemory)
{
	float segmentLength = dottedLine->segmentLength;
	float voidLength = dottedLine->voidLength;
	float lineLength = dottedLine->length;

	float chunkLength = segmentLength + voidLength;
	int totalChunks = lineLength / chunkLength;

	float normalizedSegmentLength = segmentLength / lineLength;
	float normalizedVoidLength = voidLength / lineLength;
	float normalizedChunkLength = chunkLength / lineLength;

	glm::vec2 startPoint = glm::vec2(0.0f);
	glm::vec2 *points = (glm::vec2 *)tempMemory;

	int p = 0;
	for (int i = 0; i < totalChunks; i++)
	{
		points[p].x = startPoint.x + (normalizedChunkLength * i);
		points[p].y = 0.0f;

		points[p + 1].x = points[p].x + normalizedSegmentLength;
		points[p + 1].y = 0.0f;

		p = p + 2;
	}

	points[p].x = startPoint.x + (normalizedChunkLength * totalChunks);
	points[p].y = 0.0f;
	points[p + 1].x = 1.0f;
	points[p + 1].y = 0.0f;

	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, (totalChunks + 1) * 2 * sizeof(glm::vec2), tempMemory, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	dottedLine->vao = vao;
	dottedLine->totalPoints = (totalChunks + 1) * 2;
}

void DrawDottedLine(DottedLine dottedLine, unsigned int shaderId)
{
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(dottedLine.from, 0.0f));
	model = glm::rotate(model, glm::radians(dottedLine.angle), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(dottedLine.length, 1.0f, 1.0f));

	UseShader(shaderId);
	SetUniform4x4(shaderId, "model", model);
	SetUniform4f(shaderId, "color", dottedLine.color.r, dottedLine.color.g, dottedLine.color.b, dottedLine.color.a);

	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	glBindVertexArray(dottedLine.vao);
	glLineWidth(dottedLine.thickness);
	glDrawArrays(GL_LINES, 0, dottedLine.totalPoints);
	glBindVertexArray(0);
}

void DrawLine(Line line, unsigned int shaderId)
{
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(line.from.x, line.from.y, 0.0f));
	model = glm::rotate(model, glm::radians(line.angle), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(line.length, 1.0f, 1.0f));

	UseShader(shaderId);
	SetUniform4x4(shaderId, "model", model);

	glBindVertexArray(line.vao);
	glLineWidth(line.thickness);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}