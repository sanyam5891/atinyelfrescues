#include "ElfTexture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "External/stb/stb_image.h"

void GetTexture(const char *path, Texture2D *texture)
{
	stbi_set_flip_vertically_on_load(true);

	unsigned char *data = stbi_load(path, (int*)&texture->width, (int*)&texture->height, (int*)&texture->nChannels, 0);

	if (!data)
	{
		std::cout << "Texture not found" << std::endl;
		return;
	}

	glGenTextures(1, &texture->id);

	//glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, texture->id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (texture->nChannels == 4)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}
	else if(texture->nChannels == 3)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	}
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);
}

void UseTexture2D(unsigned int id, int slot)
{
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, id);
}