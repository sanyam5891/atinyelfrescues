#ifndef ELF_SHADER_H
#define ELF_SHADER_H

#include <iostream>
#include "glad/glad.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#define SHADER_LOG 1

struct ShaderSource
{
	const char* vertexSource;
	const char* fragmentSource;
};

enum ShaderInfoLogType
{
	COMPILATION,
	LINKING
};

void GetShaderInfoLog(unsigned int shaderId, ShaderInfoLogType infoLogType, const char* shaderName);
unsigned int GetShader(const char * filepath, ShaderSource * shaderSource, void * tempMemory);
bool ParseShaderFile(const char * filepath, ShaderSource * shaderSource, void * tempMemory);
void UseShader(unsigned int id);

void SetUniform1f(unsigned int id, const char* name, float value);
void SetUniform2f(unsigned int id, const char * name, float f0, float f1);
void SetUniform1i(unsigned int id, const char* name, int value);

void SetUniform4f(unsigned int id, const char *name, float f1, float f2, float f3, float f4);
void SetUniform3f(unsigned int id, const char *name, float f1, float f2, float f3);
void SetUniform4fv(unsigned int id, const char *name, float *values);
void SetUniform4x4(unsigned id, const char *name, glm::mat4 value);

#endif