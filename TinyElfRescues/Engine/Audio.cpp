#include "Audio.h"

int GetCharVal(char c)
{
	unsigned int val = c;
	val = val << 24;
	val = val >> 24;
	return val;
}

short FileReadInt16LE(char *buffer, FILE *fileStream)
{
	fread(buffer, sizeof(char), 2, fileStream);
	short val = GetCharVal(buffer[1]);
	val = val << 8;
	val += GetCharVal(buffer[0]);
	return val;
}

int FileReadInt32LE(char *buffer, FILE *fileStream)
{
	fread(buffer, sizeof(char), 4, fileStream);
	int val = GetCharVal(buffer[3]);
	val = val << 8;
	val += GetCharVal(buffer[2]);
	val = val << 8;
	val += GetCharVal(buffer[1]);
	val = val << 8;
	val += GetCharVal(buffer[0]);
	return val;
}

int FileReadInt32LE(FILE *fileStream)
{
	char c[11];
	int count = 0;
	for (int i = 0; i < 10; i++)
	{
		char cr;
		fread(&cr, sizeof(char), 1, fileStream);

		if (cr >= '0' && cr <= '9')
		{
			c[i] = cr;
			count++;
		}
		else
		{
			c[i] = '\0';
			break;
		}
	}
	c[10] = '\0';

	int val = 0;
	for (int i = 0; i < count; i++)
	{
		int currentInt = (int)c[i] - (int)'0';
		val += (currentInt * pow(10, count - i - 1));
	}

	int result = fseek(fileStream, -1, SEEK_CUR);

	return val;
}

static inline ALenum GetFormatFromInfo(short channels, short bitsPerSample) {
	/*if (channels == 1)
		return AL_FORMAT_MONO16;
	else if (channels == 2)
		return AL_FORMAT_STEREO16;*/

	return AL_FORMAT_STEREO16;
}

int CreateAudio(const char *filepath, AudioData *audioData, void *preAllocatedMemory, int preAllocatedSize)
{
	alGenBuffers(1, &audioData->bufferId);

	// TODO: LOAD DATA TO THE BUFFER.
	FILE *audioFile;
	char xbuffer[5];
	xbuffer[4] = '\0';

	if (fopen_s(&audioFile, filepath, "rb") == 0)
	{
		if (fread(xbuffer, sizeof(char), 4, audioFile) != 4 || strcmp(xbuffer, "RIFF") != 0)
		{
			std::cout << "Not a WAV file" << std::endl;
			return 0;
		}

		fread(xbuffer, sizeof(char), 4, audioFile);

		if (fread(xbuffer, sizeof(char), 4, audioFile) != 4 || strcmp(xbuffer, "WAVE") != 0)
		{
			std::cout << "Not a WAV file" << std::endl;
			return 0;
		}


		if (fread(xbuffer, sizeof(char), 4, audioFile) != 4 || strcmp(xbuffer, "fmt ") != 0)
		{
			std::cout << "Invalid WAV file fmt" << std::endl;
			return 0;
		}

		audioData->audioFormat = FileReadInt16LE(xbuffer, audioFile);
		fread(xbuffer, sizeof(char), 4, audioFile);

		audioData->channels = FileReadInt16LE(xbuffer, audioFile);
		audioData->sampleRate = FileReadInt32LE(xbuffer, audioFile);
		audioData->byteRate = FileReadInt32LE(xbuffer, audioFile);

		fread(xbuffer, sizeof(char), 2, audioFile);

		audioData->bitsPerSample = FileReadInt16LE(xbuffer, audioFile);

		if (audioData->audioFormat != 16)
		{
			short extraParams = FileReadInt16LE(xbuffer, audioFile);
			if (preAllocatedSize < extraParams)
			{
				std::cout << "Not enough memory provided for extraParams" << std::endl;
				return 0;
			}

			fread(preAllocatedMemory, sizeof(char), extraParams, audioFile);
		}

		if (fread(xbuffer, sizeof(char), 4, audioFile) != 4 || strcmp(xbuffer, "data") != 0)
		{
			std::cout << "Invalid WAV file data" << std::endl;
			return 0;
		}

		audioData->dataSizeInBytes = FileReadInt32LE(xbuffer, audioFile);

		if (audioData->dataSizeInBytes > preAllocatedSize)
		{
			std::cout << "Not enough memory provided for AudioData" << std::endl;
			return 0;
		}

		unsigned char *data = (unsigned char *)preAllocatedMemory;

		int bytesRead = fread(data, sizeof(unsigned char), audioData->dataSizeInBytes, audioFile);
		audioData->duration = float(audioData->dataSizeInBytes) / (float)audioData->byteRate;

		alGetError();
		alBufferData(audioData->bufferId, GetFormatFromInfo(audioData->channels, audioData->bitsPerSample), data, audioData->dataSizeInBytes, audioData->sampleRate);
		ALenum error = alGetError();

		fclose(audioFile);

		return 1;
	}
	else
	{
		std::cout << "File not found." << std::endl;
		return 0;
	}
}

void CreateSource(SourceData *sourceData, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop)
{
	alGenSources(1, &sourceData->sourceId);

	alSourcef(sourceData->sourceId, AL_PITCH, pitch);
	alSourcef(sourceData->sourceId, AL_GAIN, gain);
	alSource3f(sourceData->sourceId, AL_POSITION, position.x, position.y, position.z);
	alSource3f(sourceData->sourceId, AL_VELOCITY, velocity.x, velocity.y, velocity.z);
	alSourcei(sourceData->sourceId, AL_LOOPING, loop);
}

int CreateSound(Sound *sound, const char *filepath, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop, void *allocatedMem, int sizeOfAllocatedMem)
{
	int result = CreateAudio(filepath, &sound->audioData, allocatedMem, sizeOfAllocatedMem);

	if (result)
	{
		CreateSource(&sound->sourceData, pitch, gain, position, velocity, loop);
		alSourcei(sound->sourceData.sourceId, AL_BUFFER, sound->audioData.bufferId);
		sound->valid = 1;
	}
	else
	{
		sound->valid = 0;
		std::cout << "Could not create Sound" << std::endl;
	}

	return sound->valid;
}

void PlaySound(Sound *sound)
{
	if (sound->valid)
	{
		alSourcePlay(sound->sourceData.sourceId);
	}
	else
	{
		std::cout << "Couldn't play" << std::endl;
	}
}