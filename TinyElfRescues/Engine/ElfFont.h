#ifndef ELF_FONT_H
#define ELF_FONT_H

#include <map>
#include <string>

#include "ft2build.h"
#include FT_FREETYPE_H

#include "ElfShader.h"
#include "ElfVertexArray.h"
#include "ElfTexture.h"

#define FONT_REF_SIZE 512

struct Character
{
	unsigned int textureId;
	glm::vec2 size;
	glm::vec2 bearing;
	unsigned int advance;
};

struct FontData
{
	Character characters[128];
	unsigned int VAO;
	unsigned int VBO;
};

void InitFontRenderer(FontData * fontData, int size);

void GetTextWidth(FontData * fontData, std::string text, float scale, float * width, float * heightMax);

void RenderText(unsigned int shader, FontData * fontData, std::string text, float x, float y, float scale, glm::vec3 color, int texSlot);

#endif