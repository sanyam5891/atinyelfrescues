#ifndef ELF_TEXTURE_H
#define ELF_TEXTURE_H

#include <iostream>
#include "glad/glad.h"

struct Texture2D
{
	unsigned int width;
	unsigned int height;
	unsigned int nChannels;
	unsigned int id;
};

void GetTexture(const char *path, Texture2D *texture);
void UseTexture2D(unsigned int id, int slot);

#endif