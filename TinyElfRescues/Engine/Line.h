#ifndef LINE_H
#define LINE_H

#include "glad/glad.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "ElfShader.h"

struct Line
{
	unsigned int vao;
	glm::vec2 from;
	float length;
	float angle;
	float thickness;
};

struct DottedLine
{
	unsigned int vao;
	int totalPoints;
	glm::vec2 from;
	glm::vec4 color;
	float length;
	float angle;
	float thickness;
	float segmentLength;
	float voidLength;
};

void SetupLine(Line * line);
void DrawLine(Line line, unsigned int shaderId);

void SetupDottedLine(DottedLine *dottedLine, void * tempMemory);
void DrawDottedLine(DottedLine, unsigned int shaderId);

#endif