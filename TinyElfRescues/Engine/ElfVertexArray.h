#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

unsigned int CreateVAO(int size, void * data, int attributeInfo[], int attributeCount, int startIndex);

void DrawQuad(unsigned int vao);

#endif