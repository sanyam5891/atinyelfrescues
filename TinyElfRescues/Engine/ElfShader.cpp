#include "ElfShader.h"

void GetShaderInfoLog(unsigned int shaderId, ShaderInfoLogType infoLogType, const char* shaderName)
{
	int success;
	int infoLogLength;
	char infoLog[512];

	if (infoLogType == ShaderInfoLogType::COMPILATION)
	{
		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

		if (!success)
		{
			glGetShaderInfoLog(shaderId, 512, 0, infoLog);
			infoLog[infoLogLength] = 0;
			std::cout << "ERROR::SHADER::" << shaderName << "::COMPILATION\n" << infoLog << std::endl;
		}
	}
	else if (infoLogType == ShaderInfoLogType::LINKING)
	{
		glGetProgramiv(shaderId, GL_LINK_STATUS, &success);
		glGetProgramiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

		if (!success)
		{
			glGetProgramInfoLog(shaderId, 512, 0, infoLog);
			infoLog[infoLogLength] = 0;
			std::cout << "ERROR::SHADER::" << shaderName << "::LINKING\n" << infoLog << std::endl;
		}
	}
}

unsigned int GetShader(const char* filepath, ShaderSource *shaderSource, void *tempMemory)
{
	bool result = ParseShaderFile(filepath, shaderSource, tempMemory);

	if (!result)
	{
		return -1;
	}

	unsigned int vertex;
	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &shaderSource->vertexSource, 0);
	glCompileShader(vertex);

#if SHADER_LOG
	GetShaderInfoLog(vertex, ShaderInfoLogType::COMPILATION, "Vertex");
#endif

	unsigned int fragment;
	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &shaderSource->fragmentSource, 0);
	glCompileShader(fragment);

#if SHADER_LOG
	GetShaderInfoLog(fragment, ShaderInfoLogType::COMPILATION, "Fragment");
#endif

	unsigned int shader;
	shader = glCreateProgram();
	glAttachShader(shader, vertex);
	glAttachShader(shader, fragment);
	glLinkProgram(shader);

#if SHADER_LOG
	GetShaderInfoLog(shader, ShaderInfoLogType::LINKING, "Shader");
#endif

	glDeleteShader(vertex);
	glDeleteShader(fragment);

	return shader;
}

bool ParseShaderFile(const char* filepath, ShaderSource *shaderSource, void *tempMemory)
{
	/*
		1. Scan the file to know the sizes of different shaders.
		2. Allocate memory buffer to hold source code of each shader.
		3. Scan the file to copy from file to buffer.
	*/

	int vertexSize = 0;
	int fragmentSize = 0;
	int *currentShaderSize = &vertexSize;

	FILE *file;
	fopen_s(&file, filepath, "r");

	if (!file)
	{
		std::cout << "Shader File not found." << std::endl;
		return false;
	}
	else
	{
		char lineBuffer[256];

		/*
			1. Scan the file to know the sizes of different shaders.
		*/
		int currentWriteIndex = 0;

		char ch = fgetc(file);
		while (ch != EOF)
		{
			if (ch != '\n')
			{
				lineBuffer[currentWriteIndex] = ch;
				if (currentShaderSize)
					(*currentShaderSize)++;

				currentWriteIndex++;
			}
			else
			{
				lineBuffer[currentWriteIndex] = '\0';

				if (strcmp(lineBuffer, "#VERTEX_SHADER") == 0)
				{
					*currentShaderSize = *currentShaderSize - 14;
					currentShaderSize = &vertexSize;
				}
				else if (strcmp(lineBuffer, "#FRAGMENT_SHADER") == 0)
				{
					*currentShaderSize = *currentShaderSize - 16;
					currentShaderSize = &fragmentSize;
				}
				else
				{
					if (currentShaderSize)
						(*currentShaderSize)++;
				}

				currentWriteIndex = 0;
			}

			ch = fgetc(file);
		}

		//std::cout << "Vertex: " << vertexSize << std::endl;
		//std::cout << "Fragment: " << fragmentSize << std::endl;

		/*
			2. Allocate memory buffer to hold source code of each shader.
		*/
		/*char *vertexSource = (char *)malloc(vertexSize + 20);
		char *fragmentSource = (char *)malloc(fragmentSize + 20);*/
		char *temp = (char *)tempMemory;
		char *vertexSource = temp;
		char *fragmentSource = temp + vertexSize + 20;
		/*
			3. Scan the file line-wise to copy from file to buffer.
		*/
		char line[256];

		char *writePtr = line;
		char *linePtr = line;

		fseek(file, 0, SEEK_SET);
		ch = fgetc(file);
		while (ch != EOF)
		{
			if (ch != '\n') // Line not ended.
			{
				if (writePtr)
				{
					*writePtr = ch;
					writePtr++;
				}
			}
			else // Line ended.
			{
				if (writePtr)
					*writePtr = '\0';

				if (strcmp(line, "#VERTEX_SHADER") == 0)
				{
					// Activate vertex source buffer as the current copy buffer.
					*linePtr = '\0';
					linePtr = vertexSource;
				}
				else if (strcmp(line, "#FRAGMENT_SHADER") == 0)
				{
					// Activate fragment source buffer as the current copy buffer.
					*linePtr = '\0';
					linePtr = fragmentSource;
				}
				else
				{
					// Copy contents to appropriate buffer.
					*writePtr = '\n';
					memcpy(linePtr, line, (writePtr - line + 1));
					linePtr = linePtr + (writePtr - line + 1);
				}

				writePtr = line;
			}

			ch = fgetc(file);
		}

		*writePtr = '\0';
		memcpy(linePtr, line, (writePtr - line + 1));
		linePtr = linePtr + (writePtr - line + 1);
		*linePtr = '\0';

		shaderSource->vertexSource = vertexSource;
		shaderSource->fragmentSource = fragmentSource;

#if 0
		std::cout << vertexSource << "END" << std::endl;
		std::cout << fragmentSource << "END" << std::endl;

		int sizeV = 0;
		int sizeF = 0;
		char chrV = *vertexSource;
		char chrF = *fragmentSource;

		int index = 0;
		while (chrV != '\0')
		{
			sizeV++;
			index++;
			chrV = *(vertexSource + index);
		}

		index = 0;
		while (chrF != '\0')
		{
			sizeF++;
			index++;
			chrF = *(fragmentSource + index);
		}

		std::cout << sizeV << std::endl;
		std::cout << sizeF << std::endl;
#endif
	}

	return true;
}

void UseShader(unsigned int id)
{
	glUseProgram(id);
}


void SetUniform1i(unsigned int id, const char * name, int value)
{
	glUniform1i(glGetUniformLocation(id, name), value);
}

void SetUniform1f(unsigned int id, const char * name, float value)
{
	glUniform1f(glGetUniformLocation(id, name), value);
}

void SetUniform2f(unsigned int id, const char * name, float f0, float f1)
{
	glUniform2f(glGetUniformLocation(id, name), f0, f1);
}

void SetUniform4f(unsigned int id, const char *name, float f1, float f2, float f3, float f4)
{
	glUniform4f(glGetUniformLocation(id, name), f1, f2, f3, f4);
}
void SetUniform3f(unsigned int id, const char * name, float f1, float f2, float f3)
{
	glUniform3f(glGetUniformLocation(id, name), f1, f2, f3);
}
void SetUniform4fv(unsigned int id, const char *name, float *values)
{
	glUniform4fv(glGetUniformLocation(id, name), 4, values);
}

void SetUniform4x4(unsigned id, const char *name, glm::mat4 value)
{
	glUniformMatrix4fv(glGetUniformLocation(id, name), 1, GL_FALSE, glm::value_ptr(value));
}
