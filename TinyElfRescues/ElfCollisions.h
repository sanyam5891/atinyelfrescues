#ifndef ELF_COLLISIONS_H
#define ELF_COLLISIONS_H

#include "glm/glm.hpp"



enum EntityType
{
	TILE,
	WALL,
	BALL
};

struct CollisionInfo
{
	EntityType entityType;
	unsigned int index;
	glm::vec2 impactPoint;
	glm::vec2 impactSurfaceNormal;
	glm::vec2 penetration;
};

enum ColliderType
{
	BOX,
	CIRCLE
};

struct BoxCollider
{
	glm::vec2 topLeft;
	glm::vec2 bottomRight;
};

struct CircleCollider
{
	glm::vec2 centre;
	float radius;
};

struct Collider
{
	EntityType entityType;
	ColliderType colliderType;

	union
	{
		BoxCollider box;
		CircleCollider circle;
	};
};



void SetupBoxCollider(Collider * collider, EntityType entityType, glm::vec2 centre, float width, float height);

void SetupCircleCollider(Collider * collider, EntityType entityType, glm::vec2 centre, float radius);

int DoCollisions(Collider ball, Collider * tileColliders, int totalTileColliders, Collider * wallColliders, int totalWallColliders, CollisionInfo * collisionInfo);

#endif