#ifndef ELF_GAME_H
#define ELF_GAME_H

#include <iostream>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "ElfCollisions.h"
#include "ElfIntrinsics.h"

#include "Engine/ElfShader.h"
#include "Engine/ElfVertexArray.h"
#include "Engine/ElfFramebuffer.h"
#include "Engine/ElfTexture.h"
#include "Engine/ElfFont.h"
#include "Engine/Audio.h"
#include "Engine/Line.h"

#define KB 1024
#define MB 1024*KB
#define GB 1024*MB

#define MAX_TEXTURE_SLOTS 16
#define UNBOOKED_SLOT 15

#define TILEMAP_COUNT_X 20
#define TILEMAP_COUNT_Y 10

#define MAX_BALLS 10
#define MAX_PIECES 9

#define MAX_SNOW_PARTICLES 500

#define MAX_UI_ELEMENTS 40

#define MAX_TILE_DESTROY_SIMUL 16

#define BALL_TILE_INTERACTION(name) void name(CollisionInfo info)
typedef BALL_TILE_INTERACTION(BallInteraction);

#define ON_SCENE_TRANSITION_COMPLETE(name) void name()
typedef ON_SCENE_TRANSITION_COMPLETE(OnSceneTransitionComplete);

#define UPDATE_SCENE(name) void name()
typedef UPDATE_SCENE(UpdateScene);

#define LOAD_UNLOAD_SCENE(name) void name()
typedef LOAD_UNLOAD_SCENE(LoadUnloadScene);

#define UPDATE_LEVEL(name) void name()
typedef UPDATE_LEVEL(UpdateLevel);

#define INITIALIZE_PARTICLE_EMITTER(name) void name()
typedef INITIALIZE_PARTICLE_EMITTER(InitializeEmitter);

struct HwButton
{
	bool isPressed;
	bool wasPressed;
};

struct HwInput
{
	HwButton left;
	HwButton right;
	HwButton escape;
	HwButton mouse;
	float mouseX;
	float mouseY;
};

struct Shooter
{
	glm::vec2 position;
	glm::vec2 direction;
	int changedDirection;
	int wasMoving;
	int isMoving;
	float width;
	float height;
	float movementSpeed;
	float tiltAngle;
	float maxTiltAngle;
	float tiltSpeed;
	float minX;
	float maxX;
	int isActive;
};

struct Gun
{
	glm::vec2 position;
	glm::vec2 aimDirection;
	float maxAngle;
	float angle;
	int isActive;
};


enum BallType
{
	BASIC_BALL = 0,
	BUBBLE_BALL,
	EXPLODE_BALL,
	BALL_TYPE_COUNT
};

struct Ball
{
	glm::vec2 position;
	glm::vec2 direction;
	float radius;
	float acceleration;
	float maxSpeed;
	float speed;
	float angle;
	BallType type;
	Collider collider;
	int isMoving;
	int isActive;

	union
	{
		struct Explode
		{
			float maxImpactRadius;
			float currentImpactRadius;
		} explode;
	};

	BallInteraction *ballTileInteraction;
	BallInteraction *ballWallInteraction;
};

enum LevelType
{
	FIRST = 0,
	SECOND,
	THIRD,
	FOURTH,
	FIFTH,
	LEVEL_COUNT
};

enum MainMenuUIElements
{
	MENU_CONTAINER_PANEL = 0,
	PLAY_GAME_BUTTON,
	EXIT_GAME_BUTTON,
	OVERLAY_PANEL_MAIN_MENU,
	MAIN_MENU_UI_COUNT
};

enum GameUIElements
{
	AIM_PANEL = 0,
	BALL_SELECT_BUTTON_FIRST,
	BALL_SELECT_BUTTON_LAST = BALL_SELECT_BUTTON_FIRST + MAX_BALLS - 1,
	MENU_CONTAINER_PANEL_GAME,
	RESTART_LEVEL_BUTTON_GAME,
	LEVEL_SELECTOR_BUTTON_GAME,
	HOME_SCREEN_BUTTON_GAME,
	OVERLAY_PANEL_GAME,
	GAME_UI_COUNT
};

enum LevelSelectorUIElements
{
	BEGIN_NODE_UI = 0,
	MEMORY_FIRST,
	MEMORY_LAST = MEMORY_FIRST + LevelType::LEVEL_COUNT - 1,
	OVERLAY_PANEL_LEVEL_SELECTOR,
	LEVEL_SELECTOR_UI_COUNT
};

enum UIElementType
{
	BUTTON,
	ROUND_BUTTON,
	PANEL
};

struct UIElement
{
	UIElementType type;
	int isActive;
	int isInteractable;
	int isMouseOver;
	int isMouseDown;
	int wasMouseDown;
	int scaleUpOnMouseOver;
	glm::vec4 normalColor;
	glm::vec4 nonInteractableColor;
	glm::vec4 isMouseOverColor;
	glm::vec4 isMouseDownColor;
	glm::vec2 worldPosition;
	glm::vec2 localPosition;
	float width;
	float height;
	Texture2D *texture;
	char *text;
	UIElement *parentElement;
};

enum SceneType
{
	MAIN_MENU = 0,
	GAME,
	LEVEL_SELECTOR,
	SCENE_COUNT
};


enum WallType
{
	LEFT = 0,
	TOP,
	RIGHT,
	BOTTOM,
	WALL_COUNT
};

struct Wall
{
	WallType type;
	glm::vec2 position;
	float width;
	float height;
};

enum MapType
{
	SUPER_EASY = 0,
	OKAY_EASY,
	MEDIUM,
	OKAY_HARD,
	SUPER_HARD,
	MAP_COUNT
};

enum TileType
{
	NONE = 0,
	SINGLE_HIT,
	DOUBLE_HIT,
	TRIPLE_HIT,
	RIGID,
	GOOD_MEMORY,
	BAD_MEMORY,
	TILE_COUNT
};

struct Tile
{
	unsigned int mapIndex;
	unsigned int hitsLeft;

	union
	{
		struct MemoryTile
		{
			int pieceIndex;
		} memory;
	};
};



enum JigsawPieceRowType
{
	TOP_ROW = 0,
	MID_ROW,
	BOTTOM_ROW
};

enum JigsawPieceColType
{
	LEFT_COL = 0,
	MID_COL,
	RIGHT_COL
};

struct JigsawPiece
{
	int isActive;
	float timeAlive;
	JigsawPieceRowType rowType;
	JigsawPieceColType colType;
	glm::vec2 position;
};

struct MemoryPanel
{
	int rows;
	int cols;
	int pieceSize;
	int cellSize;
	int panelWidth;
	int panelHeight;
	glm::vec2 position;
	JigsawPiece pieces[MAX_PIECES];
};

struct Blackboard
{
	glm::vec2 position;
	float boardWidth;
	float boardHeight;
	float panelWidth;
	float panelHeight;
	MemoryPanel panel;
};

enum BallSlot
{
	BASIC_BALL_SLOT = (int)BallType::BASIC_BALL,
	BUBBLE_BALL_SLOT = (int)BallType::BUBBLE_BALL,
	EXPLODE_BALL_SLOT = (int)BallType::EXPLODE_BALL,
	CURRENT_LOADED,
	NONE_SLOT
};

enum NodeType
{
	BEGIN_NODE = 0,

	LEVEL1_NODE0,
	LEVEL1_NODE1,

	LEVEL2_NODE0,
	LEVEL2_NODE1,
	LEVEL2_NODE2,
	LEVEL2_NODE3,

	LEVEL3_NODE0,
	LEVEL3_NODE1,
	LEVEL3_NODE2,
	LEVEL3_NODE3,
	LEVEL3_NODE4,
	LEVEL3_NODE5,
	LEVEL3_NODE6,
	LEVEL3_NODE7,

	LEVEL4_NODE0,
	LEVEL4_NODE1,
	LEVEL4_NODE2,
	LEVEL4_NODE3,
	LEVEL4_NODE4,
	LEVEL4_NODE5,
	LEVEL4_NODE6,
	LEVEL4_NODE7,
	LEVEL4_NODE8,
	LEVEL4_NODE9,
	LEVEL4_NODE10,
	LEVEL4_NODE11,
	LEVEL4_NODE12,
	LEVEL4_NODE13,
	LEVEL4_NODE14,
	LEVEL4_NODE15,

	LEVEL5_NODE0,
	LEVEL5_NODE1,
	LEVEL5_NODE2,
	LEVEL5_NODE3,
	LEVEL5_NODE4,
	LEVEL5_NODE5,
	LEVEL5_NODE6,
	LEVEL5_NODE7,
	LEVEL5_NODE8,
	LEVEL5_NODE9,
	LEVEL5_NODE10,
	LEVEL5_NODE11,
	LEVEL5_NODE12,
	LEVEL5_NODE13,
	LEVEL5_NODE14,
	LEVEL5_NODE15,
	LEVEL5_NODE16,
	LEVEL5_NODE17,
	LEVEL5_NODE18,
	LEVEL5_NODE19,
	LEVEL5_NODE20,
	LEVEL5_NODE21,
	LEVEL5_NODE22,
	LEVEL5_NODE23,
	LEVEL5_NODE24,
	LEVEL5_NODE25,
	LEVEL5_NODE26,
	LEVEL5_NODE27,
	LEVEL5_NODE28,
	LEVEL5_NODE29,
	LEVEL5_NODE30,
	LEVEL5_NODE31,

	NODE_COUNT
};

struct Node
{
	NodeType type;
	Node *optionPos;
	Node *optionNeg;
};

struct CurrentTilemapInfo
{
	int map[TILEMAP_COUNT_X * TILEMAP_COUNT_Y];
	Tile tiles[TILEMAP_COUNT_Y * TILEMAP_COUNT_X];
	TileType types[TILEMAP_COUNT_Y * TILEMAP_COUNT_X];
	glm::vec2 positions[TILEMAP_COUNT_Y * TILEMAP_COUNT_X];
	Wall walls[WallType::WALL_COUNT];

	Collider tileColliders[TILEMAP_COUNT_X * TILEMAP_COUNT_Y];
	Collider wallColliders[WallType::WALL_COUNT];

	MapType mapType;

	float sideGap;
	float topGap;
	float tileSpacingX;
	float tileSpacingY;
	int countX;
	int countY;
	float tileAspectRatio;
	float leftWallWidth;
	float rightWallWidth;
	float topWallHeight;
	float bottomWallHeight;

	glm::vec2 topLeft;
	glm::vec2 topRight;
	glm::vec2 tileSlotScale;

	int totalPossibleTiles;
	int totalActiveTiles;

	int totalActiveNonMemoryTiles;
	int nonMemoryTilesStartIndex;

	int totalActiveGoodMemoryTiles;
	int goodMemoryTilesStartIndex;

	int totalActiveBadMemoryTiles;
	int badMemoryTilesStartIndex;

	int stageRows;
	int stageCols;
};

struct CurrentLevelInfo
{
	int ballsAvailable[BallType::BALL_TYPE_COUNT];
	BallSlot ballSlots[MAX_BALLS];
	Shooter shooter;
	Gun gun;
	Ball ball;

	Blackboard goodMemoryBoard;
	Blackboard badMemoryBoard;
	
	int currentBallSlot;

	LevelType currentLevelType;
	LevelType nextLevelType;

	float levelChangeBeginTime;
	UpdateLevel *currentLevelUpdate;

	Node *currentNode;
	Node nodes[NodeType::NODE_COUNT];
	Node *completedNodes[LevelType::LEVEL_COUNT + 1];
};

struct CurrentSceneInfo
{
	float fadeOutTime;
	float fadeInTime;
	int totalUIElements;
	UIElement uiElements[MAX_UI_ELEMENTS];
	UIElement overlayElement;
	LoadUnloadScene *loadScene;
	LoadUnloadScene *unloadScene;
	UpdateScene *updateScene;
	OnSceneTransitionComplete *onSceneUnloadComplete;
	OnSceneTransitionComplete *onSceneLoadComplete;
	SceneType currentSceneType;
	SceneType nextSceneType;
};

struct SpritesheetVertexBufferData
{
	unsigned int vao;
	unsigned int vboI;
	unsigned int vboNI;
};

struct SpritesheetRenderData
{
	glm::mat4 model;
	int index;
};

struct ParticleVertexBufferData
{
	unsigned int vao;
	unsigned int vbo;
};

struct ParticleRenderData
{
	glm::mat4 model;
	glm::vec2 stage;
};

enum Textures
{
	BACKGROUND_TEX = 0,
	SHOOTER_TEX,
	GUN_TEX,
	BASIC_BALL_TEX,
	BUBBLE_BALL_TEX,
	EXPLODE_BALL_TEX,
	WALL_TEX,
	TREE_TEX,
	TILE_TEXES,
	JIGSAW_TEXES,
	BOX_COLLIDER_TEX,
	CIRCLE_COLLIDER_TEX,
	EXPLODE_PARTICLE_TEXES,
	BALLS_TEXES,
	SNOW_SPLASH_TEXES,
	SNOWFLAKE_TEXES,
	BLACKBOARD_TEX,
	JIGSAW_UI_TEXES,
	UNLOCK,
	TEX_COUNT
};

enum Shaders
{
	FONT_SHADER = 0,
	PARTICLE_PLAIN_SHADER,
	PARTICLE_TEXTURED_SHADER,
	QUAD_TEXTURED_SHADER,
	UI_PLAIN_SHADER,
	UI_TEXTURED_SHADER,
	TILEMAP_SHADER,
	MEMORY_PIECE_SHADER,
	HORIZONTAL_GAUSSIAN_BLUR_SHADER,
	VERTICAL_GAUSSIAN_BLUR_SHADER,
	POST_PROCESS_SHADER,
	SPRITESHEET_BASIC_SHADER,
	SCREENSHAKE_SHADER,
	OUTLINE_SHADER,
	INSTANCED_OUTLINE_SHADER,
	EDGE_DETECTION_SHADER,
	LINE_SHADER,
	SHADER_COUNT
};

enum VAOS
{
	UI_PLAIN_VAO = 0,
	UI_TEXTURED_VAO,
	PARTICLE_PLAIN_VAO,
	PARTICLE_TEX_VAO,
	MEMORY_PIECE_VAO,
	QUAD_TEXTURED_VAO,
	OFFSCREEN_TEXTURE_VAO,
	SPRITESHEET_BASIC_VAO,
	BOTTOM_CENTRE_VAO,
	BOTTOM_RIGHT_VAO,
	BOTTOM_LEFT_VAO,
	LINE_VAO,
	VAO_COUNT
};

#define MAX_PARTICLES 500

enum ParticleSystems
{
	BALL_EXPLOSION = 0,
	PARTICLE_SYSTEMS_COUNT
};

enum Texts
{
	MAIN_MENU_PLAY_BUTTON = 0,
	MAIN_MENU_EXIT_BUTTON,
	GAME_MENU_RELOAD_LEVEL_BUTTON,
	GAME_MENU_SELECT_LEVEL_BUTTON,
	GAME_MENU_EXIT_GAME_BUTTON,
	TEXT_COUNT
};

struct Particle
{
	int isActive;
	glm::vec2 position;
	glm::vec2 direction;
	float speed;
	float age;
	float lifeTime;
};

struct ParticleEmitter
{
	Particle particles[MAX_PARTICLES];

	/* For user. */

	int maxParticlesPerFrame;
	float particlesPerSecond;
	int totalParticlesToEmit;

	float lifetime;
	float speed;

	glm::vec2 baseDirection;
	glm::vec2 origin;

	InitializeEmitter *initializeEmitter;

	/* For system. */

	int isTextured;
	int isActive;
	float secondsPerParticle;
	int currentlyActiveParticles;
	int totalParticlesEmitted;
	float particlesToGenerateThisFrame;
};

struct SpriteAnimator
{
	float timePassed;
	int loopFromIndex;
	int loopToIndex;
	int framesPerSecond;
	int loopsToExecute;

	int animationRunning;
	int currentIndex;
	int loopsExecuted;
};

struct TileDestroyData
{
	SpriteAnimator animator;
	glm::mat4 model;
};

struct SnowParticle
{
	glm::vec2 position;
	glm::vec2 direction;
	float speed;
	float radius;
};

struct SnowfallSystem
{
	glm::vec2 topLeft;
	glm::vec2 bottomRight;
	
	float minDirectionX;
	float maxDirectionX;
	float minDirectionY;
	float maxDirectionY;
	float minSpeed;
	float maxSpeed;
	float minSpeedDeviation;
	float maxSpeedDeviation;
	float minDeviationX;
	float maxDeviationX;
	float minDeviationY;
	float maxDeviationY;
	float minRadius;
	float maxRadius;

	SnowParticle snowParticles[MAX_SNOW_PARTICLES];
};

struct Renderer
{
	FontData fontData;

	Texture2D nodeTextures[NodeType::NODE_COUNT];
	Texture2D textures[Textures::TEX_COUNT];
	unsigned int shaderIds[Shaders::SHADER_COUNT];
	unsigned int vaos[VAOS::VAO_COUNT];

	unsigned int texIds[MAX_TEXTURE_SLOTS];
	unsigned int texStatus[MAX_TEXTURE_SLOTS];

	SpritesheetRenderData tilemapRenderData[TILEMAP_COUNT_Y * TILEMAP_COUNT_X];
	SpritesheetVertexBufferData tilemapVertexBufferData;

	SpritesheetRenderData ballsRenderData[MAX_BALLS];
	SpritesheetVertexBufferData ballsVertexBufferData;

	SpritesheetRenderData snowRenderData[MAX_SNOW_PARTICLES];
	SpritesheetVertexBufferData snowVertexBufferData;

	ParticleRenderData particleRenderData[MAX_PARTICLES];
	ParticleVertexBufferData particleVertexBufferData;

	SnowfallSystem snowfallSystem;

	ParticleEmitter particleEmitters[ParticleSystems::PARTICLE_SYSTEMS_COUNT];

	DottedLine dottedLine;

	FrameBuffer horizontalBlur;
	FrameBuffer verticleBlur;
	FrameBuffer postProcess;
	glm::mat4 projection;

	TileDestroyData tileDestroyInfos[MAX_TILE_DESTROY_SIMUL];
	SpriteAnimator explosionBallAnimator;
	SpriteAnimator snowSplashAnimator;

	char gameButtonTexts[Texts::TEXT_COUNT][32];

	float maxShakeTime;
	float shakeTime;
	//char texts[Texts::TEXT_COUNT][32];
};

struct SoundPlayer
{
	Sound background;
	Sound explosion;
	Sound ballToGoodMemoryTile;
	Sound ballToBadMemoryTile;
	Sound ballToWall;
	Sound snowBreak;
	Sound iceBreak1;
	Sound iceBreak2;
	Sound woodBreak;
};

struct Game
{
	int canvasWidth;
	int canvasHeight;
	float deltaTime;
	float frameRateAvg;
	float currentTime;
	int pickedBall;

	CurrentTilemapInfo currentTilemapInfo;
	CurrentLevelInfo currentLevelInfo;
	CurrentSceneInfo currentSceneInfo;

	HwInput hwInput;
	Renderer renderer;
	SoundPlayer soundPlayer;
};

void OnSceneUnloaded();
void OnLevelComplete();
void OnBallFinished();
void OnBallsFinished();
void OnShoot();

void InitGame();
void UpdateGame();

BallType UpdateBallSlotsAndUI(int ballSlotIndex);
float GetRandomNumber(float min, float max);
float GetMod(float x, float y);
int GetTileHitCapacity(TileType tileType);

void LoadSceneMainMenuUI();
void LoadSceneGameUI();
void LoadSceneLevelSelectorUI();
void LoadLevelUI();

void SetupJigsawPanels();
void UpdateJigsawPieces();

void LoadSceneMainMenu();
void UpdateSceneMainMenuLoading();
void UpdateSceneMainMenu();
void UnloadSceneMainMenu();
void UpdateSceneMainMenuUnloading();

void LoadSceneGame();
void UpdateSceneGameLoading();
void UpdateSceneGamePausing();
void UpdateSceneGamePaused();
void UpdateSceneGameResuming();
void UpdateSceneGame();
void UnloadSceneGame();
void UpdateSceneGameUnloading();

void LoadSceneLevelSelector();
void UpdateSceneLevelSelectorLoading();
void UpdateSceneLevelSelector();
void UnloadSceneLevelSelector();
void UpdateSceneLevelSelectorUnloading();


void UpdateWorldPositionsAll(UIElement *uiElements, int totalElements);

void ProcessMouseInput(UIElement *uiElements, int totalElements, HwButton mouseButtonInput, float mouseX, float mouseY);

void UpdateLevelChangeLevelMode();
void UpdateLevelBallExplodeMode();
void UpdateLevelBallMoveMode();
void UpdateLevelAimMode();
void UpdateLevelBeginMode();


void InitRenderer();

Game* GetGameMemory();
void* GetTempMemory();
void* GetTempMemory(int *size);
void Quit();

int CreateAudio(const char *filepath, AudioData *audioData, void *preAllocatedMemory, int preAllocatedSize);
void CreateSource(SourceData *sourceData, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop);
int CreateSound(Sound *sound, const char *filepath, float pitch, float gain, glm::vec3 position, glm::vec3 velocity, int loop, void *allocatedMem, int sizeOfAllocatedMem);
void PlaySound(Sound *sound);

void BasicBallTileInteraction(CollisionInfo info);
void BubbleBallTileInteraction(CollisionInfo info);
void ExplodeBallTileInteraction(CollisionInfo info);
void BasicBallWallInteraction(CollisionInfo info);
void BubbleBallWallInteraction(CollisionInfo info);
void ExplodeBallWallInteraction(CollisionInfo info);


void InitiateParticleEmitter(ParticleSystems particleSystem);
void UpdateParticleEmitters();
void EndParticleEmitter(ParticleSystems particleSystem);
void EndParticleEmitters();

void RenderInstancedSpritesheet(SpritesheetVertexBufferData *spritesheetVertexBufferData, SpritesheetRenderData *spritesheetRenderData, int instanceCount,
	int texSlot, int rows, int cols, Shaders shaderToUse);
void AddTileToDestroy(TileType tileType, glm::vec2 position, glm::vec2 scale);
void InitiateSpriteAnimator(SpriteAnimator *spriteAnimator, int loopFromIndex, int loopToIndex, int framesPerSecond, int loopsToExecute);
void AnimateSprite(SpriteAnimator *spriteAnimator, float deltaTime);
void InstantiateSnowfallSystem();
void RenderOutlinedPieces(float maxTime);

void LoadRendererMainMenu();
void LoadingRendererMainMenu();
void UpdateRendererMainMenu();
void UnloadRendererMainMenu();
void UnloadingRendererMainMenu();

void LoadRendererGame();
void LoadingRendererGame();
void UpdateRendererGame();
void UpdateRendererGamePaused();
void UnloadRendererGame();
void UnloadingRendererGame();

void LoadRendererLevelSelector();
void LoadingRendererLevelSelector();
void UpdateRendererLevelSelector();
void UnloadRendererLevelSelector();
void UnloadingRendererLevelSelector();

void RenderOverlayPanel(Renderer *renderer, UIElement *element);

#endif


