#include <iostream>

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "AL/al.h"
#include "AL/alc.h"

#include "game.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

void ProcessInput(GLFWwindow* window);
HwButton ProcessButton(HwButton button, bool isPressed);
void OnCursorPosCallback(GLFWwindow* window, double xpos, double ypos);
void OnViewportSizeChanged_Callback(GLFWwindow *window, int newWindowWidth, int newWindowHeight);

Game *gameMemory;
void *tempMemory;
int hasQuit;
int tempDataSizeInBytes;
int totalMemorySize;

void InitOpenAL()
{
	ALCdevice *device = alcOpenDevice(0);
	const ALchar *devices = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);

	if (device)
	{
		ALCcontext *context = alcCreateContext(device, 0);
		alcMakeContextCurrent(context);
	}

	alListener3f(AL_POSITION, 0, 0, 0);
	alListener3f(AL_VELOCITY, 0, 0, 0);
	alListener3f(AL_ORIENTATION, 0, 0, -1);
}

int main()
{
	float beginTime = glfwGetTime();
	int frameNumber = 0;
	int framesThisDuration = 0;
	int frameRateAvgDurationInMs = 1000.0f;
	int cumulativeTime = 0.0f;
	float currentTime = beginTime;
	float previousTime = currentTime;
	float deltaTime;
	float frameRate;
	float frameRateAvg = 0;
	hasQuit = 0;

#pragma region Platform Initialization

	InitOpenAL();

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	/* Uncomment this for Mac. */
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "A Tiny Elf", NULL, NULL);
	if (!window)
	{
		std::cout << "Failed to create GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, OnViewportSizeChanged_Callback);
	glfwSetCursorPosCallback(window, OnCursorPosCallback);

#pragma endregion

#pragma region OpenGL Initialization

	/* GLAD manages all the OpenGL function pointers so we need to initialize GLAD before using any OpenGL API. */
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

#pragma endregion

	tempDataSizeInBytes = 100 * MB;
	totalMemorySize = tempDataSizeInBytes + sizeof(Game);
	gameMemory = (Game *)calloc(totalMemorySize, 1);
	tempMemory = (void *)(gameMemory + 1);

	gameMemory->canvasWidth = SCREEN_WIDTH;
	gameMemory->canvasHeight = SCREEN_HEIGHT;
	InitGame();
	HwInput *newInput = &gameMemory->hwInput;

	/* Main game loop. */
	while (!glfwWindowShouldClose(window))
	{

#pragma region FrameRateCalculation

		currentTime = glfwGetTime();
		deltaTime = currentTime - previousTime;
		deltaTime = clamp(deltaTime, 0.0f, 0.03333333f);
		previousTime = currentTime;
		frameRate = 1.0f / deltaTime;
		cumulativeTime += (deltaTime * 1000.0f);
		frameNumber++;
		framesThisDuration++;
		if (cumulativeTime >= frameRateAvgDurationInMs)
		{
			frameRateAvg = framesThisDuration;
			framesThisDuration = 0;
			cumulativeTime = 0.0f;
		}

		gameMemory->deltaTime = deltaTime;
		gameMemory->currentTime = currentTime;
		gameMemory->frameRateAvg = frameRateAvg;
#pragma endregion

		ProcessInput(window);

		UpdateGame();

		glfwSwapBuffers(window);
		glfwPollEvents();

	}

	glfwTerminate();

	return 0;
}

void ProcessInput(GLFWwindow* window)
{
	HwInput newInput = gameMemory->hwInput;
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS || hasQuit)
	{
		glfwSetWindowShouldClose(window, true);
	}

	newInput.left = ProcessButton(newInput.left, (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS));
	newInput.right = ProcessButton(newInput.right, (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS));
	newInput.escape = ProcessButton(newInput.escape, (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS));
	newInput.mouse = ProcessButton(newInput.mouse, (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS));

	gameMemory->hwInput = newInput;
}

HwButton ProcessButton(HwButton button, bool isPressed)
{
	if (button.isPressed)
		button.wasPressed = true;
	else
		button.wasPressed = false;

	button.isPressed = isPressed;

	return button;
}

void OnCursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	HwInput newInput = gameMemory->hwInput;
	newInput.mouseX = (float)xpos;
	newInput.mouseY = gameMemory->canvasHeight - (float)ypos;
	gameMemory->hwInput = newInput;
}

void OnViewportSizeChanged_Callback(GLFWwindow *window, int newWindowWidth, int newWindowHeight)
{
	glViewport(0, 0, newWindowWidth, newWindowHeight);
}

Game* GetGameMemory()
{
	return gameMemory;
}

void* GetTempMemory()
{
	return tempMemory;
}

void* GetTempMemory(int *size)
{
	*size = tempDataSizeInBytes;
	return tempMemory;
}

void Quit()
{
	hasQuit = 1;
}