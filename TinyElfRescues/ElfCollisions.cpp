#include "ElfCollisions.h"
#include "ElfIntrinsics.h"

void SetupBoxCollider(Collider *collider, EntityType entityType, glm::vec2 centre, float width, float height)
{
	Collider boxCollider;
	boxCollider.colliderType = ColliderType::BOX;
	boxCollider.entityType = entityType;
	boxCollider.box.topLeft = centre + glm::vec2(-width * 0.5f, height * 0.5f);
	boxCollider.box.bottomRight = centre + glm::vec2(width * 0.5f, -height * 0.5f);
	*collider = boxCollider;
}

void SetupCircleCollider(Collider *collider, EntityType entityType, glm::vec2 centre, float radius)
{
	Collider circleCollider;
	circleCollider.colliderType = ColliderType::CIRCLE;
	circleCollider.entityType = entityType;
	circleCollider.circle.centre = centre;
	circleCollider.circle.radius = radius;
	*collider = circleCollider;
}


inline CollisionInfo GetCollisionInfo(Collider ball, Collider box, glm::vec2 impactPoint)
{
	glm::vec2 normal;
	normal = ball.circle.centre - impactPoint;

	glm::vec2 penetration = -normal;
	glm::vec2 penetrationNorm = glm::normalize(penetration);
	penetration = (penetrationNorm * ball.circle.radius) - penetration;

	CollisionInfo info;
	info.entityType = box.entityType;
	info.impactSurfaceNormal = glm::normalize(normal);
	info.penetration = penetration;
	return info;
}

inline CollisionInfo GetCollisionInfo(Collider ball, Collider circle, float minDistance, float distance)
{
	glm::vec2 normal;
	normal = ball.circle.centre - circle.circle.centre;
	float penetrationAmount = minDistance - distance;

	CollisionInfo info;
	info.entityType = circle.entityType;
	info.impactSurfaceNormal = glm::normalize(normal);
	info.penetration = penetrationAmount * -info.impactSurfaceNormal;

	return info;
}

int IsBoxColliding(Collider ball, Collider box, CollisionInfo *info)
{
	glm::vec2 boxCentre = (box.box.topLeft + box.box.bottomRight) * 0.5f;
	glm::vec2 delta = ball.circle.centre - boxCentre;
	glm::vec2 closestPoint = glm::vec2(clamp(ball.circle.centre.x, box.box.topLeft.x, box.box.bottomRight.x),
		clamp(ball.circle.centre.y, box.box.bottomRight.y, box.box.topLeft.y));
	
	float distance = glm::distance(ball.circle.centre, closestPoint);

	bool isColliding = (distance <= ball.circle.radius) ? true : false;

	if (isColliding)
		*info = GetCollisionInfo(ball, box, closestPoint);

	return isColliding;
}

int IsCircleColliding(Collider ball, Collider circle, CollisionInfo *info)
{
	float distance = glm::distance(ball.circle.centre, circle.circle.centre);
	float minDistance = ball.circle.radius + circle.circle.radius;

	bool isColliding = (distance <= minDistance) ? true : false;

	if (isColliding)
	{
		*info = GetCollisionInfo(ball, circle, minDistance, distance);
	}

	return isColliding;
}

int DoCollisions(Collider ball, Collider *tileColliders, int totalTileColliders, Collider *wallColliders, int totalWallColliders, CollisionInfo *collisionInfo)
{
	bool isColliding = false;
	CollisionInfo info;
	for (int i = 0; i < totalTileColliders; i++)
	{
		if (tileColliders[i].colliderType == ColliderType::BOX)
		{
			isColliding = IsBoxColliding(ball, tileColliders[i], &info);
		}
		else
		{
			isColliding = IsCircleColliding(ball, tileColliders[i], &info);
		}

		if (isColliding)
		{
			info.index = i;
			*collisionInfo = info;
			return 1;
		}
	}

	for (int i = 0; i < totalWallColliders; i++)
	{
		isColliding = IsBoxColliding(ball, wallColliders[i], &info);
		if (isColliding)
		{
			info.index = i;
			*collisionInfo = info;
			return 1;
		}
	}

	return 0;
}