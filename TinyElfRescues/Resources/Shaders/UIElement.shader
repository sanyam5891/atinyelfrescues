#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoords;

uniform mat4 projection;
uniform mat4 model;

out vec2 TexCoords;

void main()
{
	gl_Position = projection * model * vec4(aPos, 0.0f, 1.0f);
	TexCoords = aTexCoords;
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords;

uniform vec4 uiColor;
uniform vec4 textColor;
uniform sampler2D text;
uniform sampler2D mainTex;

out vec4 FragColor;

void main()
{
	FragColor = uiColor;
}