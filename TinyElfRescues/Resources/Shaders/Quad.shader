#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec4 aPosTexCoords;

uniform mat4 projection;
uniform mat4 model;

out vec2 texCoord;

void main()
{
	gl_Position = projection * model * vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);
	texCoord = vec2(aPosTexCoords.z, aPosTexCoords.w);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 texCoord;

uniform sampler2D mainTex;
uniform vec4 color;

out vec4 FragColor;

void main()
{
	FragColor = texture(mainTex, texCoord) * color;
}