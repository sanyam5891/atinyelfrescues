#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aTexCoord;

uniform mat4 projection;
uniform mat4 model;

void main()
{
	gl_Position = projection * model * vec4(aPos, 0.0f, 1.0f);
}

#FRAGMENT_SHADER
#version 330 core

uniform vec4 color;
out vec4 FragColor;

void main()
{
	FragColor = color;
}