#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 aPosTexCoords;

uniform mat4 projection;
uniform float time;
uniform float strength;
out vec2 TexCoords;

void main()
{
	gl_Position = vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);
	gl_Position.x += cos(time * 10) * strength;
	gl_Position.y += cos(time * 15) * strength;

	TexCoords = vec2(aPosTexCoords.z, aPosTexCoords.w);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords;
uniform sampler2D screenTexture;
uniform vec2      offsets[9];
uniform float     blur_kernel[9];
out vec4 FragColor;

void main()
{
	FragColor = vec4(0.0f);
	vec3 sample[9];

	for (int i = 0; i < 9; i++)
		sample[i] = vec3(texture(screenTexture, TexCoords.st + offsets[i]));

	for (int i = 0; i < 9; i++)
		FragColor += vec4(sample[i] * blur_kernel[i], 0.0f);
	FragColor.a = 1.0f;
}