#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec4 aPosTexCoords;

uniform mat4 model;
uniform mat4 projection;
uniform vec2 gridSize;
uniform vec2 cellCoord;
out vec2 texCoords;

void main()
{
	gl_Position = projection * model * vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);

	vec2 texCoordDelta = vec2(1.0f, -1.0f) / gridSize;
	vec2 normalizedTexCoord = vec2(aPosTexCoords.z / gridSize.x, (-texCoordDelta.y * (gridSize.y - 1)) + aPosTexCoords.w / gridSize.y);
	texCoords = normalizedTexCoord + (cellCoord * texCoordDelta);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 texCoords;
uniform sampler2D mainTex;
uniform vec4 color;
out vec4 FragColor;

void main()
{
	FragColor = texture(mainTex, texCoords);
	if (FragColor.a == 0.0f)
		discard;
	FragColor = color;
}