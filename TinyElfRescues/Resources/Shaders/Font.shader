#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec4 aPosTex;

uniform mat4 projection;

out vec2 TexCoord;

void main()
{
	gl_Position = projection * vec4(aPosTex.xy, 0.0f, 1.0f);
	TexCoord = aPosTex.zw;
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoord;

uniform sampler2D text;
uniform vec3 textColor;

out vec4 FragColor;

void main()
{
	vec4 sampled = vec4(1.0f, 1.0f, 1.0f, texture(text, TexCoord).r);
	FragColor = vec4(textColor, 1.0f) * sampled;
}