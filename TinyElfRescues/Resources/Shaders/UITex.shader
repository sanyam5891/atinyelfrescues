#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 aPos;

uniform mat4 model;
uniform mat4 projection;

out vec2 TexCoords;

void main()
{
	gl_Position = projection * model * vec4(aPos.x, aPos.y, 0.0f, 1.0f);
	TexCoords = vec2(aPos.z, aPos.w);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords;

uniform sampler2D mainTex;
uniform vec4 color;

out vec4 FragColor;

void main()
{
	FragColor = texture(mainTex, TexCoords) * color;
}