#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 aPosTexCoords;

uniform mat4 projection;
out vec2 TexCoords;

void main()
{
	gl_Position = vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);
	TexCoords = vec2(aPosTexCoords.z, aPosTexCoords.w);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords;
uniform sampler2D screenTexture;
out vec4 FragColor;

void main()
{
	FragColor = texture(screenTexture, TexCoords);
}