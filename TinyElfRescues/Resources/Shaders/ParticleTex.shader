#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec4 aPosTexCoord;
layout(location = 1) in mat4 aModel;
layout(location = 5) in vec2 texStage;

uniform mat4 projection;
uniform vec2 gridSize;

out vec2 TexCoord;

void main()
{
	gl_Position = projection * aModel * vec4(aPosTexCoord.x, aPosTexCoord.y, 0.0f, 1.0f);
	vec2 texCoordDelta = vec2(1.0f, -1.0f) / gridSize;
	vec2 normalizedTexCoord = vec2(aPosTexCoord.z / gridSize.x, 1.0f - (aPosTexCoord.w / gridSize.y));
	TexCoord = normalizedTexCoord + (texStage * texCoordDelta);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoord;

uniform sampler2D mainTex;
uniform vec4 color;

out vec4 FragColor;

void main()
{
	FragColor = texture(mainTex, TexCoord) * color;
}