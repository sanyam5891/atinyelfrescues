#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 iPosTexCoords;

uniform int index;
uniform int rows;
uniform int cols;
uniform mat4 model;
uniform mat4 projection;

out vec2 vTexCoords;

void main()
{
	gl_Position = projection * model * vec4(iPosTexCoords.x, iPosTexCoords.y, 0.0f, 1.0f);
	int row = index / cols;
	int col = index % cols;

	vec2 deltaTexCoords = vec2(1.0f, 1.0f) / vec2(cols, rows);
	vec2 scaledTexCoords = iPosTexCoords.zw * deltaTexCoords;
	vTexCoords = vec2(scaledTexCoords.x, scaledTexCoords.y + ((rows - 1) * deltaTexCoords.y));
	vTexCoords = vTexCoords + vec2(col * deltaTexCoords.x, -row * deltaTexCoords.y);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 vTexCoords;
uniform sampler2D sheetTex;
out vec4 fColor;

void main()
{
	fColor = texture(sheetTex, vTexCoords);
}