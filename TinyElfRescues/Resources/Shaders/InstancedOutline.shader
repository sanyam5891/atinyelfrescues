#VERTEX_SHADER
#version 330 core

layout(location = 0) in vec4 aPosTexCoords;
layout(location = 1) in mat4 aModel;
layout(location = 5) in int index;

uniform int rows;
uniform int cols;
uniform mat4 projection;

out vec2 TexCoords0;

void main()
{
	gl_Position = projection * aModel * vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);

	int row = index / cols;
	int col = index % cols;

	vec2 deltaTexCoords = vec2(1.0f, 1.0f) / vec2(cols, rows);
	vec2 scaledTexCoords = aPosTexCoords.zw * deltaTexCoords;
	TexCoords0 = vec2(scaledTexCoords.x, scaledTexCoords.y + ((rows - 1) * deltaTexCoords.y));
	TexCoords0 = TexCoords0 + vec2(col * deltaTexCoords.x, -row * deltaTexCoords.y);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords0;
uniform vec4 color;
uniform sampler2D sheetTex;
out vec4 FragColor;

void main()
{
	FragColor = texture(sheetTex, TexCoords0);

	if (FragColor.a < 0.5f)
		discard;

	FragColor = color;
}