#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 aPosTexCoords;

const float offset = 1.0 / 640.0f;

uniform mat4 projection;
out vec2 TexCoords;
out vec2 offsetsH[11];

void main()
{
	gl_Position = vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);
	TexCoords = vec2(aPosTexCoords.z, aPosTexCoords.w);

	int kernelSizeHalf = 5;
	for (int i = -kernelSizeHalf; i <= kernelSizeHalf; i++)
	{
		offsetsH[i + kernelSizeHalf] = vec2(i * offset, 0.0f);
	} 
}

#FRAGMENT_SHADER
#version 330 core

in vec2 offsetsH[11];
in vec2 TexCoords;
uniform sampler2D screenTexture;
out vec4 FragColor;

void main()
{
	float kernel[11] = float[]
	(
		0.080679,	0.086548,	0.091407,	0.095043,	0.097294,	0.098056,	0.097294,	0.095043,	0.091407,	0.086548,	0.080679
	);

	// Horizontal Pass.
	vec3 sampleTexColor[11];
	vec3 col = vec3(0.0f);
	for (int i = 0; i < 11; i++)
	{
		sampleTexColor[i] = vec3(texture(screenTexture, TexCoords + offsetsH[i]));
		col += (sampleTexColor[i] * kernel[i]);
	}

	FragColor = vec4(col, 1.0f);
}