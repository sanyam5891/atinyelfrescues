#VERTEX_SHADER
#version 330 core
layout(location = 0) in vec4 aPosTexCoords;

uniform mat4 model;
uniform mat4 projection;
uniform vec2 gridSize;
uniform vec2 cellCoord;
out vec2 TexCoords;

void main()
{
	gl_Position = projection * model * vec4(aPosTexCoords.x, aPosTexCoords.y, 0.0f, 1.0f);
	vec2 texCoordDelta = vec2(1.0f, -1.0f) / gridSize;
	vec2 normalizedTexCoord = vec2(aPosTexCoords.z / gridSize.x, (-texCoordDelta.y * (gridSize.y - 1)) + aPosTexCoords.w / gridSize.y);
	TexCoords = normalizedTexCoord + (cellCoord * texCoordDelta);
}

#FRAGMENT_SHADER
#version 330 core

in vec2 TexCoords;

uniform sampler2D mainTex;
uniform vec4 color;

out vec4 FragColor;

const float offset = 1.0f / 300.0f;

void main()
{
	vec2 offsets[9] = vec2[] 
	(
		vec2(-offset,  offset), // top-left
		vec2(0.0f,    offset), // top-center
		vec2(offset,  offset), // top-right
		vec2(-offset,  0.0f),   // center-left
		vec2(0.0f,    0.0f),   // center-center
		vec2(offset,  0.0f),   // center-right
		vec2(-offset, -offset), // bottom-left
		vec2(0.0f,   -offset), // bottom-center
		vec2(offset, -offset)  // bottom-right
	);

	float kernel[9] = float[]
	(
		1, 1, 1,
		1, -8, 1,
		1, 1, 1
	);

	vec3 sampleTex[9];
	for (int i = 0; i < 9; i++)
	{
		sampleTex[i] = vec3(texture(mainTex, TexCoords.xy + offsets[i]));
	}
	vec3 col = vec3(0.0f);
	for (int i = 0; i < 9; i++)
	{
		col += (sampleTex[i] * kernel[i]);
	}

	FragColor = vec4(col, 1.0f);
}