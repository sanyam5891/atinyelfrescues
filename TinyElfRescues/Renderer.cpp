#include "game.h"
#include "renderer.h"

void RenderTexButton(Renderer *renderer, UIElement *element, unsigned int texSlot);


int AddTexId(unsigned int texId, unsigned int *texStatus, unsigned int *texIds)
{
	for (int i = 0; i < MAX_TEXTURE_SLOTS; i++)
	{
		if (texStatus[i] == 0)
		{
			texIds[i] = texId;
			texStatus[i] = 1;
			UseTexture2D(texId, i);
			return 1;
		}
	}

	return 0;
}

int ReplaceTexId(unsigned int texIdNew, unsigned int texIdOld, unsigned int *texIds)
{
	for (int i = 0; i < MAX_TEXTURE_SLOTS; i++)
	{
		if (texIds[i] == texIdOld)
		{
			texIds[i] = texIdNew;
			return 1;
		}
	}

	return 0;
}

void RemoveTexId(unsigned int texId, unsigned int *texIds, unsigned int *texStatus)
{
	for (int i = 0; i < MAX_TEXTURE_SLOTS; i++)
	{
		if (texIds[i] == texId)
		{
			texStatus[i] = 0;
		}
	}
}

int GetTexSlot(unsigned int texId, unsigned int *texIds)
{
	for (int i = 0; i < MAX_TEXTURE_SLOTS; i++)
	{
		if (texIds[i] == texId)
		{
			return i;
		}
	}

	return -1;
}

void CreateParticleSystemVAO(float *vertexData, int sizeofVertexData)
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;

	glGenVertexArrays(1, &renderer->particleVertexBufferData.vao);
	glBindVertexArray(renderer->particleVertexBufferData.vao);

	unsigned int staticVbo;
	glGenBuffers(1, &staticVbo);
	glBindBuffer(GL_ARRAY_BUFFER, staticVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeofVertexData, vertexData, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &renderer->particleVertexBufferData.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, renderer->particleVertexBufferData.vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(renderer->particleRenderData), renderer->particleRenderData, GL_STATIC_DRAW);

	int vec4Size = sizeof(glm::vec4);
	int particleRenderDataSize = sizeof(ParticleRenderData);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, particleRenderDataSize, (void *)(0));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, particleRenderDataSize, (void *)(1 * vec4Size));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, particleRenderDataSize, (void *)((2 * vec4Size)));
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, particleRenderDataSize, (void *)((3 * vec4Size)));
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, particleRenderDataSize, (void *)((4 * vec4Size)));
	glEnableVertexAttribArray(5);

	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);

	glBindVertexArray(0);

}

void CreateSpritesheetVAO(float *vertexData, int sizeofVertexData, int count, SpritesheetRenderData *spritesheetRenderData, SpritesheetVertexBufferData *spritesheetVertexBufferData)
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;

	int vec4Size = sizeof(glm::vec4);
	int spritesheetRenderDataStructSize = sizeof(SpritesheetRenderData);


	glGenVertexArrays(1, &spritesheetVertexBufferData->vao);
	glBindVertexArray(spritesheetVertexBufferData->vao);

	glGenBuffers(1, &spritesheetVertexBufferData->vboNI);
	glBindBuffer(GL_ARRAY_BUFFER, spritesheetVertexBufferData->vboNI);
	glBufferData(GL_ARRAY_BUFFER, sizeofVertexData, vertexData, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(0));
	glEnableVertexAttribArray(0);


	glGenBuffers(1, &spritesheetVertexBufferData->vboI);
	glBindBuffer(GL_ARRAY_BUFFER, spritesheetVertexBufferData->vboI);
	glBufferData(GL_ARRAY_BUFFER, count * spritesheetRenderDataStructSize, spritesheetRenderData, GL_STATIC_DRAW);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, spritesheetRenderDataStructSize, (void *)(0));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, spritesheetRenderDataStructSize, (void *)(1 * vec4Size));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, spritesheetRenderDataStructSize, (void *)(2 * vec4Size));
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, spritesheetRenderDataStructSize, (void *)(3 * vec4Size));
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, spritesheetRenderDataStructSize, (void *)(4 * vec4Size));
	glEnableVertexAttribArray(5);

	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);

	glBindVertexArray(0);
}

unsigned int GetBallIndex(BallType ballType)
{

	unsigned int index = 0;
	switch (ballType)
	{
		case BallType::BASIC_BALL:
			index = 0;
			break;
		case BallType::BUBBLE_BALL:
			index = 1;
			break;
		case BallType::EXPLODE_BALL:
		{
			Game *game = GetGameMemory();
			SpriteAnimator *explosionAnimator = &game->renderer.explosionBallAnimator;
			AnimateSprite(explosionAnimator, game->deltaTime);
			index = explosionAnimator->currentIndex;
		}
		break;
	}

	return index;
}

unsigned int GetTilemapIndex(TileType tileType)
{
	unsigned int index = 0;
	switch (tileType)
	{
		case TileType::SINGLE_HIT:
			index = 0;
			break;
		case TileType::DOUBLE_HIT:
			index = 9;
			break;
		case TileType::TRIPLE_HIT:
			index = 2;
			break;
		case TileType::RIGID:
			index = 39;
			break;
		case TileType::GOOD_MEMORY:
			index = 30;
			break;
		case TileType::BAD_MEMORY:
			index = 21;
			break;
	}

	return index;
}

void InitializeExplosionParticleSystem()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	ParticleEmitter *explosionEmitter = &renderer->particleEmitters[ParticleSystems::BALL_EXPLOSION];
	explosionEmitter->maxParticlesPerFrame = 400;
	explosionEmitter->particlesPerSecond = 1000.0f;
	explosionEmitter->totalParticlesToEmit = 1;

	explosionEmitter->lifetime = 2.0f;
	explosionEmitter->speed = 100.0f;

	explosionEmitter->baseDirection = glm::vec2(0.0f, 1.0f);
	explosionEmitter->origin = game->currentLevelInfo.ball.position;

	explosionEmitter->secondsPerParticle = 1 / explosionEmitter->particlesPerSecond;
	explosionEmitter->isTextured = true;
}

#pragma region ParticleSystem

void InitiateParticleEmitter(ParticleSystems particleSystem)
{
	Renderer *renderer = &GetGameMemory()->renderer;
	ParticleEmitter *emitter = &renderer->particleEmitters[particleSystem];

	if (emitter->initializeEmitter)
	{
		emitter->totalParticlesEmitted = 0;
		emitter->isActive = true;
		emitter->particlesToGenerateThisFrame = 0.0f;
		emitter->initializeEmitter();
	}
}

void EndParticleEmitter(ParticleSystems particleSystem)
{
	Renderer *renderer = &GetGameMemory()->renderer;
	ParticleEmitter *emitter = &renderer->particleEmitters[particleSystem];
	emitter->isActive = false;
}

void EndParticleEmitters()
{
	for (int i = 0; i < ParticleSystems::PARTICLE_SYSTEMS_COUNT; i++)
	{
		EndParticleEmitter((ParticleSystems)i);
	}
}

Particle GenerateParticle(ParticleEmitter *emitter, float residualAge)
{
	Particle particle;
	particle.age = residualAge;
	particle.lifeTime = emitter->lifetime;
	particle.speed = emitter->speed;
	particle.position = emitter->origin;
	particle.direction = emitter->baseDirection;

	return particle;
}

void UpdateParticle(Particle *currentParticle, float deltaTime)
{
	Particle particle = *currentParticle;
	particle.position += (particle.direction * particle.speed * deltaTime);
	particle.age += deltaTime;
	if (particle.age >= particle.lifeTime)
	{
		particle.isActive = false;
	}

	*currentParticle = particle;
}

void UpdateParticleEmitter(ParticleEmitter *emitter, float deltaTime)
{
	emitter->particlesToGenerateThisFrame += (emitter->particlesPerSecond * deltaTime);
	int particlesToGenerateThisFrame = emitter->particlesToGenerateThisFrame;

	float residualAge = (emitter->particlesToGenerateThisFrame / emitter->particlesPerSecond) - emitter->secondsPerParticle;

	emitter->particlesToGenerateThisFrame -= (float)particlesToGenerateThisFrame;

	particlesToGenerateThisFrame = clamp(particlesToGenerateThisFrame, 0, emitter->totalParticlesToEmit - emitter->totalParticlesEmitted);

	int currentlyActiveParticles = emitter->currentlyActiveParticles;
	int totalParticlesEmitted = emitter->totalParticlesEmitted;

	Particle *particles = emitter->particles;
	for (int i = 0; i < currentlyActiveParticles; i++)
	{
		UpdateParticle(&particles[i], deltaTime);
		if (!particles[i].isActive) // Particle needs to die.
		{
			if (particlesToGenerateThisFrame > 0) // Replace with a newly generated particle
			{
				particles[i] = GenerateParticle(emitter, residualAge);
				particlesToGenerateThisFrame--;
				totalParticlesEmitted++;
			}
			else // Replace with an already active particle from the end of the array.
			{
				currentlyActiveParticles--;
				if (i < currentlyActiveParticles) // If this is not the last particle in the array.
				{
					particles[i] = particles[currentlyActiveParticles];
					i--;
				}
			}
		}
	}

	particlesToGenerateThisFrame = clamp(particlesToGenerateThisFrame, 0, emitter->maxParticlesPerFrame - currentlyActiveParticles);

	while (particlesToGenerateThisFrame > 0)
	{
		particles[currentlyActiveParticles] = GenerateParticle(emitter, residualAge);
		currentlyActiveParticles++;
		totalParticlesEmitted++;
		particlesToGenerateThisFrame--;
	}

	emitter->totalParticlesEmitted = totalParticlesEmitted;
	emitter->currentlyActiveParticles = currentlyActiveParticles;

	if ((emitter->totalParticlesEmitted == emitter->totalParticlesToEmit) && currentlyActiveParticles == 0)
	{
		emitter->isActive = false;
	}

	//std::cout << currentlyActiveParticles << std::endl;
}

void UpdateParticleEmitters()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	Renderer *renderer = &game->renderer;
	ParticleEmitter *emitters = renderer->particleEmitters;

	for (int i = 0; i < ParticleSystems::PARTICLE_SYSTEMS_COUNT; i++)
	{
		if (emitters[i].isActive)
		{
			UpdateParticleEmitter(&emitters[i], deltaTime);
		}
	}
}

void RenderParticles()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;

	ParticleEmitter *emitters = renderer->particleEmitters;

	for (int i = 0; i < ParticleSystems::PARTICLE_SYSTEMS_COUNT; i++)
	{
		ParticleEmitter *emitter = &emitters[i];
		if (emitters->isActive)
		{
			if (emitters->isTextured)
			{
				unsigned int *texIds = renderer->texIds;
				Texture2D *textures = renderer->textures;

				ParticleVertexBufferData particleVertexBufferData = renderer->particleVertexBufferData;
				ParticleRenderData *particleRenderData = renderer->particleRenderData;

				int cols = 8;
				int rows = 6;
				int totalStages = cols * rows;
				float agePerStage = 1.0f / (float)totalStages;

				UseShader(shaderIds[Shaders::PARTICLE_TEXTURED_SHADER]);
				SetUniform1i(shaderIds[Shaders::PARTICLE_TEXTURED_SHADER], "mainTex", GetTexSlot(textures[Textures::EXPLODE_PARTICLE_TEXES].id, texIds));
				SetUniform4f(shaderIds[Shaders::PARTICLE_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
				SetUniform2f(shaderIds[Shaders::PARTICLE_TEXTURED_SHADER], "gridSize", 8, 6);

				int currentlyActiveParticles = emitter->currentlyActiveParticles;
				Particle *particles = emitter->particles;
				for (int i = 0; i < currentlyActiveParticles; i++)
				{
					glm::mat4 model = glm::mat4(1.0f);
					model = glm::translate(model, glm::vec3(particles[i].position, 0.0f));
					model = glm::scale(model, glm::vec3(512.0f, 512.0f, 1.0f));
					float normalizedAge = particles[i].age / particles[i].lifeTime;
					int stage = normalizedAge / agePerStage;
					int col = stage % cols;
					int row = stage / cols;

					particleRenderData[i].model = model;
					particleRenderData[i].stage = glm::vec2(col, row);
				}

				glBindBuffer(GL_ARRAY_BUFFER, particleVertexBufferData.vbo);
				glBufferSubData(GL_ARRAY_BUFFER, 0, currentlyActiveParticles * sizeof(ParticleRenderData), particleRenderData);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(particleVertexBufferData.vao);
				glDrawArraysInstanced(GL_TRIANGLES, 0, 6, currentlyActiveParticles);
				glBindVertexArray(0);
			}
			else
			{
				unsigned int *vaos = renderer->vaos;

				UseShader(shaderIds[Shaders::PARTICLE_PLAIN_SHADER]);
				unsigned int vao = vaos[VAOS::PARTICLE_PLAIN_VAO];

				int currentlyActiveParticles = emitter->currentlyActiveParticles;
				Particle *particles = emitter->particles;
				for (int i = 0; i < currentlyActiveParticles; i++)
				{
					glm::mat4 model = glm::mat4(1.0f);
					model = glm::translate(model, glm::vec3(particles[i].position, 0.0f));
					model = glm::scale(model, glm::vec3(5.0f, 5.0f, 1.0f));
					SetUniform4x4(shaderIds[Shaders::PARTICLE_PLAIN_SHADER], "model", model);

					DrawQuad(vao);
				}
			}
		}
	}
}

#pragma endregion

unsigned int vao;

void InitRenderer()
{
	void *tempMemory = GetTempMemory();
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	FontData *fontData = &renderer->fontData;
	int canvasWidth = game->canvasWidth;
	int canvasHeight = game->canvasHeight;

	float memoryPieceTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		 0.5f, -0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f,		0.0f, 1.0f,		// top left 
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
	};

	float quadTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		 0.5f, -0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f,		0.0f, 1.0f,		// top left 
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
	};

	float bottomCentreTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f + 0.5f,	1.0f, 1.0f,		// top right
		 0.5f, -0.5f + 0.5f,	1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f + 0.5f,	0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f + 0.5f,	0.0f, 1.0f,		// top left 
		 0.5f,  0.5f + 0.5f,	1.0f, 1.0f,		// top right
		-0.5f, -0.5f + 0.5f,	0.0f, 0.0f,		// bottom left
	};

	float bottomRightTexVerts[] =
	{	// positions		// texcoords
		 0.5f - 0.5f,  0.5f + 0.5f,		1.0f, 1.0f,		// top right
		 0.5f - 0.5f, -0.5f + 0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f - 0.5f, -0.5f + 0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f - 0.5f,  0.5f + 0.5f,		0.0f, 1.0f,		// top left 
		 0.5f - 0.5f,  0.5f + 0.5f,		1.0f, 1.0f,		// top right
		-0.5f - 0.5f, -0.5f + 0.5f,		0.0f, 0.0f,		// bottom left
	};

	float bottomLeftTexVerts[] =
	{	// positions		// texcoords
		 0.5f + 0.5f,  0.5f + 0.5f,		1.0f, 1.0f,		// top right
		 0.5f + 0.5f, -0.5f + 0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f + 0.5f, -0.5f + 0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f + 0.5f,  0.5f + 0.5f,		0.0f, 1.0f,		// top left 
		 0.5f + 0.5f,  0.5f + 0.5f,		1.0f, 1.0f,		// top right
		-0.5f + 0.5f, -0.5f + 0.5f,		0.0f, 0.0f,		// bottom left
	};

	float particlePlainVerts[] =
	{
		// positions
		 0.5f,  0.5f,	// top right
		 0.5f, -0.5f,	// bottom right
		-0.5f, -0.5f,	// bottom left
		-0.5f,  0.5f,	// top left 
		 0.5f,  0.5f,	// top right
		-0.5f, -0.5f	// bottom left
	};

	float particleTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		 0.5f, -0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f,		0.0f, 1.0f,		// top left 
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
	};

	float uiPlainVerts[] =
	{
		// positions
		 0.5f,  0.5f,	// top right
		 0.5f, -0.5f,	// bottom right
		-0.5f, -0.5f,	// bottom left
		-0.5f,  0.5f,	// top left 
		 0.5f,  0.5f,	// top right
		-0.5f, -0.5f	// bottom left
	};

	float uiTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		 0.5f, -0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f,		0.0f, 1.0f,		// top left 
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
	};

	float offscreenTexVerts[] =
	{	// positions		// texcoords
		 1.0f,  1.0f,		1.0f, 1.0f,		// top right
		 1.0f, -1.0f,		1.0f, 0.0f,		// bottom right
		-1.0f, -1.0f,		0.0f, 0.0f,		// bottom left
		-1.0f,  1.0f,		0.0f, 1.0f,		// top left 
		 1.0f,  1.0f,		1.0f, 1.0f,		// top right
		-1.0f, -1.0f,		0.0f, 0.0f,		// bottom left
	};

	float spritesheetBasicTexVerts[] =
	{	// positions		// texcoords
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		 0.5f, -0.5f,		1.0f, 0.0f,		// bottom right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
		-0.5f,  0.5f,		0.0f, 1.0f,		// top left 
		 0.5f,  0.5f,		1.0f, 1.0f,		// top right
		-0.5f, -0.5f,		0.0f, 0.0f,		// bottom left
	};


	InitFontRenderer(fontData, 128);

	ShaderSource shaderSource;

	unsigned int *shaderIds = renderer->shaderIds;
	shaderIds[Shaders::FONT_SHADER] = GetShader("Resources/Shaders/Font.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::QUAD_TEXTURED_SHADER] = GetShader("Resources/Shaders/Quad.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::PARTICLE_PLAIN_SHADER] = GetShader("Resources/Shaders/ParticlePlain.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::PARTICLE_TEXTURED_SHADER] = GetShader("Resources/Shaders/ParticleTex.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::TILEMAP_SHADER] = GetShader("Resources/Shaders/Tilemap.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::UI_TEXTURED_SHADER] = GetShader("Resources/Shaders/UITex.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::UI_PLAIN_SHADER] = GetShader("Resources/Shaders/UIPlain.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::MEMORY_PIECE_SHADER] = GetShader("Resources/Shaders/MemoryPiece.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::HORIZONTAL_GAUSSIAN_BLUR_SHADER] = GetShader("Resources/Shaders/HorizontalGaussianBlur.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::VERTICAL_GAUSSIAN_BLUR_SHADER] = GetShader("Resources/Shaders/VerticalGaussianBlur.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::POST_PROCESS_SHADER] = GetShader("Resources/Shaders/PostProcess.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::SCREENSHAKE_SHADER] = GetShader("Resources/Shaders/Screenshake.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::SPRITESHEET_BASIC_SHADER] = GetShader("Resources/Shaders/SpritesheetBasic.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::OUTLINE_SHADER] = GetShader("Resources/Shaders/Outline.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::INSTANCED_OUTLINE_SHADER] = GetShader("Resources/Shaders/InstancedOutline.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::EDGE_DETECTION_SHADER] = GetShader("Resources/Shaders/EdgeDetection.shader", &shaderSource, tempMemory);
	shaderIds[Shaders::LINE_SHADER] = GetShader("Resources/Shaders/Line.shader", &shaderSource, tempMemory);

	Texture2D *textures = renderer->textures;
	GetTexture("Resources//Textures//Background.jpg", &textures[Textures::BACKGROUND_TEX]);
	GetTexture("Resources//Textures//Elf.png", &textures[Textures::SHOOTER_TEX]);
	GetTexture("Resources//Textures//Stick.png", &textures[Textures::GUN_TEX]);
	GetTexture("Resources//Textures//BasicBall.png", &textures[Textures::BASIC_BALL_TEX]);
	GetTexture("Resources//Textures//BubbleBall.png", &textures[Textures::BUBBLE_BALL_TEX]);
	GetTexture("Resources//Textures//ExplosionBall.png", &textures[Textures::EXPLODE_BALL_TEX]);
	GetTexture("Resources//Textures//walls.png", &textures[Textures::WALL_TEX]);
	GetTexture("Resources//Textures//Tilemap.png", &textures[Textures::TILE_TEXES]);
	GetTexture("Resources/Textures/Tree.png", &textures[Textures::TREE_TEX]);
	GetTexture("Resources//Textures//BoxCollider.png", &textures[Textures::BOX_COLLIDER_TEX]);
	GetTexture("Resources//Textures//CircleCollider.png", &textures[Textures::CIRCLE_COLLIDER_TEX]);
	GetTexture("Resources//Textures//explosion.png", &textures[Textures::EXPLODE_PARTICLE_TEXES]);
	GetTexture("Resources//Textures//Jigsaw.png", &textures[Textures::JIGSAW_TEXES]);
	GetTexture("Resources//Textures//Balls.png", &textures[Textures::BALLS_TEXES]);
	GetTexture("Resources//Textures//SnowSplash.png", &textures[Textures::SNOW_SPLASH_TEXES]);
	GetTexture("Resources//Textures//Snowflake1.png", &textures[Textures::SNOWFLAKE_TEXES]);
	GetTexture("Resources//Textures//Blackboard.png", &textures[Textures::BLACKBOARD_TEX]);
	GetTexture("Resources//Textures//JigsawUI.png", &textures[Textures::JIGSAW_UI_TEXES]);
	GetTexture("Resources//Textures//Unlocked.png", &textures[Textures::UNLOCK]);

	Texture2D *nodeTextures = renderer->nodeTextures;
	GetTexture("Resources//Textures//Story//Start.png", &nodeTextures[NodeType::BEGIN_NODE]);
	GetTexture("Resources//Textures//Story//Level1//0.png", &nodeTextures[NodeType::LEVEL1_NODE0]);
	GetTexture("Resources//Textures//Story//Level1//1.png", &nodeTextures[NodeType::LEVEL1_NODE1]);

	GetTexture("Resources//Textures//Story//Level2//00.png", &nodeTextures[NodeType::LEVEL2_NODE0]);
	GetTexture("Resources//Textures//Story//Level2//01.png", &nodeTextures[NodeType::LEVEL2_NODE1]);
	GetTexture("Resources//Textures//Story//Level2//10.png", &nodeTextures[NodeType::LEVEL2_NODE2]);
	GetTexture("Resources//Textures//Story//Level2//11.png", &nodeTextures[NodeType::LEVEL2_NODE3]);

	GetTexture("Resources//Textures//Story//Level3//000.png", &nodeTextures[NodeType::LEVEL3_NODE0]);
	GetTexture("Resources//Textures//Story//Level3//001.png", &nodeTextures[NodeType::LEVEL3_NODE1]);
	GetTexture("Resources//Textures//Story//Level3//010.png", &nodeTextures[NodeType::LEVEL3_NODE2]);
	GetTexture("Resources//Textures//Story//Level3//011.png", &nodeTextures[NodeType::LEVEL3_NODE3]);
	GetTexture("Resources//Textures//Story//Level3//100.png", &nodeTextures[NodeType::LEVEL3_NODE4]);
	GetTexture("Resources//Textures//Story//Level3//101.png", &nodeTextures[NodeType::LEVEL3_NODE5]);
	GetTexture("Resources//Textures//Story//Level3//110.png", &nodeTextures[NodeType::LEVEL3_NODE6]);
	GetTexture("Resources//Textures//Story//Level3//111.png", &nodeTextures[NodeType::LEVEL3_NODE7]);

	GetTexture("Resources//Textures//Story//Level4//0000.png", &nodeTextures[NodeType::LEVEL4_NODE0]);
	GetTexture("Resources//Textures//Story//Level4//0001.png", &nodeTextures[NodeType::LEVEL4_NODE1]);
	GetTexture("Resources//Textures//Story//Level4//0010.png", &nodeTextures[NodeType::LEVEL4_NODE2]);
	GetTexture("Resources//Textures//Story//Level4//0011.png", &nodeTextures[NodeType::LEVEL4_NODE3]);
	GetTexture("Resources//Textures//Story//Level4//0100.png", &nodeTextures[NodeType::LEVEL4_NODE4]);
	GetTexture("Resources//Textures//Story//Level4//0101.png", &nodeTextures[NodeType::LEVEL4_NODE5]);
	GetTexture("Resources//Textures//Story//Level4//0110.png", &nodeTextures[NodeType::LEVEL4_NODE6]);
	GetTexture("Resources//Textures//Story//Level4//0111.png", &nodeTextures[NodeType::LEVEL4_NODE7]);
	GetTexture("Resources//Textures//Story//Level4//1000.png", &nodeTextures[NodeType::LEVEL4_NODE8]);
	GetTexture("Resources//Textures//Story//Level4//1001.png", &nodeTextures[NodeType::LEVEL4_NODE9]);
	GetTexture("Resources//Textures//Story//Level4//1010.png", &nodeTextures[NodeType::LEVEL4_NODE10]);
	/*GetTexture("Resources//Textures//Story//Level4//1011.png", &nodeTextures[NodeType::LEVEL4_NODE11]);
	GetTexture("Resources//Textures//Story//Level4//1100.png", &nodeTextures[NodeType::LEVEL4_NODE12]);
	GetTexture("Resources//Textures//Story//Level4//1101.png", &nodeTextures[NodeType::LEVEL4_NODE13]);*/
	GetTexture("Resources//Textures//Story//Level4//1110.png", &nodeTextures[NodeType::LEVEL4_NODE14]);
	GetTexture("Resources//Textures//Story//Level4//1111.png", &nodeTextures[NodeType::LEVEL4_NODE15]);

	GetTexture("Resources//Textures//Story//Level5//00000.png", &nodeTextures[NodeType::LEVEL5_NODE0]);
	GetTexture("Resources//Textures//Story//Level5//00001.png", &nodeTextures[NodeType::LEVEL5_NODE1]);
	GetTexture("Resources//Textures//Story//Level5//00010.png", &nodeTextures[NodeType::LEVEL5_NODE2]);
	GetTexture("Resources//Textures//Story//Level5//00011.png", &nodeTextures[NodeType::LEVEL5_NODE3]);
	GetTexture("Resources//Textures//Story//Level5//00100.png", &nodeTextures[NodeType::LEVEL5_NODE4]);
	GetTexture("Resources//Textures//Story//Level5//00101.png", &nodeTextures[NodeType::LEVEL5_NODE5]);
	GetTexture("Resources//Textures//Story//Level5//00110.png", &nodeTextures[NodeType::LEVEL5_NODE6]);
	GetTexture("Resources//Textures//Story//Level5//00111.png", &nodeTextures[NodeType::LEVEL5_NODE7]);
	GetTexture("Resources//Textures//Story//Level5//01000.png", &nodeTextures[NodeType::LEVEL5_NODE8]);
	GetTexture("Resources//Textures//Story//Level5//01001.png", &nodeTextures[NodeType::LEVEL5_NODE9]);
	GetTexture("Resources//Textures//Story//Level5//01010.png", &nodeTextures[NodeType::LEVEL5_NODE10]);
	GetTexture("Resources//Textures//Story//Level5//01011.png", &nodeTextures[NodeType::LEVEL5_NODE11]);
	GetTexture("Resources//Textures//Story//Level5//01100.png", &nodeTextures[NodeType::LEVEL5_NODE12]);
	GetTexture("Resources//Textures//Story//Level5//01101.png", &nodeTextures[NodeType::LEVEL5_NODE13]);
	GetTexture("Resources//Textures//Story//Level5//01110.png", &nodeTextures[NodeType::LEVEL5_NODE14]);
	GetTexture("Resources//Textures//Story//Level5//01111.png", &nodeTextures[NodeType::LEVEL5_NODE15]);
	GetTexture("Resources//Textures//Story//Level5//10000.png", &nodeTextures[NodeType::LEVEL5_NODE16]);
	GetTexture("Resources//Textures//Story//Level5//10001.png", &nodeTextures[NodeType::LEVEL5_NODE17]);
	GetTexture("Resources//Textures//Story//Level5//10010.png", &nodeTextures[NodeType::LEVEL5_NODE18]);
	GetTexture("Resources//Textures//Story//Level5//10011.png", &nodeTextures[NodeType::LEVEL5_NODE19]);
	GetTexture("Resources//Textures//Story//Level5//10100.png", &nodeTextures[NodeType::LEVEL5_NODE20]);
	GetTexture("Resources//Textures//Story//Level5//10101.png", &nodeTextures[NodeType::LEVEL5_NODE21]);
	/*GetTexture("Resources//Textures//Story//Level5//10110.png", &nodeTextures[NodeType::LEVEL5_NODE22]);
	GetTexture("Resources//Textures//Story//Level5//10111.png", &nodeTextures[NodeType::LEVEL5_NODE23]);
	GetTexture("Resources//Textures//Story//Level5//11000.png", &nodeTextures[NodeType::LEVEL5_NODE24]);
	GetTexture("Resources//Textures//Story//Level5//11001.png", &nodeTextures[NodeType::LEVEL5_NODE25]);
	GetTexture("Resources//Textures//Story//Level5//11010.png", &nodeTextures[NodeType::LEVEL5_NODE26]);
	GetTexture("Resources//Textures//Story//Level5//11011.png", &nodeTextures[NodeType::LEVEL5_NODE27]);*/
	GetTexture("Resources//Textures//Story//Level5//11100.png", &nodeTextures[NodeType::LEVEL5_NODE28]);
	GetTexture("Resources//Textures//Story//Level5//11101.png", &nodeTextures[NodeType::LEVEL5_NODE29]);
	GetTexture("Resources//Textures//Story//Level5//11110.png", &nodeTextures[NodeType::LEVEL5_NODE30]);
	GetTexture("Resources//Textures//Story//Level5//11111.png", &nodeTextures[NodeType::LEVEL5_NODE31]);

	glm::mat4 projection = glm::ortho(0.0f, (float)canvasWidth, 0.0f, (float)canvasHeight, -100.0f, 100.0f);
	renderer->projection = projection;
	for (int i = 0; i < Shaders::SHADER_COUNT; i++)
	{
		UseShader(shaderIds[i]);
		SetUniform4x4(shaderIds[i], "projection", projection);
	}

	DottedLine *line = &renderer->dottedLine;
	line->from = glm::vec2(200.0f, 200.0f);
	line->color = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f);
	line->length = 200.0f;
	line->segmentLength = 4.0f;
	line->voidLength = 3.0f;
	line->thickness = 2.0f;
	SetupDottedLine(line, tempMemory);

	unsigned int *vaos = renderer->vaos;

	int memoryPieceAttributes[1] = { 4 };
	vaos[VAOS::MEMORY_PIECE_VAO] = CreateVAO(sizeof(memoryPieceTexVerts), memoryPieceTexVerts, memoryPieceAttributes, 1, 0);

	int quadTexAttributes[1] = { 4 };
	vaos[VAOS::QUAD_TEXTURED_VAO] = CreateVAO(sizeof(quadTexVerts), quadTexVerts, quadTexAttributes, 1, 0);

	int bottomCentreTexAttributes[1] = { 4 };
	vaos[VAOS::BOTTOM_CENTRE_VAO] = CreateVAO(sizeof(bottomCentreTexVerts), bottomCentreTexVerts, bottomCentreTexAttributes, 1, 0);

	int bottomRightTexAttributes[1] = { 4 };
	vaos[VAOS::BOTTOM_RIGHT_VAO] = CreateVAO(sizeof(bottomRightTexVerts), bottomRightTexVerts, bottomRightTexAttributes, 1, 0);

	int bottomLeftTexAttributes[1] = { 4 };
	vaos[VAOS::BOTTOM_LEFT_VAO] = CreateVAO(sizeof(bottomLeftTexVerts), bottomLeftTexVerts, bottomLeftTexAttributes, 1, 0);

	int particlePlainAttributes[1] = { 2 };
	vaos[VAOS::PARTICLE_PLAIN_VAO] = CreateVAO(sizeof(particlePlainVerts), particlePlainVerts, particlePlainAttributes, 1, 0);

	int particleTexAttributes[1] = { 4 };
	vaos[VAOS::PARTICLE_TEX_VAO] = CreateVAO(sizeof(particleTexVerts), particleTexVerts, particleTexAttributes, 1, 0);

	int uiPlainAttributes[1] = { 2 };
	vaos[VAOS::UI_PLAIN_VAO] = CreateVAO(sizeof(uiPlainVerts), uiPlainVerts, uiPlainAttributes, 1, 0);

	int uiTexAttributes[1] = { 4 };
	vaos[VAOS::UI_TEXTURED_VAO] = CreateVAO(sizeof(uiTexVerts), uiTexVerts, uiTexAttributes, 1, 0);

	int offscreenTexAttributes[1] = { 4 };
	vaos[VAOS::OFFSCREEN_TEXTURE_VAO] = CreateVAO(sizeof(offscreenTexVerts), offscreenTexVerts, offscreenTexAttributes, 1, 0);

	int spritesheetBasicTexAttributes[1] = { 4 };
	vaos[VAOS::SPRITESHEET_BASIC_VAO] = CreateVAO(sizeof(spritesheetBasicTexVerts), spritesheetBasicTexVerts, spritesheetBasicTexAttributes, 1, 0);

	glViewport(0, 0, canvasWidth, canvasHeight);

	CreateSpritesheetVAO(quadTexVerts, sizeof(quadTexVerts), TILEMAP_COUNT_X * TILEMAP_COUNT_Y, renderer->tilemapRenderData, &renderer->tilemapVertexBufferData);
	CreateSpritesheetVAO(quadTexVerts, sizeof(quadTexVerts), MAX_BALLS, renderer->ballsRenderData, &renderer->ballsVertexBufferData);
	CreateSpritesheetVAO(quadTexVerts, sizeof(quadTexVerts), MAX_SNOW_PARTICLES, renderer->snowRenderData, &renderer->snowVertexBufferData);
	CreateParticleSystemVAO(particleTexVerts, sizeof(particleTexVerts));

	unsigned int *texIds = renderer->texIds;
	unsigned int *texStatus = renderer->texStatus;
	for (int i = 0; i < Textures::TEX_COUNT; i++)
	{
		if (i < (MAX_TEXTURE_SLOTS - 1))
		{
			AddTexId(textures[i].id, texStatus, texIds);
		}
	}

	ParticleEmitter *emitters = renderer->particleEmitters;
	emitters[ParticleSystems::BALL_EXPLOSION].initializeEmitter = InitializeExplosionParticleSystem;

	renderer->horizontalBlur = CreateFramebuffer(canvasWidth, canvasHeight);
	renderer->verticleBlur = CreateFramebuffer(canvasWidth, canvasHeight);
	renderer->postProcess = CreateFramebuffer(canvasWidth, canvasHeight);

	strcpy_s(renderer->gameButtonTexts[Texts::MAIN_MENU_PLAY_BUTTON], "Play");
	strcpy_s(renderer->gameButtonTexts[Texts::MAIN_MENU_EXIT_BUTTON], "Exit");
	strcpy_s(renderer->gameButtonTexts[Texts::GAME_MENU_RELOAD_LEVEL_BUTTON], "Reload");
	strcpy_s(renderer->gameButtonTexts[Texts::GAME_MENU_SELECT_LEVEL_BUTTON], "Levels");
	strcpy_s(renderer->gameButtonTexts[Texts::GAME_MENU_EXIT_GAME_BUTTON], "Quit");
	//renderer->texts[Texts::MAIN_MENU_PLAY_BUTTON] = "Hello";

	SpriteAnimator *explosionBallAnimator = &renderer->explosionBallAnimator;
	InitiateSpriteAnimator(explosionBallAnimator, 2, 5, 8, -1);

	renderer->maxShakeTime = 2.0f;

	InstantiateSnowfallSystem();
}



#pragma region RenderUIElements

void RenderOverlayPanel(Renderer *renderer, UIElement *element)
{
	unsigned int *shaderIds = renderer->shaderIds;
	UseShader(shaderIds[Shaders::UI_PLAIN_SHADER]);
	glm::mat4 model = glm::mat4(1.0f);
	glm::vec4 color = element->normalColor;

	model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
	model = glm::scale(model, glm::vec3(element->width, element->height, 1.0f));

	SetUniform4x4(shaderIds[Shaders::UI_PLAIN_SHADER], "model", model);
	SetUniform4f(shaderIds[Shaders::UI_PLAIN_SHADER], "color", color.r, color.g, color.b, color.a);
	DrawQuad(renderer->vaos[VAOS::UI_PLAIN_VAO]);
}

void RenderPlainPanel(Renderer *renderer, UIElement *element)
{
	if (element->isActive)
	{
		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::UI_PLAIN_SHADER]);
		glm::mat4 model = glm::mat4(1.0f);
		glm::vec4 color = element->normalColor;

		model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
		model = glm::scale(model, glm::vec3(element->width, element->height, 1.0f));

		if (element->isMouseOver)
		{
			color = element->isMouseOverColor;
		}
		else if (element->isMouseDown)
		{
			color = element->isMouseDownColor;
		}

		SetUniform4x4(shaderIds[Shaders::UI_PLAIN_SHADER], "model", model);
		SetUniform4f(shaderIds[Shaders::UI_PLAIN_SHADER], "color", color.r, color.g, color.b, color.a);
		DrawQuad(renderer->vaos[VAOS::UI_PLAIN_VAO]);
	}
}

void RenderTexPanel(Renderer *renderer, UIElement *element, unsigned int texSlot)
{
	if (element->isActive)
	{
		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::UI_TEXTURED_SHADER]);
		glm::mat4 model = glm::mat4(1.0f);
		glm::vec4 color = element->normalColor;

		model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
		model = glm::scale(model, glm::vec3(element->width, element->height, 1.0f));

		if (element->isMouseOver)
		{
			color = element->isMouseOverColor;
		}
		else if (element->isMouseDown)
		{
			color = element->isMouseDownColor;
		}

		SetUniform4x4(shaderIds[Shaders::UI_TEXTURED_SHADER], "model", model);
		SetUniform4f(shaderIds[Shaders::UI_TEXTURED_SHADER], "color", color.r, color.g, color.b, color.a);
		SetUniform1i(shaderIds[Shaders::UI_TEXTURED_SHADER], "mainTex", texSlot);
		DrawQuad(renderer->vaos[VAOS::UI_TEXTURED_VAO]);
	}
}

void RenderPlainButton(Renderer *renderer, UIElement *element, glm::vec3 textColor)
{
	if (element->isActive)
	{
		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::UI_PLAIN_SHADER]);
		glm::vec2 pos;
		glm::vec2 scale;
		glm::vec4 color;

		if (element->isInteractable)
		{
			if (element->isMouseOver)
			{
				if (element->isMouseDown)
				{
					pos = glm::vec2(element->worldPosition.x, element->worldPosition.y - 2.0f);
					scale = glm::vec2(element->width * 0.95f, element->height * 0.95f);
					color = element->isMouseDownColor;
				}
				else
				{
					if (element->scaleUpOnMouseOver)
					{
						pos = glm::vec2(element->worldPosition.x, element->worldPosition.y + 2.0f);
						scale = glm::vec2(element->width * 1.05, element->height * 1.05f);
					}
					else
					{
						pos = glm::vec2(element->worldPosition);
						scale = glm::vec2(element->width, element->height);
					}
					color = element->isMouseOverColor;
				}
			}
			else
			{
				pos = glm::vec2(element->worldPosition);
				scale = glm::vec2(element->width, element->height);
				color = element->normalColor;
			}
		}
		else
		{
			pos = glm::vec2(element->worldPosition);
			scale = glm::vec2(element->width, element->height);
			color = element->nonInteractableColor;
		}

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(pos, 0.0f));
		model = glm::scale(model, glm::vec3(scale, 1.0f));

		SetUniform4x4(shaderIds[Shaders::UI_PLAIN_SHADER], "model", model);
		SetUniform4f(shaderIds[Shaders::UI_PLAIN_SHADER], "color", color.r, color.g, color.b, color.a);
		DrawQuad(renderer->vaos[VAOS::UI_PLAIN_VAO]);

		if (element->text)
		{
			RenderText(renderer->shaderIds[Shaders::FONT_SHADER], &renderer->fontData, element->text,
					   pos.x, pos.y, 1.0, textColor, UNBOOKED_SLOT);
		}
	}
}

void RenderPlainButton(Renderer *renderer, UIElement *element)
{
	if (element->isActive)
	{
		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::UI_PLAIN_SHADER]);
		glm::vec2 pos;
		glm::vec2 scale;
		glm::vec4 color;

		if (element->isInteractable)
		{
			if (element->isMouseOver)
			{
				if (element->isMouseDown)
				{
					pos = glm::vec2(element->worldPosition.x, element->worldPosition.y - 2.0f);
					scale = glm::vec2(element->width * 0.95f, element->height * 0.95f);
					color = element->isMouseDownColor;
				}
				else
				{
					if (element->scaleUpOnMouseOver)
					{
						pos = glm::vec2(element->worldPosition.x, element->worldPosition.y + 2.0f);
						scale = glm::vec2(element->width * 1.05, element->height * 1.05f);
					}
					else
					{
						pos = glm::vec2(element->worldPosition);
						scale = glm::vec2(element->width, element->height);
					}
					color = element->isMouseOverColor;
				}
			}
			else
			{
				pos = glm::vec2(element->worldPosition);
				scale = glm::vec2(element->width, element->height);
				color = element->normalColor;
			}
		}
		else
		{
			pos = glm::vec2(element->worldPosition);
			scale = glm::vec2(element->width, element->height);
			color = element->nonInteractableColor;
		}

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(pos, 0.0f));
		model = glm::scale(model, glm::vec3(scale, 1.0f));

		SetUniform4x4(shaderIds[Shaders::UI_PLAIN_SHADER], "model", model);
		SetUniform4f(shaderIds[Shaders::UI_PLAIN_SHADER], "color", color.r, color.g, color.b, color.a);
		DrawQuad(renderer->vaos[VAOS::UI_PLAIN_VAO]);

		if (element->text)
		{
			RenderText(renderer->shaderIds[Shaders::FONT_SHADER], &renderer->fontData, element->text,
				pos.x, pos.y, 1.0, glm::vec3(1.0f, 1.0f, 1.0f), UNBOOKED_SLOT);
		}
	}
}

void RenderTexButton(Renderer *renderer, UIElement *element)
{
	if (element->texture != 0)
	{
		UseTexture2D(element->texture->id, UNBOOKED_SLOT);
		RenderTexButton(renderer, element, UNBOOKED_SLOT);
	}
	else
	{
		RenderPlainButton(renderer, element);
	}
}

void RenderTexButton(Renderer *renderer, UIElement *element, unsigned int texSlot)
{
	if (element->isActive)
	{
		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::UI_TEXTURED_SHADER]);
		glm::vec2 pos;
		glm::vec2 scale;
		glm::vec4 color;

		if (element->isInteractable)
		{
			if (element->isMouseOver)
			{
				if (element->isMouseDown)
				{
					pos = glm::vec2(element->worldPosition.x, element->worldPosition.y - 2.0f);
					scale = glm::vec2(element->width * 0.95f, element->height * 0.95f);
					color = element->isMouseDownColor;
				}
				else
				{
					if (element->scaleUpOnMouseOver)
					{
						pos = glm::vec2(element->worldPosition.x, element->worldPosition.y + 2.0f);
						scale = glm::vec2(element->width * 1.05, element->height * 1.05f);
					}
					else
					{
						pos = glm::vec2(element->worldPosition);
						scale = glm::vec2(element->width, element->height);
					}
					color = element->isMouseOverColor;
				}
			}
			else
			{
				pos = glm::vec2(element->worldPosition);
				scale = glm::vec2(element->width, element->height);
				color = element->normalColor;
			}
		}
		else
		{
			pos = glm::vec2(element->worldPosition);
			scale = glm::vec2(element->width, element->height);
			color = element->nonInteractableColor;
		}

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
		model = glm::scale(model, glm::vec3(scale, 1.0f));

		SetUniform4x4(shaderIds[Shaders::UI_TEXTURED_SHADER], "model", model);
		SetUniform4f(shaderIds[Shaders::UI_TEXTURED_SHADER], "color", color.r, color.g, color.b, color.a);
		SetUniform1i(shaderIds[Shaders::UI_TEXTURED_SHADER], "mainTex", texSlot);
		DrawQuad(renderer->vaos[VAOS::UI_TEXTURED_VAO]);

	}
}

#pragma endregion


#pragma region RenderSceneUIs

void RenderMainMenuSceneUI()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	UIElement *uiElements = game->currentSceneInfo.uiElements;
	RenderPlainPanel(renderer, &uiElements[MainMenuUIElements::MENU_CONTAINER_PANEL]);
	RenderPlainButton(renderer, &uiElements[MainMenuUIElements::PLAY_GAME_BUTTON]);
	RenderPlainButton(renderer, &uiElements[MainMenuUIElements::EXIT_GAME_BUTTON]);
	//RenderPlainPanel(renderer, &uiElements[MainMenuUIElements::OVERLAY_PANEL_MAIN_MENU]);
	RenderOverlayPanel(renderer, &uiElements[MainMenuUIElements::OVERLAY_PANEL_MAIN_MENU]);
}

void RenderLevelUI()
{
	Game *game = GetGameMemory();
	float currentTime = game->currentTime;
	int pickedBall = game->pickedBall;
	Renderer *renderer = &game->renderer;
	UIElement *uiElements = game->currentSceneInfo.uiElements;
	Texture2D *textures = renderer->textures;
	unsigned int *texIds = renderer->texIds;
	SpritesheetRenderData *ballRenderData = renderer->ballsRenderData;
	SpritesheetVertexBufferData *ballVertexBufferData = &renderer->ballsVertexBufferData;

	RenderPlainPanel(renderer, &uiElements[GameUIElements::AIM_PANEL]);

	//RenderTexPanel(renderer, &uiElements[GameUIElements::TREE_PANEL], GetTexSlot(textures[Textures::TREE_TEX].id, texIds));

	glEnable(GL_STENCIL_TEST);
	glClear(GL_STENCIL_BUFFER_BIT);

	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilMask(0xFF);

	BallSlot *ballSlots = game->currentLevelInfo.ballSlots;
	int renderIndex = 0;
	for (int i = 0; i < MAX_BALLS; i++)
	{
		UIElement *element = &uiElements[i + GameUIElements::BALL_SELECT_BUTTON_FIRST];

		if (element->isActive)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
			model = glm::scale(model, glm::vec3(element->width, element->height, 0.0f));

			int ballIndex = GetBallIndex((BallType)ballSlots[i]);

			ballRenderData[renderIndex].model = model;
			ballRenderData[renderIndex].index = ballIndex;

			renderIndex++;
		}
	}

	RenderInstancedSpritesheet(ballVertexBufferData, ballRenderData, renderIndex, GetTexSlot(textures[Textures::BALLS_TEXES].id, texIds), 2, 3, Shaders::TILEMAP_SHADER);

	if (!pickedBall)
	{
		glStencilFunc(GL_EQUAL, 0, 0xFF);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		glStencilMask(0xFF);

		renderIndex = 0;
		for (int i = 0; i < MAX_BALLS; i++)
		{
			UIElement *element = &uiElements[i + GameUIElements::BALL_SELECT_BUTTON_FIRST];

			if (element->isActive)
			{
				glm::mat4 model = glm::mat4(1.0f);
				model = glm::translate(model, glm::vec3(element->worldPosition, 0.0f));
				model = glm::scale(model, glm::vec3(element->width * 1.2f, element->height * 1.2f, 0.0f));

				int ballIndex = GetBallIndex((BallType)ballSlots[i]);

				ballRenderData[renderIndex].model = model;
				ballRenderData[renderIndex].index = ballIndex;

				renderIndex++;
			}
		}

		float frequency = 8.0f;

		unsigned int *shaderIds = renderer->shaderIds;
		UseShader(shaderIds[Shaders::INSTANCED_OUTLINE_SHADER]);
		SetUniform4f(shaderIds[Shaders::INSTANCED_OUTLINE_SHADER], "color", 0.0f, 1.0f, 0.0f, (sinf(currentTime * frequency) * 0.5f) + 0.5f);
		RenderInstancedSpritesheet(ballVertexBufferData, ballRenderData, renderIndex, GetTexSlot(textures[Textures::BALLS_TEXES].id, texIds), 2, 3, Shaders::INSTANCED_OUTLINE_SHADER);

	}

	glStencilMask(0xFF);
	glDisable(GL_STENCIL_TEST);
}

void RenderGameSceneUI()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	UIElement *uiElements = game->currentSceneInfo.uiElements;
	Texture2D *textures = renderer->textures;
	unsigned int *texIds = renderer->texIds;

	RenderPlainPanel(renderer, &uiElements[GameUIElements::MENU_CONTAINER_PANEL_GAME]);
	for (int i = GameUIElements::RESTART_LEVEL_BUTTON_GAME; i < GameUIElements::OVERLAY_PANEL_GAME; i++)
	{
		RenderPlainButton(renderer, &uiElements[i], glm::vec3(1.0f, 0.0f, 1.0f));
	}

	RenderOverlayPanel(renderer, &uiElements[GameUIElements::OVERLAY_PANEL_GAME]);
}

void RenderLevelSelectorSceneUI()
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	UIElement *uiElements = game->currentSceneInfo.uiElements;

	RenderTexButton(renderer, &uiElements[LevelSelectorUIElements::BEGIN_NODE_UI]);

	for (int i = LevelSelectorUIElements::MEMORY_FIRST; i <= LevelSelectorUIElements::MEMORY_LAST; i++)
	{
		RenderTexButton(renderer, &uiElements[i]);
	}

	RenderOverlayPanel(renderer, &uiElements[LevelSelectorUIElements::OVERLAY_PANEL_LEVEL_SELECTOR]);
}

#pragma endregion


#pragma region RenderEntities

void RenderInstancedSpritesheet(SpritesheetVertexBufferData *spritesheetVertexBufferData, SpritesheetRenderData *spritesheetRenderData, int instanceCount,
	int texSlot, int rows, int cols, Shaders shaderToUse)
{
	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	Texture2D *textures = renderer->textures;

	UseShader(shaderIds[shaderToUse]);
	SetUniform1i(shaderIds[shaderToUse], "sheetTex", texSlot);
	SetUniform1i(shaderIds[shaderToUse], "rows", rows);
	SetUniform1i(shaderIds[shaderToUse], "cols", cols);

	glBindBuffer(GL_ARRAY_BUFFER, spritesheetVertexBufferData->vboI);
	glBufferSubData(GL_ARRAY_BUFFER, 0, instanceCount * sizeof(SpritesheetRenderData), spritesheetRenderData);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(spritesheetVertexBufferData->vao);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, instanceCount);
	glBindVertexArray(0);
}



void InstantiateSnowfallSystem()
{
	Renderer *renderer = &GetGameMemory()->renderer;
	SnowfallSystem *snowfallSystem = &renderer->snowfallSystem;
	SnowParticle *snowParticles = snowfallSystem->snowParticles;

	snowfallSystem->topLeft = glm::vec2(96.0f - 40.0f, 624.0f + 40.0f);
	snowfallSystem->bottomRight = glm::vec2(1184.0f + 40.0f, 132.0f - 40.0f);

	snowfallSystem->minDirectionX = -0.1f;
	snowfallSystem->maxDirectionX = 2.9f;
	snowfallSystem->minDirectionY = -1.0f;
	snowfallSystem->maxDirectionY = -1.0f;
	snowfallSystem->minSpeed = 20.0f;
	snowfallSystem->maxSpeed = 420.0f;
	snowfallSystem->minSpeedDeviation = -5.0f;
	snowfallSystem->maxSpeedDeviation = 5.0f;
	snowfallSystem->minDeviationY = -0.0f; // Direction deviation per second;
	snowfallSystem->maxDeviationY = 0.0f;
	snowfallSystem->minDeviationX = -0.5f;
	snowfallSystem->maxDeviationX = 0.5f;
	snowfallSystem->minRadius = 20.0f;
	snowfallSystem->maxRadius = 12.0f;

	for (int i = 0; i < MAX_SNOW_PARTICLES; i++)
	{
		snowParticles[i].position = glm::vec2(GetRandomNumber(snowfallSystem->topLeft.x, snowfallSystem->bottomRight.x),
			GetRandomNumber(snowfallSystem->bottomRight.y, snowfallSystem->topLeft.y));
		snowParticles[i].radius = GetRandomNumber(snowfallSystem->minRadius, snowfallSystem->maxRadius);
		snowParticles[i].direction = glm::normalize(glm::vec2(GetRandomNumber(snowfallSystem->minDirectionX, snowfallSystem->maxDirectionX),
			GetRandomNumber(snowfallSystem->minDirectionY, snowfallSystem->maxDirectionY)));
		snowParticles[i].speed = GetRandomNumber(snowfallSystem->minSpeed, snowfallSystem->maxSpeed);
	}
}

void UpdateSnowParticle(SnowfallSystem *snowfallSystem, SnowParticle *snowParticle, float deltaTime)
{
	snowParticle->direction += (glm::vec2(GetRandomNumber(snowfallSystem->minDeviationX, snowfallSystem->maxDeviationX),
		GetRandomNumber(snowfallSystem->minDeviationY, snowfallSystem->maxDeviationY)) * deltaTime);
	snowParticle->direction = glm::normalize(snowParticle->direction);

	snowParticle->speed += (GetRandomNumber(snowfallSystem->minSpeedDeviation, snowfallSystem->maxSpeedDeviation) * deltaTime);

	snowParticle->position += (snowParticle->direction * snowParticle->speed * deltaTime);

	float deltaX = snowfallSystem->bottomRight.x - snowfallSystem->topLeft.x;
	float deltaY = snowfallSystem->topLeft.y - snowfallSystem->bottomRight.y;

	float particleDeltaX = snowParticle->position.x - snowfallSystem->topLeft.x;
	float particleDeltaY = snowParticle->position.y - snowfallSystem->bottomRight.y;
	particleDeltaX = GetMod(particleDeltaX, deltaX);
	particleDeltaY = GetMod(particleDeltaY, deltaY);

	snowParticle->position = glm::vec2(snowfallSystem->topLeft.x + particleDeltaX, snowfallSystem->bottomRight.y + particleDeltaY);
}

void RenderSnowfall()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	Renderer *renderer = &game->renderer;
	Texture2D *textures = renderer->textures;
	SnowfallSystem *snowfallSystem = &renderer->snowfallSystem;
	SnowParticle *snowParticles = snowfallSystem->snowParticles;

	SpritesheetRenderData *snowfallRenderData = renderer->snowRenderData;
	SpritesheetVertexBufferData *snowfallVertexBufferData = &renderer->snowVertexBufferData;

	for (int i = 0; i < MAX_SNOW_PARTICLES; i++)
	{
		UpdateSnowParticle(snowfallSystem, &snowParticles[i], deltaTime);

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(snowParticles[i].position, 0.0f));
		model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::scale(model, glm::vec3(snowParticles[i].radius, snowParticles[i].radius, 1.0f));

		snowfallRenderData[i].index = 0;
		snowfallRenderData[i].model = model;
	}

	UseTexture2D(textures[Textures::SNOWFLAKE_TEXES].id, UNBOOKED_SLOT);
	RenderInstancedSpritesheet(snowfallVertexBufferData, snowfallRenderData, MAX_SNOW_PARTICLES, UNBOOKED_SLOT, 1, 1, Shaders::TILEMAP_SHADER);
}

void AddTileToDestroy(TileType tileType, glm::vec2 position, glm::vec2 scale)
{
	Renderer *renderer = &GetGameMemory()->renderer;
	TileDestroyData *tileDestroyData = renderer->tileDestroyInfos;
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(position, 1.0f));
	model = glm::scale(model, glm::vec3(scale, 1.0f));

	int loopFromIndex = 0;
	int loopToIndex = 0;
	int loopsToExecute = 1;
	int framesPerSecond = 32;

	switch (tileType)
	{
		case TileType::SINGLE_HIT:
			loopFromIndex = 1;
			loopToIndex = 8;
			break;
		case TileType::DOUBLE_HIT:
			loopFromIndex = 11;
			loopToIndex = 20;
			break;
		case TileType::RIGID:
			loopFromIndex = 39;
			loopToIndex = 39;
			break;
		case TileType::GOOD_MEMORY:
			loopFromIndex = 31;
			loopToIndex = 38;
			break;
		case TileType::BAD_MEMORY:
			loopFromIndex = 22;
			loopToIndex = 29;
			break;
	}

	for (int i = 0; i < MAX_TILE_DESTROY_SIMUL; i++)
	{
		if (!tileDestroyData[i].animator.animationRunning)
		{
			tileDestroyData[i].model = model;
			InitiateSpriteAnimator(&tileDestroyData[i].animator, loopFromIndex, loopToIndex, framesPerSecond, loopsToExecute);
			break;
		}
	}
}

void InitiateSpriteAnimator(SpriteAnimator *spriteAnimator, int loopFromIndex, int loopToIndex, int framesPerSecond, int loopsToExecute)
{
	spriteAnimator->timePassed = 0.0f;
	spriteAnimator->loopFromIndex = loopFromIndex;
	spriteAnimator->loopToIndex = loopToIndex;
	spriteAnimator->framesPerSecond = framesPerSecond;
	spriteAnimator->loopsToExecute = loopsToExecute;
	spriteAnimator->animationRunning = true;
	spriteAnimator->currentIndex = loopFromIndex;
	spriteAnimator->loopsExecuted = 0;
}

void AnimateSprite(SpriteAnimator *spriteAnimator, float deltaTime)
{
	if (spriteAnimator->animationRunning)
	{
		spriteAnimator->timePassed += deltaTime;
		int framesToProgress = spriteAnimator->timePassed * spriteAnimator->framesPerSecond;
		int framesPerLoop = spriteAnimator->loopToIndex - spriteAnimator->loopFromIndex + 1;
		spriteAnimator->loopsExecuted = framesToProgress / framesPerLoop;
		spriteAnimator->currentIndex = (framesToProgress % framesPerLoop) + spriteAnimator->loopFromIndex;
		if (spriteAnimator->loopsExecuted == spriteAnimator->loopsToExecute)
		{
			spriteAnimator->animationRunning = false;
		}
	}
}

void RenderHorizontalBlur()
{
	glClearColor(0.5f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_STENCIL_TEST);

	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	FrameBuffer framebuffer = renderer->horizontalBlur;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;

	UseTexture2D(framebuffer.colorTextureBuffer, UNBOOKED_SLOT);
	UseShader(shaderIds[Shaders::HORIZONTAL_GAUSSIAN_BLUR_SHADER]);
	SetUniform1i(shaderIds[Shaders::HORIZONTAL_GAUSSIAN_BLUR_SHADER], "screenTexture", UNBOOKED_SLOT);
	DrawQuad(vaos[VAOS::OFFSCREEN_TEXTURE_VAO]);
}

void RenderVerticalBlur()
{
	glClearColor(0.5f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_STENCIL_TEST);

	Game *game = GetGameMemory();
	Renderer *renderer = &game->renderer;
	FrameBuffer framebuffer = renderer->verticleBlur;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;

	UseTexture2D(framebuffer.colorTextureBuffer, UNBOOKED_SLOT);
	UseShader(shaderIds[Shaders::VERTICAL_GAUSSIAN_BLUR_SHADER]);
	SetUniform1i(shaderIds[Shaders::VERTICAL_GAUSSIAN_BLUR_SHADER], "screenTexture", UNBOOKED_SLOT);
	DrawQuad(vaos[VAOS::OFFSCREEN_TEXTURE_VAO]);
}

void RenderPostProcess()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_STENCIL_TEST);

	Game *game = GetGameMemory();
	float time = game->currentTime;
	float deltaTime = game->deltaTime;
	Renderer *renderer = &game->renderer;
	float shakeTime = renderer->shakeTime;
	FrameBuffer framebuffer = renderer->postProcess;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;

	Shaders shaderInUse;
	unsigned int shaderInUseId;

	if (shakeTime > 0.0f)
	{
		shaderInUse = Shaders::SCREENSHAKE_SHADER;
		shaderInUseId = shaderIds[shaderInUse];

		float denominator = 300.0f / (shakeTime * shakeTime);
		//denominator = clamp(denominator, 1.0f, 300.0f);

		float offset = 1.0f / denominator;
		float offsets[9][2] = {
			{ -offset,  offset  },  // top-left
			{  0.0f,    offset  },  // top-center
			{  offset,  offset  },  // top-right
			{ -offset,  0.0f    },  // center-left
			{  0.0f,    0.0f    },  // center-center
			{  offset,  0.0f    },  // center - right
			{ -offset, -offset  },  // bottom-left
			{  0.0f,   -offset  },  // bottom-center
			{  offset, -offset  }   // bottom-right    
		};

		GLfloat blur_kernel[9] = {
			1.0 / 16, 2.0 / 16, 1.0 / 16,
			2.0 / 16, 4.0 / 16, 2.0 / 16,
			1.0 / 16, 2.0 / 16, 1.0 / 16
		};

		float shakeStrength = shakeTime * shakeTime * 0.01f;
		//shakeStrength = clamp(shakeStrength, 0.0f, 1.0f);

		UseShader(shaderInUseId);
		SetUniform1f(shaderInUseId, "time", time);
		SetUniform1f(shaderInUseId, "strength", shakeStrength);
		glUniform2fv(glGetUniformLocation(shaderInUseId, "offsets"), 9, (GLfloat*)offsets);
		glUniform1fv(glGetUniformLocation(shaderInUseId, "blur_kernel"), 9, blur_kernel);

		shakeTime -= deltaTime;
		shakeTime = clamp(shakeTime, 0.0f, renderer->maxShakeTime);
		renderer->shakeTime = shakeTime;
	}
	else
	{
		shaderInUse = Shaders::POST_PROCESS_SHADER;
		shaderInUseId = shaderIds[shaderInUse];

		UseShader(shaderInUseId);
	}

	UseTexture2D(framebuffer.colorTextureBuffer, UNBOOKED_SLOT);
	SetUniform1i(shaderInUseId, "screenTexture", UNBOOKED_SLOT);
	DrawQuad(vaos[VAOS::OFFSCREEN_TEXTURE_VAO]);
}

void RenderBackground()
{
	Game *game = GetGameMemory();

	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;
	unsigned int *texIds = renderer->texIds;
	Texture2D *textures = renderer->textures;

	UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);

	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(game->canvasWidth * 0.5f, game->canvasHeight * 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(game->canvasWidth, game->canvasHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", GetTexSlot(textures[Textures::BACKGROUND_TEX].id, texIds));
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
}

void RenderTiles()
{
	Game *game = GetGameMemory();
	float time = game->currentTime;
	float deltaTime = game->deltaTime;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;

	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;
	unsigned int *texIds = renderer->texIds;
	Texture2D *textures = renderer->textures;
	SpritesheetVertexBufferData tilemapVertexBufferData = renderer->tilemapVertexBufferData;
	SpritesheetRenderData *tilemapRenderData = renderer->tilemapRenderData;

	int gridStage, deltaStage;

	Tile *tiles = currentTilemapInfo->tiles;
	glm::vec2 *positions = currentTilemapInfo->positions;
	TileType *types = currentTilemapInfo->types;
	glm::vec3 tileScale = glm::vec3(currentTilemapInfo->tileSlotScale.x, currentTilemapInfo->tileSlotScale.y, 1.0f);

	glm::vec3 colorCollider = glm::vec3(0.0f, (sinf(time*8.0f) / 2.0f) + 0.5f, 0.2f);
	int totalActiveTiles = currentTilemapInfo->totalActiveTiles;

	int totalActiveNonMemoryTiles = currentTilemapInfo->totalActiveNonMemoryTiles;
	int nonMemoryTilesStartIndex = currentTilemapInfo->nonMemoryTilesStartIndex;

	int totalActiveGoodMemoryTiles = currentTilemapInfo->totalActiveGoodMemoryTiles;
	int goodMemoryTilesStartIndex = currentTilemapInfo->goodMemoryTilesStartIndex;

	int totalActiveBadMemoryTiles = currentTilemapInfo->totalActiveBadMemoryTiles;
	int badMemoryTilesStartIndex = currentTilemapInfo->badMemoryTilesStartIndex;

	for (int i = 0; i < totalActiveTiles; i++)
	{
		gridStage = GetTilemapIndex(types[i]);
		deltaStage = GetTileHitCapacity(types[i]) - tiles[i].hitsLeft;
		gridStage += deltaStage;
		tilemapRenderData[i].index = gridStage;
		tilemapRenderData[i].model = glm::mat4(1.0f);
		tilemapRenderData[i].model = glm::translate(tilemapRenderData[i].model, glm::vec3(positions[i], 0.0f));
		tilemapRenderData[i].model = glm::scale(tilemapRenderData[i].model, tileScale);
	}

	TileDestroyData *tileDestroyDatas = renderer->tileDestroyInfos;
	int tilesToDestroy = 0;
	for (int i = 0; i < MAX_TILE_DESTROY_SIMUL; i++)
	{
		if (tileDestroyDatas[i].animator.animationRunning)
		{
			tilemapRenderData[totalActiveTiles + tilesToDestroy].model = tileDestroyDatas[i].model;
			AnimateSprite(&tileDestroyDatas[i].animator, deltaTime);

			if (tileDestroyDatas[i].animator.animationRunning)
			{
				tilemapRenderData[totalActiveTiles + tilesToDestroy].index = tileDestroyDatas[i].animator.currentIndex;
				tilesToDestroy++;
			}
		}
		//std::cout << tilesToDestroy << std::endl;
	}

	int totalTilesToRender = totalActiveTiles + tilesToDestroy;

	RenderInstancedSpritesheet(&renderer->tilemapVertexBufferData, renderer->tilemapRenderData, totalTilesToRender, GetTexSlot(textures[Textures::TILE_TEXES].id, texIds), 10, 4, Shaders::TILEMAP_SHADER);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	unsigned int shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
	unsigned int textureId = textures[Textures::JIGSAW_TEXES].id;

	float pieceProportionToTile = 0.75f;
	float outlineProportionToPiece = 1.5f;
	float degreeRotationPerSecond = 120.0f;
	float colorBlinkSpeedPerSecond = 8.0f;
	float currentTime = game->currentTime;

	float offset = 1.0f;
	glm::vec2 offsetPoses[4] =
	{
		glm::vec2(-offset, offset),
		glm::vec2(offset, offset),
		glm::vec2(-offset, -offset),
		glm::vec2(offset, -offset),
	};

	glEnable(GL_STENCIL_TEST);

	MemoryPanel *panel = &currentLevelInfo->goodMemoryBoard.panel;
	JigsawPiece *pieces = panel->pieces;
	int startIndex = goodMemoryTilesStartIndex;
	int lastIndex = startIndex + totalActiveGoodMemoryTiles - 1;
	glm::vec4 outlineColor = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);

	shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
	textureId = textures[Textures::JIGSAW_UI_TEXES].id;

	UseShader(shaderId);
	UseTexture2D(textureId, UNBOOKED_SLOT);

	for (int i = startIndex; i <= lastIndex; i++)
	{
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilFunc(GL_ALWAYS, i + 1, 0xFF);
		glStencilMask(0xFF);
		
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(positions[i], 0.0f));
		model = glm::rotate(model, glm::radians(currentTime * degreeRotationPerSecond), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(tileScale.y * pieceProportionToTile, tileScale.y * pieceProportionToTile, 1.0f));
		
		SetUniform4x4(shaderId, "model", model);
		SetUniform2f(shaderId, "gridSize", 3, 3);
		SetUniform2f(shaderId, "cellCoord", pieces[tiles[i].memory.pieceIndex].colType, pieces[tiles[i].memory.pieceIndex].rowType);
		SetUniform1i(shaderId, "mainTex", UNBOOKED_SLOT);
		SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);
		
		DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);


		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		glStencilFunc(GL_EQUAL, 0, 0xFF);
		glStencilMask(0xFF);


		for (int j = 0; j < 4; j++)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(positions[i] + offsetPoses[j], 0.0f));
			model = glm::rotate(model, glm::radians(currentTime * degreeRotationPerSecond), glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::scale(model, glm::vec3(tileScale.y * pieceProportionToTile, tileScale.y * pieceProportionToTile, 1.0f));

			SetUniform4x4(shaderId, "model", model);
			SetUniform2f(shaderId, "gridSize", 3, 3);
			SetUniform2f(shaderId, "cellCoord", pieces[tiles[i].memory.pieceIndex].colType, pieces[tiles[i].memory.pieceIndex].rowType);
			SetUniform1i(shaderId, "mainTex", UNBOOKED_SLOT);
			SetUniform4f(shaderId, "color", outlineColor.r, outlineColor.g, outlineColor.b, outlineColor.a);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
	}



	panel = &currentLevelInfo->badMemoryBoard.panel;
	pieces = panel->pieces;
	startIndex = badMemoryTilesStartIndex;
	lastIndex = startIndex + totalActiveBadMemoryTiles - 1;
	outlineColor = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
	textureId = textures[Textures::JIGSAW_UI_TEXES].id;

	UseShader(shaderId);
	UseTexture2D(textureId, UNBOOKED_SLOT);

	for (int i = startIndex; i <= lastIndex; i++)
	{
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilFunc(GL_ALWAYS, i + 1, 0xFF);
		glStencilMask(0xFF);

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(positions[i], 0.0f));
		model = glm::rotate(model, glm::radians(currentTime * degreeRotationPerSecond), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(tileScale.y * pieceProportionToTile, tileScale.y * pieceProportionToTile, 1.0f));

		SetUniform4x4(shaderId, "model", model);
		SetUniform2f(shaderId, "gridSize", 3, 3);
		SetUniform2f(shaderId, "cellCoord", pieces[tiles[i].memory.pieceIndex].colType, pieces[tiles[i].memory.pieceIndex].rowType);
		SetUniform1i(shaderId, "mainTex", UNBOOKED_SLOT);
		SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);

		DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);


		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		glStencilFunc(GL_EQUAL, 0, 0xFF);
		glStencilMask(0xFF);


		for (int j = 0; j < 4; j++)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(positions[i] + offsetPoses[j], 0.0f));
			model = glm::rotate(model, glm::radians(currentTime * degreeRotationPerSecond), glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::scale(model, glm::vec3(tileScale.y * pieceProportionToTile, tileScale.y * pieceProportionToTile, 1.0f));

			SetUniform4x4(shaderId, "model", model);
			SetUniform2f(shaderId, "gridSize", 3, 3);
			SetUniform2f(shaderId, "cellCoord", pieces[tiles[i].memory.pieceIndex].colType, pieces[tiles[i].memory.pieceIndex].rowType);
			SetUniform1i(shaderId, "mainTex", UNBOOKED_SLOT);
			SetUniform4f(shaderId, "color", outlineColor.r, outlineColor.g, outlineColor.b, outlineColor.a);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
	}

	glDisable(GL_STENCIL_TEST);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	

}

void RenderBall()
{
	Game *game = GetGameMemory();
	float time = game->currentTime;
	float deltaTime = game->deltaTime;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	Renderer *renderer = &game->renderer;
	SpriteAnimator *explosionBallAnimator = &renderer->explosionBallAnimator;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;
	unsigned int *texIds = renderer->texIds;
	Texture2D *textures = renderer->textures;
	glm::vec3 colorCollider = glm::vec3(0.0f, (sinf(time*8.0f) / 2.0f) + 0.5f, 0.2f);

	Ball *ball = &currentLevelInfo->ball;
	if (ball->isActive)
	{
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(ball->position, 0.0f));
		model = glm::scale(model, glm::vec3(ball->radius * 2.0f, ball->radius * 2.0f, 1.0f));
		model = glm::rotate(model, glm::radians(ball->angle), glm::vec3(0.0f, 0.0f, 1.0f));
		unsigned int index = GetBallIndex(ball->type);

		int slot = GetTexSlot(textures[Textures::BALLS_TEXES].id, texIds);

		UseShader(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER]);
		UseTexture2D(textures[Textures::BALLS_TEXES].id, slot);

		SetUniform1i(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER], "rows", 2);
		SetUniform1i(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER], "cols", 3);
		SetUniform1i(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER], "index", index);
		SetUniform4x4(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER], "model", model);
		SetUniform1i(shaderIds[Shaders::SPRITESHEET_BASIC_SHADER], "sheetTex", slot);
		DrawQuad(vaos[VAOS::SPRITESHEET_BASIC_VAO]);
	}
	else
	{
		explosionBallAnimator->timePassed = 0.0f;
	}
}

void RenderWalls()
{
	Game *game = GetGameMemory();
	float canvasWidth = game->canvasWidth;
	float canvasHeight = game->canvasHeight;
	CurrentTilemapInfo *currentTilemapInfo = &game->currentTilemapInfo;
	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;
	unsigned int *texIds = renderer->texIds;
	Texture2D *textures = renderer->textures;

	UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(canvasWidth * 0.5f, canvasHeight * 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(canvasWidth, canvasHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", GetTexSlot(textures[Textures::WALL_TEX].id, texIds));
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
}

void RenderShooter(int renderGun)
{
	Game *game = GetGameMemory();
	float time = game->currentTime;
	float deltaTime = game->deltaTime;

	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *vaos = renderer->vaos;
	unsigned int *texIds = renderer->texIds;
	Texture2D *textures = renderer->textures;

	Shooter *shooter = &currentLevelInfo->shooter;
	Gun *gun = &currentLevelInfo->gun;

	
	float shooterAlpha = 1.0f;
	/* GUN RENDERING */
	if (renderGun && gun->isActive)
	{
		DottedLine *line = &renderer->dottedLine;
		line->angle = gun->angle + 90.0f;
		line->from = gun->position;
		DrawDottedLine(*line, shaderIds[Shaders::LINE_SHADER]);

		shooterAlpha = 0.5f;
	}
	

	/* SHOOTER RENDERING */
	if (shooter->isActive)
	{
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(shooter->position, 0.0f));
		model = glm::rotate(model, glm::radians(shooter->tiltAngle), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::scale(model, glm::vec3(shooter->width, shooter->height, 1.0f));

		UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);
		SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
		SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", GetTexSlot(textures[Textures::SHOOTER_TEX].id, texIds));
		SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, shooterAlpha);
		DrawQuad(vaos[VAOS::BOTTOM_CENTRE_VAO]);
	}
}

void RenderMemoryPieces()
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	Renderer *renderer = &game->renderer;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *texIds = renderer->texIds;
	unsigned int *vaos = renderer->vaos;
	Texture2D *textures = renderer->textures;
	Texture2D *nodeTextures = renderer->nodeTextures;


	UseTexture2D(textures[Textures::BLACKBOARD_TEX].id, UNBOOKED_SLOT);
	unsigned int shaderToUse = shaderIds[Shaders::QUAD_TEXTURED_SHADER];
	UseShader(shaderToUse);
	SetUniform1i(shaderToUse, "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderToUse, "color", 1.0f, 1.0f, 1.0f, 1.0f);


	Blackboard *board = &currentLevelInfo->goodMemoryBoard;

	glm::mat4 modelFrame = glm::mat4(1.0f);
	modelFrame = glm::translate(modelFrame, glm::vec3(board->position.x, board->position.y, 0.0f));
	modelFrame = glm::scale(modelFrame, glm::vec3(board->boardWidth, board->boardHeight, 1.0f));
	SetUniform4x4(shaderToUse, "model", modelFrame);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);

	board = &currentLevelInfo->badMemoryBoard;

	modelFrame = glm::mat4(1.0f);
	modelFrame = glm::translate(modelFrame, glm::vec3(board->position.x, board->position.y, 0.0f));
	modelFrame = glm::scale(modelFrame, glm::vec3(board->boardWidth, board->boardHeight, 1.0f));
	SetUniform4x4(shaderToUse, "model", modelFrame);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);


	float offset = 1.0f;
	glm::vec2 offsetPoses[4] =
	{
		glm::vec2(-offset, offset),
		glm::vec2(offset, offset),
		glm::vec2(-offset, -offset),
		glm::vec2(offset, -offset),
	};

	glEnable(GL_STENCIL_TEST);

	unsigned int shaderId;
	unsigned int textureId;

	MemoryPanel *panel = &currentLevelInfo->goodMemoryBoard.panel;
	JigsawPiece *pieces = panel->pieces;
	glm::vec4 outlineColor = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);

	for (int i = 0; i <= MAX_PIECES; i++)
	{
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilMask(0xFF);

		shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
		textureId = textures[Textures::JIGSAW_TEXES].id;

		UseShader(shaderId);

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
		model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));

		SetUniform4x4(shaderId, "model", model);
		SetUniform2f(shaderId, "gridSize", 3, 3);
		SetUniform2f(shaderId, "cellCoord", pieces[i].colType, pieces[i].rowType);
		SetUniform1i(shaderId, "mainTex", GetTexSlot(textureId, texIds));

		if (pieces[i].isActive)
		{
			glStencilFunc(GL_ALWAYS, i + 100, 0xFF);
			SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
		else
		{
			glStencilFunc(GL_ALWAYS, i + 1, 0xFF);
			SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
	}

	for (int i = 0; i <= MAX_PIECES; i++)
	{
		shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
		textureId = textures[Textures::JIGSAW_TEXES].id;

		UseShader(shaderId);

		if (pieces[i].isActive)
		{
			glStencilOp(GL_KEEP, GL_KEEP, GL_ZERO);
			glStencilFunc(GL_GREATER, 99, 0xFF);
			glStencilMask(0xFF);
		}
		else
		{
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
			glStencilFunc(GL_NOTEQUAL, i + 1, 0xFF);
			glStencilMask(0xFF);
		}

		for (int j = 0; j < 4; j++)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position + offsetPoses[j], 1.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));

			SetUniform4x4(shaderId, "model", model);
			SetUniform2f(shaderId, "gridSize", 3, 3);
			SetUniform2f(shaderId, "cellCoord", pieces[i].colType, pieces[i].rowType);
			SetUniform1i(shaderId, "mainTex", GetTexSlot(textureId, texIds));
			SetUniform4f(shaderId, "color", outlineColor.r, outlineColor.g, outlineColor.b, outlineColor.a);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
	}

	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_LESS, 99, 0xFF);
	glStencilMask(0xFF);

	panel = &currentLevelInfo->goodMemoryBoard.panel;
	Node *node = currentLevelInfo->currentNode->optionPos;

	UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);
	UseTexture2D(nodeTextures[node->type].id, UNBOOKED_SLOT);
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(panel->position, 0.0f));
	model = glm::scale(model, glm::vec3(panel->panelWidth, panel->panelHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);


	UseShader(shaderIds[Shaders::MEMORY_PIECE_SHADER]);
	SetUniform2f(shaderIds[Shaders::MEMORY_PIECE_SHADER], "gridSize", 3, 3);
	SetUniform1i(shaderIds[Shaders::MEMORY_PIECE_SHADER], "mainTex", GetTexSlot(textures[Textures::JIGSAW_TEXES].id, texIds));




	panel = &currentLevelInfo->badMemoryBoard.panel;
	pieces = panel->pieces;
	outlineColor = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	for (int i = 0; i <= MAX_PIECES; i++)
	{
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilMask(0xFF);

		shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
		textureId = textures[Textures::JIGSAW_TEXES].id;

		UseShader(shaderId);

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
		model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));

		SetUniform4x4(shaderId, "model", model);
		SetUniform2f(shaderId, "gridSize", 3, 3);
		SetUniform2f(shaderId, "cellCoord", pieces[i].colType, pieces[i].rowType);
		SetUniform1i(shaderId, "mainTex", GetTexSlot(textureId, texIds));

		if (pieces[i].isActive)
		{
			glStencilFunc(GL_ALWAYS, i + 100, 0xFF);
			SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
		else
		{
			glStencilFunc(GL_ALWAYS, i + 1, 0xFF);
			SetUniform4f(shaderId, "color", 1.0f, 1.0f, 1.0f, 0.0f);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}

	}

	for (int i = 0; i <= MAX_PIECES; i++)
	{
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilMask(0xFF);

		shaderId = shaderIds[Shaders::MEMORY_PIECE_SHADER];
		textureId = textures[Textures::JIGSAW_TEXES].id;

		UseShader(shaderId);

		if (pieces[i].isActive)
		{
			glStencilOp(GL_KEEP, GL_KEEP, GL_ZERO);
			glStencilFunc(GL_GREATER, 99, 0xFF);
			glStencilMask(0xFF);
		}
		else
		{
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
			glStencilFunc(GL_NOTEQUAL, i + 1, 0xFF);
			glStencilMask(0xFF);
		}

		for (int j = 0; j < 4; j++)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position + offsetPoses[j], 0.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));

			SetUniform4x4(shaderId, "model", model);
			SetUniform2f(shaderId, "gridSize", 3, 3);
			SetUniform2f(shaderId, "cellCoord", pieces[i].colType, pieces[i].rowType);
			SetUniform1i(shaderId, "mainTex", GetTexSlot(textureId, texIds));
			SetUniform4f(shaderId, "color", outlineColor.r, outlineColor.g, outlineColor.b, outlineColor.a);

			DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);
		}
	}

	glStencilFunc(GL_LESS, 99, 0xFF);
	glStencilMask(0xFF);

	panel = &currentLevelInfo->badMemoryBoard.panel;
	node = currentLevelInfo->currentNode->optionNeg;

	UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);
	UseTexture2D(nodeTextures[node->type].id, UNBOOKED_SLOT);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(panel->position, 0.0f));
	model = glm::scale(model, glm::vec3(panel->panelWidth, panel->panelHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);






	glStencilMask(0xFF);
	glDisable(GL_STENCIL_TEST);
}

void RenderMemoryPieces(int renderOutline)
{
	Game *game = GetGameMemory();
	float deltaTime = game->deltaTime;
	Renderer *renderer = &game->renderer;
	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *texIds = renderer->texIds;
	unsigned int *vaos = renderer->vaos;
	Texture2D *textures = renderer->textures;
	Texture2D *nodeTextures = renderer->nodeTextures;


	UseTexture2D(textures[Textures::BLACKBOARD_TEX].id, UNBOOKED_SLOT);
	unsigned int shaderToUse = shaderIds[Shaders::QUAD_TEXTURED_SHADER];
	UseShader(shaderToUse);
	SetUniform1i(shaderToUse, "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderToUse, "color", 1.0f, 1.0f, 1.0f, 1.0f);


	Blackboard *board = &currentLevelInfo->goodMemoryBoard;

	glm::mat4 modelFrame = glm::mat4(1.0f);
	modelFrame = glm::translate(modelFrame, glm::vec3(board->position.x, board->position.y, 0.0f));
	modelFrame = glm::scale(modelFrame, glm::vec3(board->boardWidth, board->boardHeight, 1.0f));
	SetUniform4x4(shaderToUse, "model", modelFrame);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);

	board = &currentLevelInfo->badMemoryBoard;

	modelFrame = glm::mat4(1.0f);
	modelFrame = glm::translate(modelFrame, glm::vec3(board->position.x, board->position.y, 0.0f));
	modelFrame = glm::scale(modelFrame, glm::vec3(board->boardWidth, board->boardHeight, 1.0f));
	SetUniform4x4(shaderToUse, "model", modelFrame);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);


	glEnable(GL_STENCIL_TEST);

	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE); // Replace only if both stencil and depth pass.
	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	glStencilMask(0xFF);

	UseShader(shaderIds[Shaders::MEMORY_PIECE_SHADER]);
	SetUniform2f(shaderIds[Shaders::MEMORY_PIECE_SHADER], "gridSize", 3, 3);
	SetUniform1i(shaderIds[Shaders::MEMORY_PIECE_SHADER], "mainTex", GetTexSlot(textures[Textures::JIGSAW_TEXES].id, texIds));

	MemoryPanel *panel = &currentLevelInfo->goodMemoryBoard.panel;
	JigsawPiece *pieces = panel->pieces;
	for (int i = 0; i < MAX_PIECES; i++)
	{
		if (pieces[i].isActive)
		{
			pieces[i].timeAlive += deltaTime;
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));
			SetUniform4x4(shaderIds[Shaders::MEMORY_PIECE_SHADER], "model", model);
			SetUniform2f(shaderIds[Shaders::MEMORY_PIECE_SHADER], "cellCoord", pieces[i].colType, pieces[i].rowType);
			DrawQuad(vaos[VAOS::MEMORY_PIECE_VAO]);
		}
	}

	panel = &currentLevelInfo->badMemoryBoard.panel;
	pieces = panel->pieces;
	for (int i = 0; i < MAX_PIECES; i++)
	{
		if (pieces[i].isActive)
		{
			pieces[i].timeAlive += deltaTime;
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize, panel->pieceSize, 1.0f));
			SetUniform4x4(shaderIds[Shaders::MEMORY_PIECE_SHADER], "model", model);
			SetUniform2f(shaderIds[Shaders::MEMORY_PIECE_SHADER], "cellCoord", pieces[i].colType, pieces[i].rowType);
			DrawQuad(vaos[VAOS::MEMORY_PIECE_VAO]);
		}
	}

	glStencilFunc(GL_EQUAL, 1, 0xFF);
	glStencilMask(0xFF);

	UseShader(shaderIds[Shaders::QUAD_TEXTURED_SHADER]);

	panel = &currentLevelInfo->goodMemoryBoard.panel;
	Node *node = currentLevelInfo->currentNode->optionPos;

	UseTexture2D(nodeTextures[node->type].id, UNBOOKED_SLOT);
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(panel->position, 0.0f));
	model = glm::scale(model, glm::vec3(panel->panelWidth, panel->panelHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);

	panel = &currentLevelInfo->badMemoryBoard.panel;
	node = currentLevelInfo->currentNode->optionNeg;

	UseTexture2D(nodeTextures[node->type].id, UNBOOKED_SLOT);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(panel->position, 0.0f));
	model = glm::scale(model, glm::vec3(panel->panelWidth, panel->panelHeight, 1.0f));
	SetUniform4x4(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "model", model);
	SetUniform1i(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "mainTex", UNBOOKED_SLOT);
	SetUniform4f(shaderIds[Shaders::QUAD_TEXTURED_SHADER], "color", 1.0f, 1.0f, 1.0f, 1.0f);
	DrawQuad(vaos[VAOS::QUAD_TEXTURED_VAO]);

	//glStencilMask(0xFF);

	//glDisable(GL_STENCIL_TEST);

	RenderOutlinedPieces(3.0f);

	glStencilMask(0xFF);
	glDisable(GL_STENCIL_TEST);
}

void RenderOutlinedPieces(float maxTime)
{
	Game *game = GetGameMemory();
	float time = game->currentTime;
	Renderer *renderer = &game->renderer;
	unsigned int *shaderIds = renderer->shaderIds;
	unsigned int *texIds = renderer->texIds;
	unsigned int *vaos = renderer->vaos;
	Texture2D *textures = renderer->textures;
	Texture2D *nodeTextures = renderer->nodeTextures;

	CurrentLevelInfo *currentLevelInfo = &game->currentLevelInfo;

	//glDisable(GL_BLEND);

	glStencilOp(GL_KEEP, GL_INCR, GL_INCR);
	glStencilFunc(GL_EQUAL, 0, 0xFF);
	glStencilMask(0xFF);

	float frequency = 8.0f;
	float scale = 1.1f;
	Shaders shaderToUse = Shaders::OUTLINE_SHADER;

	UseShader(shaderIds[shaderToUse]);
	SetUniform2f(shaderIds[shaderToUse], "gridSize", 3, 3);
	SetUniform4f(shaderIds[shaderToUse], "color", 0.0f, 1.0f, 0.0f, (sinf(time * frequency) * 0.5f) + 0.5f);
	SetUniform1i(shaderIds[shaderToUse], "mainTex", GetTexSlot(textures[Textures::JIGSAW_TEXES].id, texIds));

	MemoryPanel *panel = &currentLevelInfo->goodMemoryBoard.panel;
	JigsawPiece *pieces = panel->pieces;
	for (int i = 0; i < MAX_PIECES; i++)
	{
		if (pieces[i].isActive && pieces[i].timeAlive <= maxTime)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize * scale, panel->pieceSize * scale, 1.0f));
			//model = glm::scale(model, glm::vec3(panel->pieceSize + scale, panel->pieceSize + scale, 1.0f));
			SetUniform4x4(shaderIds[shaderToUse], "model", model);
			SetUniform2f(shaderIds[shaderToUse], "cellCoord", pieces[i].colType, pieces[i].rowType);
			DrawQuad(vaos[VAOS::MEMORY_PIECE_VAO]);
		}
	}

	panel = &currentLevelInfo->badMemoryBoard.panel;
	pieces = panel->pieces;
	for (int i = 0; i < MAX_PIECES; i++)
	{
		if (pieces[i].isActive && pieces[i].timeAlive <= maxTime)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(pieces[i].position, 0.0f));
			model = glm::scale(model, glm::vec3(panel->pieceSize * scale, panel->pieceSize * scale, 1.0f));
			//model = glm::scale(model, glm::vec3(panel->pieceSize + scale, panel->pieceSize + scale, 1.0f));
			SetUniform4x4(shaderIds[shaderToUse], "model", model);
			SetUniform2f(shaderIds[shaderToUse], "cellCoord", pieces[i].colType, pieces[i].rowType);
			DrawQuad(vaos[VAOS::MEMORY_PIECE_VAO]);
		}
	}

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

#pragma endregion

void CommonRenderFlags()
{
	glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
	glEnable(GL_STENCIL_TEST);
	glEnable(GL_BLEND);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glStencilFunc(GL_NOTEQUAL, 1, 0XFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	glStencilMask(0x00);
	// Something new.
}

#pragma region RenderSceneMainMenu

void LoadRendererMainMenu()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderMainMenuSceneUI();	
}

void LoadingRendererMainMenu()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderMainMenuSceneUI();
}

void UpdateRendererMainMenu()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderMainMenuSceneUI();
}

void UnloadRendererMainMenu()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderMainMenuSceneUI();
}

void UnloadingRendererMainMenu()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderMainMenuSceneUI();
}

#pragma endregion


#pragma region RenderSceneGame

void RenderGameScene()
{
	CommonRenderFlags();

	RenderBackground();
	RenderTiles();
	RenderMemoryPieces();
	RenderShooter(true);
	RenderBall();
	RenderSnowfall();
	RenderWalls();

	RenderParticles();

	RenderLevelUI();
}

void RenderSceneGamePlay()
{
	Renderer *renderer = &GetGameMemory()->renderer;
	FrameBuffer framebuffer = renderer->postProcess;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.fbo);

	RenderGameScene();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	RenderPostProcess();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	RenderGameSceneUI();
}

void RenderSceneGamePaused()
{
	Renderer *renderer = &GetGameMemory()->renderer;
	FrameBuffer framebuffer = renderer->horizontalBlur;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.fbo);

	RenderGameScene();

	framebuffer = renderer->verticleBlur;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.fbo);

	RenderHorizontalBlur();

	framebuffer = renderer->postProcess;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.fbo);

	RenderVerticalBlur();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//CommonRenderFlags();
	RenderPostProcess();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	RenderGameSceneUI();
}

void LoadRendererGame()
{
	RenderSceneGamePlay();
	//RenderGameScene();
}

void LoadingRendererGame()
{
	RenderSceneGamePlay();
	//RenderGameScene();
}

void UpdateRendererGame()
{
	RenderSceneGamePlay();
	//RenderGameScene();
}

void UpdateRendererGamePaused()
{
	RenderSceneGamePaused();
}

void UnloadRendererGame()
{
	RenderSceneGamePlay();
	//RenderGameScene();
}

void UnloadingRendererGame()
{
	RenderSceneGamePlay();
	//RenderGameScene();
}

#pragma endregion


#pragma region RenderSceneLevelSelector

void LoadRendererLevelSelector()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderLevelSelectorSceneUI();
}

void LoadingRendererLevelSelector()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderLevelSelectorSceneUI();
}

void UpdateRendererLevelSelector()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderLevelSelectorSceneUI();
}

void UnloadRendererLevelSelector()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderLevelSelectorSceneUI();
}

void UnloadingRendererLevelSelector()
{
	CommonRenderFlags();

	RenderBackground();
	RenderSnowfall();
	RenderWalls();

	RenderLevelSelectorSceneUI();
}


#pragma endregion